package de.fuchspfoten.gulag.modules;

import de.fuchspfoten.gulag.model.ChunkCoordinate;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownManager;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Tameable;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.AreaEffectCloudApplyEvent;
import org.bukkit.event.entity.EntityCombustByEntityEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.vehicle.VehicleDamageEvent;

/**
 * Protects chunks in the town world.
 */
public class ProtectionModule implements Listener {

    @EventHandler
    public void onBlockBreak(final BlockBreakEvent event) {
        // Check takes care of world.
        if (!TownManager.get().isAllowedByProtection(event.getPlayer(), event.getBlock().getLocation())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockPlace(final BlockPlaceEvent event) {
        // Check takes care of world.
        if (!TownManager.get().isAllowedByProtection(event.getPlayer(), event.getBlock().getLocation())) {
            event.setCancelled(true);
        }
    }

    /**
     * Gets the player that caused an entity to do something.
     *
     * @param entity The entity.
     * @return The player, or null.
     */
    private static Player getPlayerCause(final Entity entity) {
        if (entity instanceof Player) {
            return (Player) entity;
        }
        if (entity instanceof Projectile) {
            if (((Projectile) entity).getShooter() instanceof Player) {
                return (Player) ((Projectile) entity).getShooter();
            }
        } else if (entity instanceof Tameable) {
            if (((Tameable) entity).isTamed() && ((Tameable) entity).getOwner() instanceof Player) {
                return (Player) ((Tameable) entity).getOwner();
            }
        }
        return null;
    }

    @EventHandler
    public void onHangingBreakByEntity(final HangingBreakByEntityEvent event) {
        // Check takes care of world.
        if (event.getRemover() instanceof Player) {
            if (!TownManager.get().isAllowedByProtection((Player) event.getRemover(),
                    event.getEntity().getLocation())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onHangingPlace(final HangingPlaceEvent event) {
        // Check takes care of world.
        if (!TownManager.get().isAllowedByProtection(event.getPlayer(), event.getEntity().getLocation())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerBucketEmpty(final PlayerBucketEmptyEvent event) {
        // Check takes care of world.
        final Block effective = event.getBlockClicked().getRelative(event.getBlockFace());
        if (!TownManager.get().isAllowedByProtection(event.getPlayer(), effective.getLocation())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerBucketFill(final PlayerBucketFillEvent event) {
        // Check takes care of world.
        if (!TownManager.get().isAllowedByProtection(event.getPlayer(), event.getBlockClicked().getLocation())) {
            event.setCancelled(true);
        }
    }

    /**
     * Checks whether or not the entity type is whitelisted.
     *
     * @param type The entity type.
     * @return True iff it is whitelisted.
     */
    private static boolean isWhitelistedEntity(final EntityType type) {
        switch (type) {
            case BLAZE:
            case CAVE_SPIDER:
            case CREEPER:
            case DRAGON_FIREBALL:
            case ELDER_GUARDIAN:
            case ENDER_DRAGON:
            case ENDERMAN:
            case ENDERMITE:
            case EVOKER:
            case FIREBALL:
            case GHAST:
            case GIANT:
            case GUARDIAN:
            case HUSK:
            case ILLUSIONER:
            case MAGMA_CUBE:
            case PIG_ZOMBIE:
            case POLAR_BEAR:
            case SHULKER:
            case SILVERFISH:
            case SKELETON:
            case SLIME:
            case SPIDER:
            case STRAY:
            case VEX:
            case VINDICATOR:
            case WITCH:
            case WITHER:
            case WITHER_SKELETON:
            case ZOMBIE:
            case ZOMBIE_VILLAGER:
                return true;
            default:
                return false;
        }
    }

    @EventHandler
    public void onPlayerInteractEntity(final PlayerInteractEntityEvent event) {
        if (event.getRightClicked() instanceof Tameable) {
            final Tameable tameable = (Tameable) event.getRightClicked();
            if (tameable.isTamed() && tameable.getOwner() != null
                    && tameable.getOwner().getUniqueId().equals(event.getPlayer().getUniqueId())) {
                // Is owned by the actor.
                return;
            }
        }

        // Check takes care of world.
        if (!TownManager.get().isAllowedByProtection(event.getPlayer(), event.getRightClicked().getLocation())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerArmorStandManipulate(final PlayerArmorStandManipulateEvent event) {
        // Check takes care of world.
        if (!TownManager.get().isAllowedByProtection(event.getPlayer(), event.getRightClicked().getLocation())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onVehicleDamage(final VehicleDamageEvent event) {
        final Player issuer = getPlayerCause(event.getAttacker());
        if (issuer == null) {
            // No player cause.
            return;
        }

        // Check takes care of world.
        if (!TownManager.get().isAllowedByProtection(issuer, event.getVehicle().getLocation())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPotionSplash(final PotionSplashEvent event) {
        final Player issuer = getPlayerCause(event.getPotion());
        if (issuer == null) {
            // No player cause.
            return;
        }

        final Location splashLocation = event.getPotion().getLocation();
        if (!TownManager.get().isTownWorld(splashLocation.getWorld())) {
            // Not handling non-town worlds.
            return;
        }

        event.getAffectedEntities().stream()
                .filter(e -> !mayAttack(issuer, e))
                .forEach(e -> event.setIntensity(e, -1.0));
    }

    @EventHandler
    public void onAreaEffectCloudApply(final AreaEffectCloudApplyEvent event) {
        if (!(event.getEntity().getSource() instanceof Player)) {
            // Not from a player.
            return;
        }
        final Player issuer = (Player) event.getEntity().getSource();

        final Location splashLocation = event.getEntity().getLocation();
        if (!TownManager.get().isTownWorld(splashLocation.getWorld())) {
            // Not handling non-town worlds.
            return;
        }

        event.getAffectedEntities().removeIf(e -> !mayAttack(issuer, e));
    }

    @EventHandler
    public void onBlockPistonExtend(final BlockPistonExtendEvent event) {
        // Only handle town worlds.
        if (!TownManager.get().isTownWorld(event.getBlock().getWorld())) {
            return;
        }

        final ChunkCoordinate pistonCoord = ChunkCoordinate.forChunk(event.getBlock().getChunk());
        final Town pistonTown = TownManager.get().getTownAt(pistonCoord);

        for (final Block block : event.getBlocks()) {
            final ChunkCoordinate blockCoord = ChunkCoordinate.forChunk(block.getChunk());
            final ChunkCoordinate targetCoord =
                    ChunkCoordinate.forChunk(block.getRelative(event.getDirection()).getChunk());
            final Town blockTown = TownManager.get().getTownAt(blockCoord);
            final Town targetTown = TownManager.get().getTownAt(targetCoord);

            if (blockTown != pistonTown || targetTown != blockTown) {
                event.setCancelled(true);
                return;
            }
        }
    }

    @EventHandler
    public void onBlockPistonRetract(final BlockPistonRetractEvent event) {
        // Only handle town worlds.
        if (!TownManager.get().isTownWorld(event.getBlock().getWorld())) {
            return;
        }

        final ChunkCoordinate pistonCoord = ChunkCoordinate.forChunk(event.getBlock().getChunk());
        final Town pistonTown = TownManager.get().getTownAt(pistonCoord);

        for (final Block block : event.getBlocks()) {
            final ChunkCoordinate blockCoord = ChunkCoordinate.forChunk(block.getChunk());
            final ChunkCoordinate targetCoord =
                    ChunkCoordinate.forChunk(block.getRelative(event.getDirection()).getChunk());
            final Town blockTown = TownManager.get().getTownAt(blockCoord);
            final Town targetTown = TownManager.get().getTownAt(targetCoord);

            if (blockTown != pistonTown || targetTown != blockTown) {
                event.setCancelled(true);
                return;
            }
        }
    }

    /**
     * Checks whether or not the given issuer may attack the given target.
     *
     * @param issuer The issuer.
     * @param target The target.
     * @return Whether or not attacking is allowed.
     */
    private static boolean mayAttack(final Player issuer, final Entity target) {
        // Whitelist certain entities.
        if (isWhitelistedEntity(target.getType())) {
            return true;
        }

        // Attacking self is ok.
        if (issuer == target) {
            return true;
        }

        // PVP check.
        if (target.getType() == EntityType.PLAYER) {
            final ChunkCoordinate coordTarget = ChunkCoordinate.forChunk(target.getLocation().getChunk());
            final ChunkCoordinate coordIssuer = ChunkCoordinate.forChunk(issuer.getLocation().getChunk());
            final Town targetTown = TownManager.get().getTownAt(coordTarget);
            final Town issuerTown = TownManager.get().getTownAt(coordIssuer);
            // Allow PVP iff both are in wilderness.
            return targetTown == null && issuerTown == null;
        }

        // Require permission for everything else.
        return TownManager.get().isAllowedByProtection(issuer, target.getLocation());
    }

    @EventHandler
    public void onEntityCombustByEntity(final EntityCombustByEntityEvent event) {
        // Only handle town worlds.
        if (!TownManager.get().isTownWorld(event.getEntity().getWorld())) {
            return;
        }

        final Player issuer = getPlayerCause(event.getCombuster());
        if (issuer == null) {
            // No player cause.
            return;
        }

        if (!mayAttack(issuer, event.getEntity())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(final EntityDamageByEntityEvent event) {
        // Only handle town worlds.
        if (!TownManager.get().isTownWorld(event.getEntity().getWorld())) {
            return;
        }

        if (event.getEntity() instanceof Wolf) {
            if (((Creature) event.getEntity()).getTarget() == event.getDamager()) {
                // Allow defense.
                return;
            }
        }

        // Prevent explosion griefing.
        if (event.getCause() == DamageCause.BLOCK_EXPLOSION || event.getCause() == DamageCause.ENTITY_EXPLOSION) {
            if (!(event.getEntity() instanceof Player)) {
                // Can only hurt players.
                event.setCancelled(true);
                return;
            }
        }

        final Player issuer = getPlayerCause(event.getDamager());
        if (issuer == null) {
            // No player cause.
            return;
        }

        // Check whether or not attacking is allowed.
        if (!mayAttack(issuer, event.getEntity())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent event) {
        // Allow non-block events.
        if (!event.hasBlock()) {
            return;
        }

        // Only ban certain blocks.
        switch (event.getClickedBlock().getType()) {
            case FIRE:
                if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
                    // OK.
                    return;
                }
                break;
            case GOLD_PLATE:
            case IRON_PLATE:
            case STONE_PLATE:
            case SOIL:
                if (event.getAction() != Action.PHYSICAL) {
                    // OK.
                    return;
                }
                break;
            case DISPENSER:
            case NOTE_BLOCK:
            case TNT:
            case CHEST:
            case FURNACE:
            case BURNING_FURNACE:
            case WOODEN_DOOR:
            case LEVER:
            case STONE_BUTTON:
            case JUKEBOX:
            case CAKE_BLOCK:
            case DIODE_BLOCK_OFF:
            case DIODE_BLOCK_ON:
            case TRAP_DOOR:
            case FENCE_GATE:
            case BREWING_STAND:
            case CAULDRON:
            case ENDER_PORTAL_FRAME:
            case DRAGON_EGG:
            case BEACON:
            case FLOWER_POT:
            case ANVIL:
            case TRAPPED_CHEST:
            case REDSTONE_COMPARATOR_OFF:
            case REDSTONE_COMPARATOR_ON:
            case DAYLIGHT_DETECTOR:
            case HOPPER:
            case DROPPER:
            case DAYLIGHT_DETECTOR_INVERTED:
            case SPRUCE_FENCE_GATE:
            case BIRCH_FENCE_GATE:
            case JUNGLE_FENCE_GATE:
            case DARK_OAK_FENCE_GATE:
            case ACACIA_FENCE_GATE:
            case SPRUCE_DOOR:
            case BIRCH_DOOR:
            case JUNGLE_DOOR:
            case ACACIA_DOOR:
            case DARK_OAK_DOOR:
            case WHITE_SHULKER_BOX:
            case ORANGE_SHULKER_BOX:
            case MAGENTA_SHULKER_BOX:
            case LIGHT_BLUE_SHULKER_BOX:
            case YELLOW_SHULKER_BOX:
            case LIME_SHULKER_BOX:
            case PINK_SHULKER_BOX:
            case GRAY_SHULKER_BOX:
            case SILVER_SHULKER_BOX:
            case CYAN_SHULKER_BOX:
            case PURPLE_SHULKER_BOX:
            case BLUE_SHULKER_BOX:
            case BROWN_SHULKER_BOX:
            case GREEN_SHULKER_BOX:
            case RED_SHULKER_BOX:
            case BLACK_SHULKER_BOX:
                break;
            default:
                return;
        }

        // Require permission for everything else.
        // Check takes care of world.
        if (!TownManager.get().isAllowedByProtection(event.getPlayer(), event.getClickedBlock().getLocation())) {
            event.setCancelled(true);
        }
    }
}
