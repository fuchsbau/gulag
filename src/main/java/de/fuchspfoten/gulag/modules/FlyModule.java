package de.fuchspfoten.gulag.modules;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.gulag.GulagPlugin;
import de.fuchspfoten.gulag.event.PlayerTownBorderCrossEvent;
import de.fuchspfoten.gulag.model.ChunkCoordinate;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

/**
 * This module grants fly-mode while in towns.
 */
public class FlyModule implements Listener {

    /**
     * The currently active flight-warnings.
     */
    private final Collection<UUID> flyWarnings = new HashSet<>();

    /**
     * Constructor.
     */
    public FlyModule() {
        Messenger.register("gulag.fly.warning");
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        if (event.getPlayer().getGameMode() != GameMode.SURVIVAL) {
            // Ignore non-survival-mode players.
            return;
        }

        if (!TownManager.get().isTownWorld(event.getPlayer().getWorld())) {
            return;
        }

        final ChunkCoordinate joinCoord = ChunkCoordinate.forChunk(event.getPlayer().getLocation().getChunk());
        final Town joinTown = TownManager.get().getTownAt(joinCoord);
        if (joinTown != null) {
            // Allow flight upon joining inside a town.
            event.getPlayer().setAllowFlight(true);
        }
    }

    @EventHandler
    public void onPlayerDeath(final PlayerDeathEvent event) {
        if (event.getEntity().getGameMode() != GameMode.SURVIVAL) {
            // Ignore non-survival-mode players.
            return;
        }

        if (TownManager.get().isTownWorld(event.getEntity().getWorld())) {
            event.getEntity().setAllowFlight(false);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerTeleport(final PlayerTeleportEvent event) {
        if (event.isCancelled()) {
            return;
        }

        if (event.getPlayer().getGameMode() != GameMode.SURVIVAL) {
            // Ignore non-survival-mode players.
            return;
        }

        final TownManager townMgr = TownManager.get();
        if (!townMgr.isTownWorld(event.getTo().getWorld()) && townMgr.isTownWorld(event.getFrom().getWorld())) {
            // Disable fly upon leaving the town world.
            event.getPlayer().setFlying(false);
            event.getPlayer().setAllowFlight(false);
        }
    }

    @EventHandler
    public void onPlayerTownBorderCross(final PlayerTownBorderCrossEvent event) {
        if (event.getPlayer().getGameMode() != GameMode.SURVIVAL) {
            // Ignore non-survival-mode players.
            return;
        }

        if (event.getTo() != null) {
            // Now in town, enforce flying.
            if (!event.getPlayer().getAllowFlight()) {
                event.getPlayer().setAllowFlight(true);
            }

            // Remove any flight-warning.
            flyWarnings.remove(event.getPlayer().getUniqueId());
        } else {
            // Now in wilderness, remove flight capability.
            if (event.getPlayer().isFlying()) {
                // Currently flying, allow grace period of 3 seconds to re-enter town.

                // Add a flight-warning for the player.
                flyWarnings.add(event.getPlayer().getUniqueId());
                Messenger.send(event.getPlayer(), "gulag.fly.warning");

                Bukkit.getScheduler().scheduleSyncDelayedTask(GulagPlugin.getSelf(), () -> {
                    // Disable flight if the flight-warning persists.
                    if (flyWarnings.contains(event.getPlayer().getUniqueId())) {
                        event.getPlayer().setFlying(false);
                        event.getPlayer().setAllowFlight(false);
                        flyWarnings.remove(event.getPlayer().getUniqueId());
                    }
                }, 3 * 20L);
            } else {
                // Not currently flying, just remove capability.
                event.getPlayer().setAllowFlight(false);
            }
        }
    }
}
