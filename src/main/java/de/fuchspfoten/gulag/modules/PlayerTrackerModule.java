package de.fuchspfoten.gulag.modules;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.gulag.GulagPlugin;
import de.fuchspfoten.gulag.event.PlayerTownBorderCrossEvent;
import de.fuchspfoten.gulag.model.ChunkCoordinate;
import de.fuchspfoten.gulag.model.Plot;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownManager;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

/**
 * Tracks the player's movements through the town world.
 */
public class PlayerTrackerModule implements Listener {

    /**
     * Constructor.
     */
    public PlayerTrackerModule() {
        Messenger.register("gulag.move.townEnterCity");
        Messenger.register("gulag.move.townEnterOwned");
        Messenger.register("gulag.move.townEnterForSale");
        Messenger.register("gulag.move.townEnterWild");
    }

    /**
     * Handles chunk changes by player movement/teleport.
     *
     * @param player The player changing chunks.
     * @param from   The source chunk.
     * @param to     The target chunk.
     */
    private static void onChunkChange(final Player player, final Chunk from, final Chunk to) {
        final ChunkCoordinate oldCoord = ChunkCoordinate.forChunk(from);
        final ChunkCoordinate newCoord = ChunkCoordinate.forChunk(to);
        final Town oldTown = TownManager.get().getTownAt(oldCoord);
        final Town newTown = TownManager.get().getTownAt(newCoord);

        if (oldTown == newTown) {
            // Intra-Town change.
            if (newTown != null) {
                // Not a wilderness-wilderness change.
                final Plot oldPlot = newTown.getPlotAt(oldCoord);
                final Plot newPlot = newTown.getPlotAt(newCoord);
                final boolean changedOwnedStatus = oldPlot.isOwned() != newPlot.isOwned();
                final boolean changedForSaleStatus = oldPlot.isForSale() != newPlot.isForSale();
                final boolean changedOwner = oldPlot.isOwned() && newPlot.isOwned()
                        && !oldPlot.getOwnerId().equals(newPlot.getOwnerId());
                if (changedOwnedStatus || changedForSaleStatus || changedOwner) {
                    // Different kind of plot.
                    if (newPlot.isOwned()) {
                        final OfflinePlayer who = Bukkit.getOfflinePlayer(newPlot.getOwnerId());
                        Messenger.send(player, "gulag.move.townEnterOwned", newTown.getName(), who.getName());
                    } else {
                        if (newPlot.isForSale()) {
                            Messenger.send(player, "gulag.move.townEnterForSale", newTown.getName(),
                                    GulagPlugin.getSelf().getEconomy().format(newPlot.getTax()));
                        } else {
                            Messenger.send(player, "gulag.move.townEnterCity", newTown.getName());
                        }
                    }
                }
            }
        } else {
            // Town change.
            final PlayerTownBorderCrossEvent crossEvent = new PlayerTownBorderCrossEvent(player, oldTown, newTown);
            Bukkit.getServer().getPluginManager().callEvent(crossEvent);

            if (newTown == null) {
                // To wilderness.
                Messenger.send(player, "gulag.move.townEnterWild");
            } else {
                // To a new town.
                final Plot newPlot = newTown.getPlotAt(newCoord);
                if (newPlot.isOwned()) {
                    final OfflinePlayer who = Bukkit.getOfflinePlayer(newPlot.getOwnerId());
                    Messenger.send(player, "gulag.move.townEnterOwned", newTown.getName(), who.getName());
                } else {
                    if (newPlot.isForSale()) {
                        Messenger.send(player, "gulag.move.townEnterForSale", newTown.getName(),
                                GulagPlugin.getSelf().getEconomy().format(newPlot.getTax()));
                    } else {
                        Messenger.send(player, "gulag.move.townEnterCity", newTown.getName());
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerTeleport(final PlayerTeleportEvent event) {
        if (event.isCancelled()) {
            return;
        }

        if (!TownManager.get().isTownWorld(event.getTo().getWorld())) {
            return;
        }

        // Only care for chunk changes.
        if (event.getFrom().getChunk() != event.getTo().getChunk()) {
            onChunkChange(event.getPlayer(), event.getFrom().getChunk(), event.getTo().getChunk());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerMove(final PlayerMoveEvent event) {
        if (event.isCancelled()) {
            return;
        }

        if (!TownManager.get().isTownWorld(event.getTo().getWorld())) {
            return;
        }

        // Only care for chunk changes.
        if (event.getFrom().getChunk() != event.getTo().getChunk()) {
            onChunkChange(event.getPlayer(), event.getFrom().getChunk(), event.getTo().getChunk());
        }
    }
}
