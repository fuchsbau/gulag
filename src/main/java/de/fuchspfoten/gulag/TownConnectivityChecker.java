package de.fuchspfoten.gulag;

import de.fuchspfoten.gulag.model.ChunkCoordinate;
import de.fuchspfoten.gulag.model.Plot;
import de.fuchspfoten.gulag.model.Town;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Checks the connectivity of towns.
 */
public final class TownConnectivityChecker {

    /**
     * Checks whether the town is still connected without the given chunk.
     *
     * @param town  The town.
     * @param coord The chunk.
     * @return Whether the town is still connected.
     */
    public static boolean isConnectedWithoutChunk(final Town town, final ChunkCoordinate coord) {
        if (town.getPlots().size() == 1) {
            return false;
        }

        // Create the unreached set and the open list.
        final Set<ChunkCoordinate> unreachedSet = town.getPlots().stream()
                .map(Plot::getCoordinate)
                .filter(x -> !x.equals(coord))
                .collect(Collectors.toSet());
        final Deque<ChunkCoordinate> openStack = new LinkedList<>();
        openStack.add(unreachedSet.stream().findAny().orElse(null));

        // Check connectivity.
        while (!openStack.isEmpty()) {
            // Mark the current chunk.
            final ChunkCoordinate current = openStack.pop();
            unreachedSet.remove(current);

            // Get potential neighbors.
            final int x = current.getX();
            final int z = current.getZ();
            final ChunkCoordinate coordNorth = new ChunkCoordinate(x, z - 1);
            final ChunkCoordinate coordWest = new ChunkCoordinate(x - 1, z);
            final ChunkCoordinate coordSouth = new ChunkCoordinate(x, z + 1);
            final ChunkCoordinate coordEast = new ChunkCoordinate(x + 1, z);

            // Add neighbors that are not removed and still unreached.
            if (coordNorth != coord && unreachedSet.contains(coordNorth)) {
                openStack.push(coordNorth);
            }
            if (coordWest != coord && unreachedSet.contains(coordWest)) {
                openStack.push(coordWest);
            }
            if (coordSouth != coord && unreachedSet.contains(coordSouth)) {
                openStack.push(coordSouth);
            }
            if (coordEast != coord && unreachedSet.contains(coordEast)) {
                openStack.push(coordEast);
            }
        }

        return unreachedSet.isEmpty();
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private TownConnectivityChecker() {
    }
}
