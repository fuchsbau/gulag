package de.fuchspfoten.gulag.model;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.StringSerializer;
import de.fuchspfoten.fuchslib.data.DataFile;
import de.fuchspfoten.gulag.GulagPlugin;
import lombok.Getter;
import lombok.Setter;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Represents a town.
 */
public final class Town {

    /**
     * Creates a town with the given name.
     *
     * @param name  The town name.
     * @param at    The initial chunk.
     * @param mayor The mayor.
     * @return The town.
     */
    static Town create(final String name, final ChunkCoordinate at, final UUID mayor) {
        // Pre-configure the town.
        final DataFile townData = new DataFile(new File(GulagPlugin.getSelf().getDataFolder(),
                "data/" + name + ".yml"), new TownDataUpdater());
        townData.getStorage().set("name", name);
        townData.getStorage().set("mayor", mayor.toString());
        townData.getStorage().set("balance", TownManager.get().getCost("upkeep") * 2);
        townData.getStorage().set("tax", 30);

        // Load the town and modify the data structures.
        final Town town = load(townData);
        town.residentIds.add(mayor);
        town.plotMap.put(at, new Plot(at, null, town.getTax()));

        // Save the town for the first time.
        town.save();

        return town;
    }

    /**
     * Loads a town from the given data file.
     *
     * @param dataFile The data file.
     * @return The town.
     */
    static Town load(final DataFile dataFile) {
        final String name = dataFile.getStorage().getString("name");
        final UUID mayorId = UUID.fromString(dataFile.getStorage().getString("mayor"));
        final Town result = new Town(dataFile, name, mayorId);

        // Load balance and tax.
        result.balance = dataFile.getStorage().getInt("balance");
        result.tax = dataFile.getStorage().getInt("tax");

        // Load the spawn.
        final String spawnStr = dataFile.getStorage().getString("spawn");
        if (spawnStr != null) {
            result.spawn = StringSerializer.parseLocation(spawnStr);
        }

        // Add the residents.
        final List<String> residentIds = dataFile.getStorage().getStringList("residents");
        residentIds.stream().map(UUID::fromString).forEach(result.residentIds::add);

        // Add the plots.
        final ConfigurationSection plotsSection = dataFile.getStorage().getConfigurationSection("plots");
        if (plotsSection != null) {
            for (final String key : plotsSection.getKeys(false)) {
                final ConfigurationSection plotSection = plotsSection.getConfigurationSection(key);

                // Get coordinates and owner.
                final ChunkCoordinate coord = new ChunkCoordinate(plotSection.getInt("x"),
                        plotSection.getInt("z"));
                final String ownerId = plotSection.getString("owner", null);
                final int tax = plotSection.getInt("tax", result.getTax());
                final Plot plot = new Plot(coord, ownerId == null ? null : UUID.fromString(ownerId), tax);

                // Get the members of the plot.
                final List<String> memberIds = plotSection.getStringList("members");
                memberIds.stream().map(UUID::fromString).forEach(plot::addMember);

                // Check whether the plot is for sale.
                plot.setForSale(plotSection.getBoolean("forsale"));

                // Store the plot in the town.
                result.plotMap.put(coord, plot);
            }
        }

        // Add the ranks.
        final ConfigurationSection ranksSection = dataFile.getStorage().getConfigurationSection("ranks");
        if (ranksSection != null) {
            for (final String key : ranksSection.getKeys(false)) {
                final ConfigurationSection rankSection = ranksSection.getConfigurationSection(key);
                final Rank rank = new Rank(rankSection.getString("name"));

                // Get permissions.
                final List<String> permissions = rankSection.getStringList("perms");
                permissions.forEach(rank::addPermission);

                // Get members.
                final List<UUID> members = rankSection.getStringList("members").stream()
                        .map(UUID::fromString)
                        .collect(Collectors.toList());
                members.forEach(m -> {
                    rank.addMember(m);
                    result.rankResolver.put(m, rank);
                });

                // Add the rank to the rank list.
                result.ranks.add(rank);
            }
        }

        return result;
    }

    /**
     * The town data file.
     */
    private final DataFile dataFile;

    /**
     * The name of the town.
     */
    private @Getter final String name;

    /**
     * The UUID of the mayor.
     */
    private @Getter @Setter UUID mayorId;

    /**
     * The balance of the town bank.
     */
    private @Getter int balance;

    /**
     * The town spawn.
     */
    private @Getter @Setter Location spawn;

    /**
     * The tax for the residents of the town.
     */
    private @Getter @Setter int tax;

    /**
     * The UUIDs of the residents of the town.
     */
    private final Set<UUID> residentIds = new HashSet<>();

    /**
     * The ranks this town has.
     */
    private final Set<Rank> ranks = new HashSet<>();

    /**
     * Maps resident IDs to their rank.
     */
    private final Map<UUID, Rank> rankResolver = new HashMap<>();

    /**
     * A mapping that maps chunk coordinates to town plots.
     */
    private final Map<ChunkCoordinate, Plot> plotMap = new HashMap<>();

    /**
     * The set of invited IDs.
     */
    private final Collection<UUID> inviteIds = new HashSet<>();

    /**
     * Constructor.
     *
     * @param name    The name of the town.
     * @param mayorId The mayor of the town.
     */
    private Town(final DataFile dataFile, final String name, final UUID mayorId) {
        this.dataFile = dataFile;
        this.name = name;
        this.mayorId = mayorId;
    }

    /**
     * Creates a rank with the given name.
     *
     * @param name The name.
     */
    public void createRank(final String name) {
        ranks.add(new Rank(name));
    }

    /**
     * Executes a task for all town residents that are online.
     *
     * @param task The task.
     */
    public void forEachOnline(final Consumer<Player> task) {
        residentIds.stream()
                .map(Bukkit::getOfflinePlayer)
                .filter(OfflinePlayer::isOnline)
                .map(OfflinePlayer::getPlayer)
                .forEach(task);
    }

    /**
     * Get the plot at the given coordinate.
     *
     * @param coord The coordinate.
     * @return The plot.
     */
    public Plot getPlotAt(final ChunkCoordinate coord) {
        return plotMap.get(coord);
    }

    /**
     * Fetches all plots of the town.
     *
     * @return The plots.
     */
    public Collection<Plot> getPlots() {
        return Collections.unmodifiableCollection(plotMap.values());
    }

    /**
     * Get the rank with the given name.
     *
     * @param name The name.
     * @return The rank, or null if it does not exist.
     */
    public Rank getRank(final String name) {
        return ranks.stream().filter(x -> x.getName().equalsIgnoreCase(name)).findAny().orElse(null);
    }

    /**
     * Get the rank of the given ID.
     *
     * @param who The ID.
     * @return The rank.
     */
    public Rank getRankOf(final UUID who) {
        return rankResolver.get(who);
    }

    /**
     * Returns the ranks in the town.
     *
     * @return The ranks in the town.
     */
    public Set<Rank> getRanks() {
        return Collections.unmodifiableSet(ranks);
    }

    /**
     * Returns the IDs of the residents of the town.
     *
     * @return The residents.
     */
    public Collection<UUID> getResidentIDs() {
        return Collections.unmodifiableSet(residentIds);
    }

    /**
     * Checks whether or not the given player has the given permission in this town. Notifies the player about failures.
     *
     * @param who        The player.
     * @param permission The permission.
     * @return Whether the player has the given permission.
     */
    public boolean hasPermission(final Entity who, final TownPermission permission) {
        if (!hasPermission(who.getUniqueId(), permission)) {
            // Notify the player of the failure.
            Messenger.send(who, "gulag.townPermission", permission);
            return false;
        }
        return true;
    }

    /**
     * Checks whether or not the given player has the given permission in this town. Notifies the player about failures.
     *
     * @param who        The player.
     * @param permission The permission.
     * @return Whether the player has the given permission.
     */
    public boolean hasPermission(final UUID who, final TownPermission permission) {
        // Stub-check: Mayors have all permissions.
        if (mayorId.equals(who)) {
            return true;
        }

        // Determine rank permissions.
        final Rank userRank = rankResolver.get(who);
        if (userRank == null) {
            // Users have no permissions by default.
            return false;
        }

        // The user has the permission if their rank has the permission.
        return userRank.getPermissions().contains(permission.getNode());
    }

    /**
     * Invites the given ID.
     *
     * @param id The id.
     */
    public void invite(final UUID id) {
        inviteIds.add(id);
    }

    /**
     * Checks if the given ID is invited to the town.
     *
     * @param id The ID.
     * @return Whether the ID is invited.
     */
    public boolean isInvited(final UUID id) {
        return inviteIds.contains(id);
    }

    /**
     * Changes the balance of this town.
     *
     * @param change The amount to add.
     */
    public void modifyBalance(final int change) {
        balance += change;
    }

    /**
     * Remove a rank.
     *
     * @param name The name of the rank.
     */
    public void removeRank(final String name) {
        final Rank which = getRank(name);
        if (which != null) {
            ranks.remove(which);

            // Remove all members.
            final Set<UUID> forRemove = rankResolver.entrySet().stream()
                    .filter(x -> x.getValue() == which)
                    .map(Entry::getKey)
                    .collect(Collectors.toSet());
            forRemove.forEach(rankResolver::remove);

            // Remove the key from the data file.
            final String key = "ranks." + UUID.nameUUIDFromBytes(which.getName().getBytes());
            dataFile.getStorage().set(key, null);
        }
    }

    /**
     * Runs taxes for this town.
     */
    public void runTaxes() {
        // Compute plot taxes for residents.
        final Map<UUID, Integer> duePayments = new HashMap<>();
        final Map<UUID, Set<Plot>> ownedPlots = new HashMap<>();
        for (final Plot plot : plotMap.values()) {
            if (plot.isOwned()) {
                if (duePayments.containsKey(plot.getOwnerId())) {
                    duePayments.put(plot.getOwnerId(), duePayments.get(plot.getOwnerId()) + plot.getTax());
                } else {
                    duePayments.put(plot.getOwnerId(), plot.getTax());
                    ownedPlots.put(plot.getOwnerId(), new HashSet<>());
                }
                ownedPlots.get(plot.getOwnerId()).add(plot);
            }
        }

        // Collect plot taxes from residents.
        final Economy eco = GulagPlugin.getSelf().getEconomy();
        for (final Entry<UUID, Integer> duePayment : duePayments.entrySet()) {
            final OfflinePlayer account = Bukkit.getOfflinePlayer(duePayment.getKey());
            final int owed = duePayment.getValue();
            if (eco.has(account, owed)) {
                // Withdraw the owed amount and add it to the balance.
                eco.withdrawPlayer(account, owed);
                balance += owed;
            } else {
                // Seize all plots.
                for (final Plot plot : ownedPlots.get(duePayment.getKey())) {
                    plot.setOwnerId(null);
                    for (final UUID member : plot.getMemberIDs()) {
                        plot.removeMember(member);
                    }
                }
                forEachOnline(p -> Messenger.send(p, "gulag.lostPlots", account.getName()));
            }
        }

        // Collect server tax from the balance.
        final int owed = plotMap.size() * TownManager.get().getCost("upkeep");
        if (balance < owed) {
            // Delete town.
            Bukkit.getOnlinePlayers().forEach(p -> Messenger.send(p, "gulag.lostTown", name));
            TownManager.get().removeTown(this);
        } else {
            balance -= owed;
            forEachOnline(p -> Messenger.send(p, "gulag.taxCollected"));
        }
    }

    /**
     * Sets the rank of a player in the town.
     *
     * @param who  The player.
     * @param rank The rank.
     */
    public void setRank(final UUID who, final Rank rank) {
        if (rank == null) {
            // Demote.
            final Rank old = rankResolver.remove(who);
            if (old != null) {
                old.removeMember(who);
            }
        } else {
            // Promote.
            final Rank old = rankResolver.put(who, rank);
            if (old != null) {
                old.removeMember(who);
            }
            rank.addMember(who);
        }
    }

    /**
     * Claims the given chunk for the town.
     *
     * @param coord The chunk.
     */
    void claim(final ChunkCoordinate coord) {
        plotMap.put(coord, new Plot(coord, null, tax));
    }

    /**
     * Deletes the town.
     */
    void delete() {
        dataFile.delete();
    }

    /**
     * Lets the given ID join the town.
     *
     * @param who The ID.
     */
    void join(final UUID who) {
        residentIds.add(who);
        inviteIds.remove(who);
    }

    /**
     * Lets the given ID leave the town.
     *
     * @param who The ID.
     */
    void leave(final UUID who) {
        residentIds.remove(who);
        for (final Plot plot : plotMap.values()) {
            if (plot.getOwnerId() == who) {
                plot.setOwnerId(null);
            }
            plot.removeMember(who);
        }

        // Remove the ID from all ranks.
        rankResolver.remove(who);
        for (final Rank rank : ranks) {
            rank.removeMember(who);
        }
    }

    /**
     * Saves the town to the data file and then saves the data file.
     */
    void save() {
        dataFile.getStorage().set("mayor", mayorId.toString());

        // Save balance and tax.
        dataFile.getStorage().set("balance", balance);
        dataFile.getStorage().set("tax", tax);

        // Save the spawn.
        if (spawn != null) {
            dataFile.getStorage().set("spawn", StringSerializer.toString(spawn));
        } else {
            dataFile.getStorage().set("spawn", null);
        }

        // Save the residents.
        final List<String> residents = residentIds.stream().map(UUID::toString).collect(Collectors.toList());
        dataFile.getStorage().set("residents", residents);

        // Save the plots.
        for (final Plot plot : plotMap.values()) {
            final String key = "plots." + plot.getCoordinate();
            final List<String> members = plot.getMemberIDs().stream().map(UUID::toString).collect(Collectors.toList());
            dataFile.getStorage().set(key + ".x", plot.getCoordinate().getX());
            dataFile.getStorage().set(key + ".z", plot.getCoordinate().getZ());
            dataFile.getStorage().set(key + ".owner", plot.isOwned() ? plot.getOwnerId().toString() : null);
            dataFile.getStorage().set(key + ".members", members);
            dataFile.getStorage().set(key + ".forsale", plot.isForSale());
            dataFile.getStorage().set(key + ".tax", plot.getTax());
        }

        // Save the ranks.
        for (final Rank rank : ranks) {
            final String key = "ranks." + UUID.nameUUIDFromBytes(rank.getName().getBytes());
            final List<String> permissions = new ArrayList<>(rank.getPermissions());
            dataFile.getStorage().set(key + ".name", rank.getName());
            dataFile.getStorage().set(key + ".perms", permissions);
            final List<String> members = rank.getMemberIds().stream().map(UUID::toString).collect(Collectors.toList());
            dataFile.getStorage().set(key + ".members", members);
        }

        // Save the backend file.
        dataFile.save(TownDataUpdater.VERSION);
    }

    /**
     * Unclaims the given chunk from the town.
     *
     * @param coord The chunk.
     */
    void unclaim(final ChunkCoordinate coord) {
        plotMap.remove(coord);

        // Remove the key from the data file.
        final String key = "plots." + coord;
        dataFile.getStorage().set(key, null);
    }
}
