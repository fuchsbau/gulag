package de.fuchspfoten.gulag.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.Chunk;

/**
 * Represents a chunk coordinate pair (x,z).
 */
@RequiredArgsConstructor
@Getter
@EqualsAndHashCode
public class ChunkCoordinate {

    /**
     * Creates chunk coordinates for the given chunk.
     *
     * @param chunk The chunk.
     * @return The created coordinate.
     */
    public static ChunkCoordinate forChunk(final Chunk chunk) {
        return new ChunkCoordinate(chunk.getX(), chunk.getZ());
    }

    /**
     * The x coordinate.
     */
    private final int x;

    /**
     * The z coordinate.
     */
    private final int z;

    @Override
    public String toString() {
        return x + "_" + z;
    }
}
