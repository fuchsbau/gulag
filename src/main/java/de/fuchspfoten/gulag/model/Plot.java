package de.fuchspfoten.gulag.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Represents a town plot.
 */
public class Plot {

    /**
     * The chunk coordinate of this plot.
     */
    private @Getter final ChunkCoordinate coordinate;

    /**
     * The UUIDs of the members on the plot.
     */
    private final Set<UUID> memberIds = new HashSet<>();

    /**
     * The ID of the owner of this plot, if any.
     */
    private @Getter @Setter UUID ownerId;

    /**
     * Whether the plot is for sale. If true, then ownerId must be null.
     */
    private @Getter @Setter boolean forSale;

    /**
     * The tax of the plot.
     */
    private @Getter @Setter int tax;

    /**
     * Constructor.
     *
     * @param coordinate The chunk coordinate.
     * @param ownerId    The plot owner's UUID.
     * @param tax        The tax of this plot.
     */
    public Plot(final ChunkCoordinate coordinate, final UUID ownerId, final int tax) {
        this.coordinate = coordinate;
        this.ownerId = ownerId;
        this.tax = tax;
        forSale = false;
    }

    /**
     * Adds a member to the plot.
     *
     * @param memberId The ID of the member.
     */
    public void addMember(final UUID memberId) {
        memberIds.add(memberId);
    }

    /**
     * Fetches the member IDs.
     *
     * @return The member IDs.
     */
    public Collection<UUID> getMemberIDs() {
        return Collections.unmodifiableSet(memberIds);
    }

    /**
     * Checks whether the plot is owned.
     *
     * @return Whether the plot is owned.
     */
    public boolean isOwned() {
        return ownerId != null;
    }

    /**
     * Removes a member from the plot.
     *
     * @param memberId The ID of the member.
     */
    public void removeMember(final UUID memberId) {
        memberIds.remove(memberId);
    }
}
