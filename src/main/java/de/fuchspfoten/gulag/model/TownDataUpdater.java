package de.fuchspfoten.gulag.model;

import de.fuchspfoten.fuchslib.data.DataFile;

import java.util.function.BiConsumer;

/**
 * Takes care of updating town data across plugin versions.
 */
public class TownDataUpdater implements BiConsumer<Integer, DataFile> {

    /**
     * The current data version.
     */
    public static final int VERSION = 0;

    @Override
    public void accept(final Integer oldVersion, final DataFile dataFile) {
        // Do nothing.
    }
}
