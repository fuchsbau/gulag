package de.fuchspfoten.gulag.model;

import de.fuchspfoten.fuchslib.data.DataFile;
import de.fuchspfoten.gulag.GulagPlugin;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.UUID;

/**
 * Manages the existing towns and global town configuration.
 */
public class TownManager {

    /**
     * Convenience method to fetch the singleton town manager.
     *
     * @return The town manager.
     */
    public static TownManager get() {
        return GulagPlugin.getSelf().getTownManager();
    }

    /**
     * The town world.
     */
    private String townWorld;

    /**
     * Maps operations to their cost.
     */
    private final Map<String, Integer> costs = new HashMap<>();

    /**
     * The number of plots per resident.
     */
    private @Getter int plotsPerResident;

    /**
     * The maximum tax.
     */
    private @Getter int maximumTax;

    /**
     * Maps town names to town models.
     */
    private final Map<String, Town> townByName = new HashMap<>();

    /**
     * The towns in saving order.
     */
    private final Deque<Town> savingQueue = new ArrayDeque<>();

    /**
     * Maps resident UUIDs to town models.
     */
    private final Map<UUID, Town> townByResident = new HashMap<>();

    /**
     * Maps chunk coordinates to town models.
     */
    private final Map<ChunkCoordinate, Town> townByChunk = new HashMap<>();

    /**
     * Claims a chunk for the given town.
     *
     * @param town  The town.
     * @param coord The coordinate.
     */
    public void claimChunk(final Town town, final ChunkCoordinate coord) {
        if (getTownAt(coord) != null) {
            throw new IllegalArgumentException("already claimed");
        }
        townByChunk.put(coord, town);
        town.claim(coord);
    }

    /**
     * Creates a town.
     *
     * @param name  The name of the town.
     * @param at    The position the town is at.
     * @param mayor The mayor of the town.
     * @return The town.
     */
    public Town createTown(final String name, final ChunkCoordinate at, final UUID mayor) {
        if (getTown(name) != null || getTownAt(at) != null || getTownOf(Bukkit.getOfflinePlayer(mayor)) != null) {
            throw new IllegalArgumentException("can not create town: duplicate name, chunk, or mayor");
        }
        final Town town = Town.create(name, at, mayor);
        savingQueue.addLast(town);
        townByName.put(town.getName(), town);
        townByResident.put(mayor, town);
        townByChunk.put(at, town);
        return town;
    }

    /**
     * Fetches the cost set for the given key.
     *
     * @param key The key.
     * @return The cost.
     */
    public int getCost(final String key) {
        return costs.get(key);
    }

    /**
     * Get the town with the given name.
     *
     * @param name The name.
     * @return The town or null.
     */
    public Town getTown(final String name) {
        return townByName.entrySet().stream()
                .filter(x -> x.getKey().equalsIgnoreCase(name))
                .map(Entry::getValue)
                .findAny()
                .orElse(null);
    }

    /**
     * Get the town at the given chunk coordinate.
     *
     * @param coord The chunk coordinate.
     * @return The town or null.
     */
    public Town getTownAt(final ChunkCoordinate coord) {
        return townByChunk.get(coord);
    }

    /**
     * Get the town of the given player.
     *
     * @param player The player.
     * @return The town or null.
     */
    public Town getTownOf(final OfflinePlayer player) {
        return townByResident.get(player.getUniqueId());
    }

    /**
     * Fetches the existing towns.
     *
     * @return The towns.
     */
    public Collection<Town> getTowns() {
        return Collections.unmodifiableCollection(townByName.values());
    }

    /**
     * Checks whether an action is allowed by the protection.
     *
     * @param who   The player that performs the action.
     * @param where The location where the action takes place.
     * @return Whether the action is allowed.
     */
    public boolean isAllowedByProtection(final Player who, final Location where) {
        // Only handle town worlds.
        if (!isTownWorld(where.getWorld())) {
            return true;
        }

        // Allow everything done by operators.
        if (who.isOp()) {
            return true;
        }

        // Allow everything done in creative mode.
        if (who.getGameMode() == GameMode.CREATIVE) {
            return true;
        }

        // Get the town where the action takes place.
        final ChunkCoordinate actionCoord = ChunkCoordinate.forChunk(where.getChunk());
        final Town actionTown = getTownAt(actionCoord);

        // Disallow everything in the wilderness.
        if (actionTown == null) {
            return false;
        }

        // Get the town where the actor is in.
        final Town actorTown = getTownOf(who);

        // Different towns.
        if (actionTown != actorTown) {
            // Only allow if member of the plot.
            final Plot actionPlot = actionTown.getPlotAt(actionCoord);

            // Allow members.
            return actionPlot.getMemberIDs().contains(who.getUniqueId());
        }

        // Check plot ownership.
        final Plot actionPlot = actionTown.getPlotAt(actionCoord);
        if (actionPlot.isOwned()) {
            if (who.getUniqueId().equals(actionPlot.getOwnerId())) {
                // Is owned by actor.
                return true;
            }

            // Use UUID to check silently.
            if (actorTown.hasPermission(who.getUniqueId(), TownPermission.PROTECTION_OWNED)) {
                // Has owned-protection override.
                return true;
            }
        } else {
            if (actorTown.hasPermission(who.getUniqueId(), TownPermission.PROTECTION_CITY)) {
                // Has city-protection override.
                return true;
            }
        }

        // Check plot membership.
        return actionPlot.getMemberIDs().contains(who.getUniqueId());
    }

    /**
     * Checks whether the given world is the town world.
     *
     * @param world The world.
     * @return Whether it is the town world.
     */
    public boolean isTownWorld(final World world) {
        return world.getName().equalsIgnoreCase(townWorld);
    }

    /**
     * Makes a player join a town.
     *
     * @param town The town.
     * @param who  The player.
     */
    public void joinTown(final Town town, final UUID who) {
        if (getTownOf(Bukkit.getOfflinePlayer(who)) != null) {
            throw new IllegalArgumentException("already in town");
        }
        townByResident.put(who, town);
        town.join(who);
    }

    /**
     * Makes a player leave a town.
     *
     * @param who The player.
     */
    public void leaveTown(final UUID who) {
        final Town town = townByResident.get(who);
        if (town == null) {
            throw new IllegalArgumentException("not in town");
        }
        if (town.getMayorId().equals(who)) {
            throw new IllegalArgumentException("target is mayor");
        }
        townByResident.remove(who);
        town.leave(who);
    }

    /**
     * Loads the town data from the given directory.
     *
     * @param pluginConfig The plugin configuration.
     * @param dataDir      The directory.
     */
    public void load(final ConfigurationSection pluginConfig, final File dataDir) {
        townWorld = pluginConfig.getString("world");
        plotsPerResident = pluginConfig.getInt("plotsPerResident");
        maximumTax = pluginConfig.getInt("maxTax");

        // Load costs.
        final ConfigurationSection costSection = pluginConfig.getConfigurationSection("costs");
        for (final String operation : costSection.getKeys(false)) {
            costs.put(operation, costSection.getInt(operation));
        }

        // Load towns.
        for (final File townFile : Objects.requireNonNull(dataDir.listFiles())) {
            if (townFile.isDirectory()) {
                continue;
            }

            final DataFile townData = new DataFile(townFile, new TownDataUpdater());
            final Town town = Town.load(townData);

            // Enter the town in the respective resolver maps.
            savingQueue.addLast(town);
            townByName.put(town.getName(), town);
            for (final UUID resident : town.getResidentIDs()) {
                townByResident.put(resident, town);
            }
            for (final Plot plot : town.getPlots()) {
                townByChunk.put(plot.getCoordinate(), town);
            }
        }
    }

    /**
     * Removes the given town.
     *
     * @param town The town.
     */
    public void removeTown(final Town town) {
        townByName.remove(town.getName());
        for (final UUID resident : town.getResidentIDs()) {
            townByResident.remove(resident);
        }
        for (final Plot plot : town.getPlots()) {
            townByChunk.remove(plot.getCoordinate());
        }
        town.delete();
    }

    /**
     * Saves all towns.
     */
    public void saveAll() {
        for (final Town town : townByName.values()) {
            town.save();
        }
    }

    /**
     * Saves some of the towns.
     */
    public void save() {
        if (savingQueue.isEmpty()) {
            return;
        }

        // Allocate 15ms to saving.
        final long start = System.currentTimeMillis();
        int k = 0;
        while (System.currentTimeMillis() - start < 15) {
            if (k == townByName.size()) {
                // Saved every town.
                break;
            }

            final Town town = savingQueue.poll();
            if (town != null && townByName.containsKey(town.getName())) {
                town.save();
                savingQueue.addLast(town);
            }
            k++;
        }
    }

    /**
     * Unclaims a chunk.
     *
     * @param coord The coordinate.
     */
    public void unclaimChunk(final ChunkCoordinate coord) {
        final Town town = townByChunk.get(coord);
        if (town == null) {
            throw new IllegalArgumentException("not claimed");
        }
        if (town.getPlots().size() == 1) {
            throw new IllegalArgumentException("last chunk can not be unclaimed");
        }
        townByChunk.remove(coord);
        town.unclaim(coord);
    }
}
