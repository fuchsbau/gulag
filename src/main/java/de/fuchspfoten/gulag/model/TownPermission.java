package de.fuchspfoten.gulag.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Town permissions.
 */
@RequiredArgsConstructor
public enum TownPermission {

    /**
     * Claim chunks for the town.
     */
    CLAIM("claim"),

    /**
     * Clear owned plots.
     */
    CLEAR_OWNED("clearOwned"),

    /**
     * Invite players.
     */
    INVITE("invite"),

    /**
     * Kick other players.
     */
    KICK("kick"),

    /**
     * Modify plot members of plots not owned by the player.
     */
    MODIFY_MEMBERS("modifyMembers"),

    /**
     * Bypass any intra-town city-protection.
     */
    PROTECTION_CITY("protectionCity"),

    /**
     * Bypass any intra-town owned-protection.
     */
    PROTECTION_OWNED("protectionOwned"),

    /**
     * Assign lower ranks to players.
     */
    RANK_ASSIGN("rankAssign"),

    /**
     * Assign all ranks to players.
     */
    RANK_ASSIGN_ALL("rankAssignAll"),

    /**
     * Manage existing lower ranks.
     */
    RANK_MANAGE("rankManage"),

    /**
     * Manage all existing ranks.
     */
    RANK_MANAGE_ALL("rankManageAll"),

    /**
     * Sell a unsold plot.
     */
    SELL_PLOT("sellPlot"),

    /**
     * Set the spawn point.
     */
    SET_SPAWN("setSpawn"),

    /**
     * Set the town tax.
     */
    SET_TAX("setTax"),

    /**
     * Takes a plot down from sale.
     */
    TAKEBACK_PLOT("takebackPlot"),

    /**
     * Unclaim chunks of the town.
     */
    UNCLAIM("unclaim"),

    /**
     * Makes the player unkickable.
     */
    UNKICKABLE("unkickable");

    /**
     * Get a town permission by its node.
     *
     * @param node The node.
     * @return The permission, or null.
     */
    public static TownPermission byNode(final String node) {
        for (final TownPermission tp : values()) {
            if (tp.node.equalsIgnoreCase(node)) {
                return tp;
            }
        }
        return null;
    }

    /**
     * The node used in save files.
     */
    private @Getter final String node;
}
