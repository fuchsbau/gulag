package de.fuchspfoten.gulag.model;

import lombok.Getter;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Represents a town rank.
 */
public class Rank {

    /**
     * The name of the rank.
     */
    private @Getter final String name;

    /**
     * The UUIDs of the members with this rank.
     */
    private final Set<UUID> memberIds = new HashSet<>();

    /**
     * The permissions that this rank has.
     */
    private final Set<String> permissions = new HashSet<>();

    /**
     * Constructor.
     *
     * @param name The name of the rank
     */
    public Rank(final String name) {
        this.name = name;
    }

    /**
     * Adds a member to the rank.
     *
     * @param memberId The ID of the member.
     */
    public void addMember(final UUID memberId) {
        memberIds.add(memberId);
    }

    /**
     * Adds a permission to a rank.
     *
     * @param permission The permission.
     */
    public void addPermission(final String permission) {
        permissions.add(permission);
    }

    /**
     * Get the members in this rank.
     *
     * @return The members.
     */
    public Set<UUID> getMemberIds() {
        return Collections.unmodifiableSet(memberIds);
    }

    /**
     * Get the permissions this rank has.
     *
     * @return The permissions.
     */
    public Set<String> getPermissions() {
        return Collections.unmodifiableSet(permissions);
    }

    /**
     * Checks whether a rank is lower than another one, i.e. not the same and has a subset of the permissions.
     *
     * @param other The other rank.
     * @return Whether this rank is a lower rank than the other.
     */
    public boolean isLowerThan(final Rank other) {
        // Same rank -> Not lower.
        if (this == other) {
            return false;
        }

        // Check that all permissions in this rank are also in the other.
        for (final String perm : permissions) {
            if (!other.permissions.contains(perm)) {
                return false;
            }
        }

        // For this rank to be lower, the other rank must have a permission that this rank does not have.
        // At this point it is enough to just check the sizes.
        return permissions.size() < other.permissions.size();
    }

    /**
     * Removes a member from the rank.
     *
     * @param memberId The ID of the member.
     */
    public void removeMember(final UUID memberId) {
        memberIds.remove(memberId);
    }

    /**
     * Removes a permission from this rank.
     *
     * @param permission The permission.
     */
    public void removePermission(final String permission) {
        permissions.remove(permission);
    }
}
