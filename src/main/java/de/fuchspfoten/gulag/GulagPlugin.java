package de.fuchspfoten.gulag;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.gulag.command.TownCommand;
import de.fuchspfoten.gulag.model.TownManager;
import de.fuchspfoten.gulag.modules.FlyModule;
import de.fuchspfoten.gulag.modules.PlayerTrackerModule;
import de.fuchspfoten.gulag.modules.ProtectionModule;
import lombok.Getter;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

/**
 * The main plugin class.
 */
public class GulagPlugin extends JavaPlugin {

    /**
     * The singleton plugin instance.
     */
    private @Getter static GulagPlugin self;

    /**
     * The economy API.
     */
    private @Getter Economy economy;

    /**
     * The town manager.
     */
    private @Getter TownManager townManager;

    @Override
    public void onDisable() {
        townManager.saveAll();
        getLogger().info("Saved all towns for shutdown.");
    }

    @Override
    public void onEnable() {
        self = this;

        // Register messages.
        Messenger.register("gulag.lostPlots");
        Messenger.register("gulag.lostTown");
        Messenger.register("gulag.taxCollected");
        Messenger.register("gulag.townPermission");
        Messenger.register("gulag.townPermissionMayor");
        TownConditionHelper.registerMessages();

        // Load the economy.
        economy = getServer().getServicesManager().load(Economy.class);
        if (economy == null) {
            throw new IllegalStateException("economy not present during enable!");
        }

        // Create the default configuration.
        getConfig().options().copyDefaults(true);
        saveConfig();

        // Create the data directory for the towns, if needed.
        final File dataDir = new File(getDataFolder(), "data");
        if (!dataDir.exists()) {
            if (!dataDir.mkdir()) {
                throw new IllegalStateException("Could not create data directory: failure");
            }
        }

        // Create the town manager and load all towns.
        townManager = new TownManager();
        townManager.load(getConfig(), dataDir);

        // Register command executor modules.
        getCommand("town").setExecutor(new TownCommand());

        // Register modules.
        getServer().getPluginManager().registerEvents(new FlyModule(), this);
        getServer().getPluginManager().registerEvents(new PlayerTrackerModule(), this);
        getServer().getPluginManager().registerEvents(new ProtectionModule(), this);

        getServer().getScheduler().scheduleSyncRepeatingTask(this, () -> townManager.save(), 60 * 20L,
                60 * 20L);
    }
}
