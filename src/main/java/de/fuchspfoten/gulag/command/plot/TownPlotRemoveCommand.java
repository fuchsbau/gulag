package de.fuchspfoten.gulag.command.plot;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Plot;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownPermission;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town plot remove &lt;name&gt; command.
 */
public class TownPlotRemoveCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownPlotRemoveCommand() {
        super("gulag.townHelp.plot.remove", "gulag.use");
        Messenger.register("gulag.commandPlotRemove.success");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "gulag.townHelp.plot.remove");
            return;
        }
        final String name = args[0];

        // Check that the following conditions hold:
        //  - Actor is in town world.
        //  - Actor is in town.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireActorInTownWorld()
                .requireActorInTown()
                .requireActorChunkIsOwnedByActorTown();
        if (condition.isFailure()) {
            return;
        }

        final Town town = condition.getActorTown();
        final Plot plot = town.getPlotAt(condition.getActorCoord());

        // Fetch the target.
        final OfflinePlayer target = PlayerHelper.requireSeenOfflinePlayerByName(sender, name);
        if (target == null) {
            return;
        }

        // Check the permissions.
        final boolean allowed;
        if (plot.isOwned() && plot.getOwnerId().equals(player.getUniqueId())) {
            allowed = true;
        } else {
            allowed = town.hasPermission(player, TownPermission.MODIFY_MEMBERS);
        }

        // Modify the members, if allowed.
        if (allowed) {
            plot.removeMember(target.getUniqueId());
            Messenger.send(sender, "gulag.commandPlotRemove.success");
        }
    }
}
