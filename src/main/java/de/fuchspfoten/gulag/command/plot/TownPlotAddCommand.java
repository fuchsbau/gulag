package de.fuchspfoten.gulag.command.plot;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Plot;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownPermission;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town plot add &lt;name&gt; command.
 */
public class TownPlotAddCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownPlotAddCommand() {
        super("gulag.townHelp.plot.add", "gulag.use");
        Messenger.register("gulag.commandPlotAdd.notSameTown");
        Messenger.register("gulag.commandPlotAdd.success");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "gulag.townHelp.plot.add");
            return;
        }
        final String name = args[0];
        final OfflinePlayer target = PlayerHelper.requireSeenOfflinePlayerByName(player, name);
        if (target == null) {
            return;
        }

        // Check that the following conditions hold:
        //  - Actor is in town world.
        //  - Actor is in town.
        //  - Actor chunk is owned by actor town.
        //  - Target is in town.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player).withTarget(target)
                .requireActorInTownWorld()
                .requireActorInTown()
                .requireActorChunkIsOwnedByActorTown()
                .requireTargetInTown();
        if (condition.isFailure()) {
            return;
        }

        final Town actorTown = condition.getActorTown();
        final Plot plot = actorTown.getPlotAt(condition.getActorCoord());

        // Check that the user is in the same town.
        if (condition.getTargetTown() != actorTown) {
            // TODO when do we allow this? Also, target town might be null.
            Messenger.send(sender, "gulag.commandPlotAdd.notSameTown");
            return;
        }

        // Check the permissions.
        final boolean allowed;
        if (plot.isOwned() && plot.getOwnerId().equals(player.getUniqueId())) {
            allowed = true;
        } else {
            allowed = actorTown.hasPermission(player, TownPermission.MODIFY_MEMBERS);
        }

        // Modify the members, if allowed.
        if (allowed) {
            plot.addMember(target.getUniqueId());
            Messenger.send(sender, "gulag.commandPlotAdd.success");
        }
    }
}
