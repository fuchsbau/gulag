package de.fuchspfoten.gulag.command.plot;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Plot;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownPermission;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town plot sell command.
 */
public class TownPlotSellCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownPlotSellCommand() {
        super("gulag.townHelp.plot.sell", "gulag.use");
        Messenger.register("gulag.commandPlotSell.broadcast");
        Messenger.register("gulag.commandPlotSell.cantSell");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Check that the following conditions hold:
        //  - Actor is in town world.
        //  - Actor is in a town.
        //  - Actor chunk is claimed by the actor town.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireActorInTownWorld()
                .requireActorInTown()
                .requireActorChunkIsOwnedByActorTown();
        if (condition.isFailure()) {
            return;
        }

        final Town town = condition.getActorTown();
        final Plot plot = town.getPlotAt(condition.getActorCoord());

        if (plot.isForSale()) {
            // Case 1: Nothing to do.
            Messenger.send(sender, "gulag.nothingToDo");
        } else {
            if (plot.isOwned()) {
                // Case 2: Impossible.
                Messenger.send(sender, "gulag.commandPlotSell.cantSell");
            } else {
                // Case 3: Not For Sale -> For Sale.
                if (town.hasPermission(player, TownPermission.SELL_PLOT)) {
                    plot.setForSale(true);
                    town.forEachOnline(p -> Messenger.send(p, "gulag.commandPlotSell.broadcast",
                            sender.getName()));
                }
            }
        }
    }
}
