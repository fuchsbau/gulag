package de.fuchspfoten.gulag.command.plot;

import de.fuchspfoten.fuchslib.command.TreeCommand;

/**
 * /town plot command.
 */
public class TownPlotCommand extends TreeCommand {

    /**
     * Constructor.
     */
    public TownPlotCommand() {
        super("gulag.townHelp.plot.self");
        addSubCommand("add", new TownPlotAddCommand());
        addSubCommand("buy", new TownPlotBuyCommand());
        addSubCommand("clear", new TownPlotClearCommand());
        addSubCommand("info", new TownPlotInfoCommand());
        addSubCommand("remove", new TownPlotRemoveCommand());
        addSubCommand("sell", new TownPlotSellCommand());
        addSubCommand("tax", new TownPlotTaxCommand());
    }
}
