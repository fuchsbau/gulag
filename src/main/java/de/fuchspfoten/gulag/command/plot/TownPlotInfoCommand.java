package de.fuchspfoten.gulag.command.plot;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.GulagPlugin;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Plot;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

/**
 * /town plot info command.
 */
public class TownPlotInfoCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownPlotInfoCommand() {
        super("gulag.townHelp.plot.info", "gulag.use");
        Messenger.register("gulag.commandPlotInfo.header");
        Messenger.register("gulag.commandPlotInfo.plotOwned");
        Messenger.register("gulag.commandPlotInfo.plotFS");
        Messenger.register("gulag.commandPlotInfo.plotCity");
        Messenger.register("gulag.commandPlotInfo.plotMembers");
        Messenger.register("gulag.commandPlotInfo.taxDifference");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Check that the following conditions hold:
        //  - Actor is in town world.
        //  - Actor is in a claimed chunk (any town).
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireActorInTownWorld()
                .requireActorChunkIsOwnedByAnyTown();
        if (condition.isFailure()) {
            return;
        }

        final Town town = TownManager.get().getTownAt(condition.getActorCoord());
        final Plot plot = town.getPlotAt(condition.getActorCoord());

        // Display plot information.
        Messenger.send(sender, "gulag.commandPlotInfo.header", town.getName());

        // Display ownership status.
        if (plot.isOwned()) {
            final OfflinePlayer who = Bukkit.getOfflinePlayer(plot.getOwnerId());
            Messenger.send(sender, "gulag.commandPlotInfo.plotOwned", who.getName());
        } else {
            if (plot.isForSale()) {
                Messenger.send(sender, "gulag.commandPlotInfo.plotFS",
                        GulagPlugin.getSelf().getEconomy().format(plot.getTax()));
            } else {
                Messenger.send(sender, "gulag.commandPlotInfo.plotCity");
            }
        }

        // Display tax difference.
        if (plot.getTax() != town.getTax()) {
            Messenger.send(sender, "gulag.commandPlotInfo.taxDifference",
                    GulagPlugin.getSelf().getEconomy().format(plot.getTax()),
                    GulagPlugin.getSelf().getEconomy().format(town.getTax()));
        }

        // Display plot members.
        final List<String> memberNames = plot.getMemberIDs().stream()
                .map(Bukkit::getOfflinePlayer)
                .map(OfflinePlayer::getName)
                .collect(Collectors.toList());
        Messenger.send(sender, "gulag.commandPlotInfo.plotMembers", String.join(", ", memberNames));
    }
}
