package de.fuchspfoten.gulag.command.plot;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Plot;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownPermission;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town plot clear command.
 */
public class TownPlotClearCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownPlotClearCommand() {
        super("gulag.townHelp.plot.clear", "gulag.use");
        Messenger.register("gulag.commandPlotClear.broadcastEvict");
        Messenger.register("gulag.commandPlotClear.broadcastNFS");
        Messenger.register("gulag.commandPlotClear.broadcastRenounce");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Check that the following conditions hold:
        //  - Actor is in town world.
        //  - Actor is in a town.
        //  - Actor chunk is owned by the actor town.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireActorInTownWorld()
                .requireActorInTown()
                .requireActorChunkIsOwnedByActorTown();
        if (condition.isFailure()) {
            return;
        }

        final Town actorTown = condition.getActorTown();
        final Plot plot = actorTown.getPlotAt(condition.getActorCoord());

        if (plot.isForSale()) {
            // Case 1: For Sale -> Not For Sale.
            if (actorTown.hasPermission(player, TownPermission.TAKEBACK_PLOT)) {
                plot.setForSale(false);
                actorTown.forEachOnline(p -> Messenger.send(p, "gulag.commandPlotClear.broadcastNFS",
                        sender.getName()));
            }
        } else {
            if (plot.isOwned()) {
                if (plot.getOwnerId().equals(player.getUniqueId())) {
                    // Case 3: Owner clears.
                    plot.setOwnerId(null);
                    actorTown.forEachOnline(p -> Messenger.send(p, "gulag.commandPlotClear.broadcastRenounce",
                            sender.getName()));
                } else {
                    // Case 4: Third person clears.
                    if (actorTown.hasPermission(player, TownPermission.CLEAR_OWNED)) {
                        plot.setOwnerId(null);
                        actorTown.forEachOnline(p -> Messenger.send(p, "gulag.commandPlotClear.broadcastEvict",
                                sender.getName()));
                    }
                }
            } else {
                // Case 2: Nothing to do.
                Messenger.send(sender, "gulag.nothingToDo");
            }
        }
    }
}
