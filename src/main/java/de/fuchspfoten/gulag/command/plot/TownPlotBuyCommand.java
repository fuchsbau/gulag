package de.fuchspfoten.gulag.command.plot;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Plot;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town plot buy command.
 */
public class TownPlotBuyCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownPlotBuyCommand() {
        super("gulag.townHelp.plot.buy", "gulag.use");
        Messenger.register("gulag.commandPlotBuy.broadcast");
        Messenger.register("gulag.commandPlotBuy.cantBuy");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Check that the following conditions hold:
        //  - The actor is in the town world.
        //  - The actor is in a town.
        //  - The actor is in a chunk of their town.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireActorInTownWorld()
                .requireActorInTown()
                .requireActorChunkIsOwnedByActorTown();
        if (condition.isFailure()) {
            return;
        }

        final Plot plot = condition.getActorTown().getPlotAt(condition.getActorCoord());
        if (plot.isForSale()) {
            // Case 1: Buy.
            plot.setForSale(false);
            plot.setOwnerId(player.getUniqueId());
            condition.getActorTown().forEachOnline(p -> Messenger.send(p, "gulag.commandPlotBuy.broadcast",
                    sender.getName()));
        } else {
            // Case 2: Impossible.
            Messenger.send(sender, "gulag.commandPlotBuy.cantBuy");
        }
    }
}
