package de.fuchspfoten.gulag.command;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.command.TreeCommand;
import de.fuchspfoten.gulag.command.admin.TownAdminCommand;
import de.fuchspfoten.gulag.command.chef.TownChefCommand;
import de.fuchspfoten.gulag.command.plot.TownPlotCommand;

/**
 * /town command.
 */
public class TownCommand extends TreeCommand {

    /**
     * Constructor.
     */
    public TownCommand() {
        super(null);
        addSubCommand("admin", new TownAdminCommand());
        addSubCommand("chef", new TownChefCommand());
        addSubCommand("deposit", new TownDepositCommand());
        addSubCommand("info", new TownInfoCommand());
        addSubCommand("join", new TownJoinCommand());
        addSubCommand("leave", new TownLeaveCommand());
        addSubCommand("list", new TownListCommand());
        addSubCommand("new", new TownNewCommand());
        addSubCommand("plot", new TownPlotCommand());
        addSubCommand("spawn", new TownSpawnCommand());
        addSubCommand("who", new TownWhoCommand());

        Messenger.register("gulag.cantDeleteConnector");
        Messenger.register("gulag.chunkNotConnected");
        Messenger.register("gulag.notEnoughSafety");
        Messenger.register("gulag.nothingToDo");
        Messenger.register("gulag.permNotExist");
        Messenger.register("gulag.rankExist");
        Messenger.register("gulag.rankNotExist");
        Messenger.register("gulag.rankNotLower");
        Messenger.register("gulag.targetNotFound");
        Messenger.register("gulag.targetNotOnline");
    }
}
