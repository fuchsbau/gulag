package de.fuchspfoten.gulag.command;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.GulagPlugin;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.stream.Collectors;

/**
 * /town info [town] command.
 */
public class TownInfoCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownInfoCommand() {
        super("gulag.townHelp.info", "gulag.use");
        Messenger.register("gulag.commandInfo.bank");
        Messenger.register("gulag.commandInfo.header");
        Messenger.register("gulag.commandInfo.mayor");
        Messenger.register("gulag.commandInfo.plots");
        Messenger.register("gulag.commandInfo.residents");
        Messenger.register("gulag.commandInfo.tax");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender);

        // Determine the target.
        if (args.length >= 1) {
            if (TownManager.get().getTown(args[0]) != null) {
                condition.requireActorTownByName(args[0]);
            } else {
                final OfflinePlayer targetPlayer = PlayerHelper.requireSeenOfflinePlayerByName(sender, args[0]);
                if (targetPlayer == null) {
                    return;
                }
                condition.withActor(targetPlayer).requireActorInTown();
            }
        } else {
            final Player player = PlayerHelper.ensurePlayer(sender);
            if (player == null) {
                return;
            }
            condition.withActor(player).requireActorInTown();
        }
        if (condition.isFailure()) {
            return;
        }

        // Retrieve the actor town.
        final Town actorTown = condition.getActorTown();

        // General information.
        final OfflinePlayer mayor = Bukkit.getOfflinePlayer(actorTown.getMayorId());
        Messenger.send(sender, "gulag.commandInfo.header", actorTown.getName());
        Messenger.send(sender, "gulag.commandInfo.mayor", mayor.getName());
        Messenger.send(sender, "gulag.commandInfo.bank",
                GulagPlugin.getSelf().getEconomy().format(actorTown.getBalance()));

        // Tax information.
        final int plots = actorTown.getPlots().size();
        final int upkeep = TownManager.get().getCost("upkeep") * plots;
        Messenger.send(sender, "gulag.commandInfo.tax",
                GulagPlugin.getSelf().getEconomy().format(actorTown.getTax()),
                GulagPlugin.getSelf().getEconomy().format(upkeep));

        // Resident information.
        final String residents = String.join(", ", actorTown.getResidentIDs().stream()
                .map(Bukkit::getOfflinePlayer)
                .map(OfflinePlayer::getName)
                .collect(Collectors.toList()));
        Messenger.send(sender, "gulag.commandInfo.residents", residents);

        // Plot information.
        final int maxPlots = actorTown.getResidentIDs().size() * TownManager.get().getPlotsPerResident();
        Messenger.send(sender, "gulag.commandInfo.plots", plots, maxPlots);
    }
}
