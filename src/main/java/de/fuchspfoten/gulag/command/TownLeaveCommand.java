package de.fuchspfoten.gulag.command;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.TownManager;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town leave command.
 */
public class TownLeaveCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownLeaveCommand() {
        super("gulag.townHelp.leave", "gulag.use");
        Messenger.register("gulag.commandLeave.broadcast");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Check the following conditions:
        //  - Actor is in town.
        //  - Actor is not mayor.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireActorInTown()
                .requireActorIsNotMayor();
        if (condition.isFailure()) {
            return;
        }

        // Leave the town.
        condition.getActorTown().forEachOnline(p -> Messenger.send(p, "gulag.commandLeave.broadcast",
                sender.getName()));
        TownManager.get().leaveTown(player.getUniqueId());
    }
}
