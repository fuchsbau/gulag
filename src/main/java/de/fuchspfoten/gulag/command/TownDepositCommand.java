package de.fuchspfoten.gulag.command;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.GulagPlugin;
import de.fuchspfoten.gulag.TownConditionHelper;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town deposit &lt;amount&gt; command.
 */
public class TownDepositCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownDepositCommand() {
        super("gulag.townHelp.deposit", "gulag.use");
        Messenger.register("gulag.commandDeposit.broadcast");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "gulag.townHelp.deposit");
            return;
        }
        final String amount = args[0];

        // Parse the amount.
        final int amountParsed;
        try {
            amountParsed = Integer.parseInt(amount);
            if (amountParsed < 1) {
                return;
            }
        } catch (final NumberFormatException ex) {
            return;
        }

        // Perform the condition checks:
        //  - Actor in town.
        //  - Actor has $amountParsed.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireActorInTown()
                .requireActorHasMoney(amountParsed);
        if (condition.isFailure()) {
            return;
        }

        // Withdraw the amount.
        GulagPlugin.getSelf().getEconomy().withdrawPlayer(player, amountParsed);

        // Deposit the money.
        condition.getActorTown().modifyBalance(amountParsed);
        condition.getActorTown().forEachOnline(p -> Messenger.send(p, "gulag.commandDeposit.broadcast",
                sender.getName(), GulagPlugin.getSelf().getEconomy().format(amountParsed)));
    }
}
