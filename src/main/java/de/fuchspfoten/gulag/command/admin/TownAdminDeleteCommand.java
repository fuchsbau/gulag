package de.fuchspfoten.gulag.command.admin;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.TownManager;
import org.bukkit.command.CommandSender;

/**
 * /town admin delete &lt;town&gt; command.
 */
public class TownAdminDeleteCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownAdminDeleteCommand() {
        super("gulag.townHelp.admin.delete", "gulag.admin");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "gulag.townHelp.admin.delete");
            return;
        }
        final String name = args[0];

        // Check that the following conditions hold:
        //  - The given town name is valid.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).requireActorTownByName(name);
        if (condition.isFailure()) {
            return;
        }

        // Delete the town.
        TownManager.get().removeTown(condition.getActorTown());
        sender.sendMessage("done");
    }
}
