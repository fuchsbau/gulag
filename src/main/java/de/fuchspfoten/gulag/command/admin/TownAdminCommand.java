package de.fuchspfoten.gulag.command.admin;

import de.fuchspfoten.fuchslib.command.TreeCommand;

/**
 * /town admin command.
 */
public class TownAdminCommand extends TreeCommand {

    /**
     * Constructor.
     */
    public TownAdminCommand() {
        super(null);  // No helpline to prevent listing the command.
        addSubCommand("delete", new TownAdminDeleteCommand());
        addSubCommand("runtax", new TownAdminRuntaxCommand());
        addSubCommand("seize", new TownAdminSeizeCommand());
    }
}
