package de.fuchspfoten.gulag.command.admin;

import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownManager;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;

/**
 * /town admin runtax command.
 */
public class TownAdminRuntaxCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownAdminRuntaxCommand() {
        super("gulag.townHelp.admin.runtax", "gulag.admin");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        sender.sendMessage("Collecting tax for all towns!");
        final Iterable<Town> towns = new ArrayList<>(TownManager.get().getTowns());
        for (final Town town : towns) {
            town.runTaxes();
        }
    }
}
