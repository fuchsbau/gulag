package de.fuchspfoten.gulag.command.admin;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.TownManager;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town admin seize &lt;town&gt; command.
 */
public class TownAdminSeizeCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownAdminSeizeCommand() {
        super("gulag.townHelp.admin.seize", "gulag.admin");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "gulag.townHelp.admin.seize");
            return;
        }
        final String name = args[0];

        // Check that the following conditions hold:
        //  - Given target town exists.
        //  - Actor is not in town.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireTargetTownByName(name)
                .requireActorNotInTown();
        if (condition.isFailure()) {
            return;
        }

        // Seize the town.
        TownManager.get().joinTown(condition.getTargetTown(), player.getUniqueId());
        condition.getTargetTown().setMayorId(player.getUniqueId());
        sender.sendMessage("done");
    }
}
