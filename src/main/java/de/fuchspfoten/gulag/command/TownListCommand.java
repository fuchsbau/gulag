package de.fuchspfoten.gulag.command;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.Pagination;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownManager;
import org.bukkit.command.CommandSender;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * /town list [page] command.
 */
public class TownListCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownListCommand() {
        super("gulag.townHelp.list", "gulag.use");
        Messenger.register("gulag.commandList.line");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        // Get the page number.
        final int page;
        try {
            page = (args.length == 0) ? 1 : Integer.parseInt(args[0]);
            if (page < 1) {
                return;
            }
        } catch (final NumberFormatException ex) {
            return;
        }

        // Sort the towns.
        final SortedMap<String, Town> sortedTowns = new TreeMap<>();
        for (final Town town : TownManager.get().getTowns()) {
            sortedTowns.put(town.getName(), town);
        }

        // Pagination setup.
        int step = 10;
        int skip = step * (page - 1);
        final int pages = (sortedTowns.size() % step == 0) ? sortedTowns.size() / step : sortedTowns.size() / step + 1;

        // Display the pagination.
        Pagination.showPagination(sender, page, pages, "/town list ");
        for (final Town town : sortedTowns.values()) {
            if (skip-- > 0) {
                continue;
            }
            if (step-- == 0) {
                break;
            }
            Messenger.send(sender, "gulag.commandList.line", town.getName());
        }
        Pagination.showPagination(sender, page, pages, "/town list ");
    }
}
