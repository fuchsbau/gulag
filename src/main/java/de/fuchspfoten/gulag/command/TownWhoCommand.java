package de.fuchspfoten.gulag.command;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Rank;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * /town info [town] command.
 */
public class TownWhoCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownWhoCommand() {
        super("gulag.townHelp.who", "gulag.use");
        Messenger.register("gulag.commandWho.header");
        Messenger.register("gulag.commandWho.line");
        Messenger.register("gulag.commandWho.lineDefault");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender);

        // Determine the target.
        if (args.length >= 1) {
            if (TownManager.get().getTown(args[0]) != null) {
                condition.requireActorTownByName(args[0]);
            } else {
                final OfflinePlayer targetPlayer = PlayerHelper.requireSeenOfflinePlayerByName(sender, args[0]);
                if (targetPlayer == null) {
                    return;
                }
                condition.withActor(targetPlayer).requireActorInTown();
            }
        } else {
            final Player player = PlayerHelper.ensurePlayer(sender);
            if (player == null) {
                return;
            }
            condition.withActor(player).requireActorInTown();
        }
        if (condition.isFailure()) {
            return;
        }

        // Get the actor town.
        final Town actorTown = condition.getActorTown();

        // Show the header.
        Messenger.send(sender, "gulag.commandWho.header", actorTown.getName());

        // Show each rank.
        final Set<UUID> unmentioned = new HashSet<>(actorTown.getResidentIDs());
        final Collection<Rank> sortedRanks = new TreeSet<>(Comparator.comparing(Rank::getName));
        sortedRanks.addAll(actorTown.getRanks());
        for (final Rank rank : sortedRanks) {
            // Make rank members mentioned.
            final Set<UUID> members = rank.getMemberIds();
            members.forEach(unmentioned::remove);

            // Show the rank members.
            final String memberString = String.join(", ", members.stream()
                    .map(Bukkit::getOfflinePlayer)
                    .map(OfflinePlayer::getName)
                    .collect(Collectors.toList()));
            Messenger.send(sender, "gulag.commandWho.line", rank.getName(), members.size(), memberString);
        }

        // Show the rest, if needed.
        if (!unmentioned.isEmpty()) {
            final String memberString = String.join(", ", unmentioned.stream()
                    .map(Bukkit::getOfflinePlayer)
                    .map(OfflinePlayer::getName)
                    .collect(Collectors.toList()));
            Messenger.send(sender, "gulag.commandWho.lineDefault", unmentioned.size(), memberString);
        }
    }
}
