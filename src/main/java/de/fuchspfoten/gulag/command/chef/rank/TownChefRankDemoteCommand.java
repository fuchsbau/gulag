package de.fuchspfoten.gulag.command.chef.rank;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Rank;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownPermission;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town chef rank demote &lt;name&gt; command.
 */
public class TownChefRankDemoteCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownChefRankDemoteCommand() {
        super("gulag.townHelp.chef.rank.demote", "gulag.use");
        Messenger.register("gulag.commandRankDemote.broadcast");
        Messenger.register("gulag.commandRankDemote.hasNoRank");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "gulag.townHelp.chef.rank.demote");
            return;
        }
        final String targetName = args[0];
        final OfflinePlayer target = PlayerHelper.requireSeenOfflinePlayerByName(sender, targetName);
        if (target == null) {
            return;
        }

        // Check that the following conditions hold:
        //  - Actor is in a town.
        //  - Target is in a town.
        //  - Actor town is target town.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player).withTarget(target)
                .requireActorInTown()
                .requireTargetInTown()
                .requireActorTownIsTargetTown();
        if (condition.isFailure()) {
            return;
        }

        final Town town = condition.getActorTown();

        // Check that the target has a rank.
        final Rank demotedRank = town.getRankOf(target.getUniqueId());
        if (demotedRank == null) {
            Messenger.send(sender, "gulag.commandRankDemote.hasNoRank");
            return;
        }

        // Ensure permissions.
        if (town.hasPermission(player, TownPermission.RANK_ASSIGN)) {
            // Check that the given rank is lower than the sender's rank, if the sender isn't able to assign all ranks.
            final Rank senderRank = town.getRankOf(player.getUniqueId());
            if (senderRank == null || !demotedRank.isLowerThan(senderRank)) {
                if (!town.hasPermission(player, TownPermission.RANK_ASSIGN_ALL)) {
                    Messenger.send(sender, "gulag.rankNotLower");
                    return;
                }
            }

            // Change the rank and announce it.
            town.forEachOnline(p -> Messenger.send(p, "gulag.commandRankDemote.broadcast", sender.getName(),
                    target.getName()));
            town.setRank(target.getUniqueId(), null);
        }
    }
}
