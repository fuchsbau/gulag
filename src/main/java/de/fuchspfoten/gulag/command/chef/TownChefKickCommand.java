package de.fuchspfoten.gulag.command.chef;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownManager;
import de.fuchspfoten.gulag.model.TownPermission;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * /town chef kick &lt;name&gt; command.
 */
public class TownChefKickCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownChefKickCommand() {
        super("gulag.townHelp.chef.kick", "gulag.use");
        Messenger.register("gulag.commandKick.broadcast");
        Messenger.register("gulag.commandKick.canNotKick");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "gulag.townHelp.chef.kick");
            return;
        }
        final String name = args[0];
        final OfflinePlayer target = PlayerHelper.requireSeenOfflinePlayerByName(sender, name);
        if (target == null) {
            return;
        }

        // Check that the following conditions hold:
        //  - Actor is in a town.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireActorInTown();
        if (condition.isFailure()) {
            return;
        }

        final Town town = condition.getActorTown();
        final UUID targetId = target.getUniqueId();

        // Check that the user can be kicked.
        if (!town.getResidentIDs().contains(targetId) || town.hasPermission(targetId, TownPermission.UNKICKABLE)) {
            Messenger.send(sender, "gulag.commandKick.canNotKick");
            return;
        }

        // Ensure permissions.
        if (town.hasPermission(player, TownPermission.KICK)) {
            town.forEachOnline(p -> Messenger.send(p, "gulag.commandKick.broadcast", sender.getName(), name));
            TownManager.get().leaveTown(targetId);
        }
    }
}
