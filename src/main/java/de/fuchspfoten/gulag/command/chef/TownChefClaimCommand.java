package de.fuchspfoten.gulag.command.chef;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.ChunkCoordinate;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownManager;
import de.fuchspfoten.gulag.model.TownPermission;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town chef claim command.
 */
public class TownChefClaimCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownChefClaimCommand() {
        super("gulag.townHelp.chef.claim", "gulag.use");
        Messenger.register("gulag.commandClaim.broadcast");
        Messenger.register("gulag.commandClaim.notEnoughSpace");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Check that the following conditions hold:
        //  - Actor is in town world.
        //  - Actor is in town.
        //  - Actor chunk is not claimed.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireActorInTownWorld()
                .requireActorInTown()
                .requireActorChunkIsUnclaimed();
        if (condition.isFailure()) {
            return;
        }

        final Town town = condition.getActorTown();
        final ChunkCoordinate coord = condition.getActorCoord();

        // Check that an adjacent chunk is claimed by the town.
        final int x = player.getLocation().getChunk().getX();
        final int z = player.getLocation().getChunk().getZ();
        final ChunkCoordinate coordNorth = new ChunkCoordinate(x, z - 1);
        final ChunkCoordinate coordWest = new ChunkCoordinate(x - 1, z);
        final ChunkCoordinate coordSouth = new ChunkCoordinate(x, z + 1);
        final ChunkCoordinate coordEast = new ChunkCoordinate(x + 1, z);
        if (TownManager.get().getTownAt(coordNorth) != town && TownManager.get().getTownAt(coordWest) != town
                && TownManager.get().getTownAt(coordSouth) != town && TownManager.get().getTownAt(coordEast) != town) {
            Messenger.send(sender, "gulag.chunkNotConnected");
            return;
        }

        // Ensure that the balance of the town would not be critical.
        final int plots = town.getPlots().size() + 1;  // Add 1 for the new claim.
        final int upkeep = TownManager.get().getCost("upkeep") * plots;
        final int needed = upkeep + TownManager.get().getCost("claim");
        if (town.getBalance() < needed) {
            Messenger.send(sender, "gulag.notEnoughSafety");
            return;
        }

        final int maxPlots =
                town.getResidentIDs().size() * TownManager.get().getPlotsPerResident();
        if (plots > maxPlots) {  // Attention, plots is already +1.
            Messenger.send(sender, "gulag.commandClaim.notEnoughSpace");
            return;
        }

        // Ensure permissions.
        if (town.hasPermission(player, TownPermission.CLAIM)) {
            town.modifyBalance(-TownManager.get().getCost("claim"));
            TownManager.get().claimChunk(town, coord);
            town.forEachOnline(p -> Messenger.send(p, "gulag.commandClaim.broadcast", sender.getName()));
        }
    }
}
