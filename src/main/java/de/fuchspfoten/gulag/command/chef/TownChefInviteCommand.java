package de.fuchspfoten.gulag.command.chef;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownPermission;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town chef invite &lt;name&gt; command.
 */
public class TownChefInviteCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownChefInviteCommand() {
        super("gulag.townHelp.chef.invite", "gulag.use");
        Messenger.register("gulag.commandInvite.broadcast");
        Messenger.register("gulag.commandInvite.notification");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "gulag.townHelp.chef.invite");
            return;
        }
        final String name = args[0];
        final Player target = PlayerHelper.requireVisiblePlayerByName(player, name);
        if (target == null) {
            return;
        }

        // Check that the following conditions hold:
        //  - Actor is in a town.
        //  - Target is not in a town.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player).withTarget(target)
                .requireActorInTown()
                .requireTargetNotInTown();
        if (condition.isFailure()) {
            return;
        }

        final Town town = condition.getActorTown();

        // Ensure permissions.
        if (town.hasPermission(player, TownPermission.INVITE)) {
            town.forEachOnline(p -> Messenger.send(p, "gulag.commandInvite.broadcast", sender.getName(),
                    target.getName()));
            Messenger.send(target, "gulag.commandInvite.notification", town.getName());
            town.invite(target.getUniqueId());
        }
    }
}
