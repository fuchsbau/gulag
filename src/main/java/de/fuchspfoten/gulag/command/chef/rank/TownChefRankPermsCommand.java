package de.fuchspfoten.gulag.command.chef.rank;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownPermission;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * /town chef rank perms command.
 */
public class TownChefRankPermsCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownChefRankPermsCommand() {
        super("gulag.townHelp.chef.rank.perms", "gulag.use");
        Messenger.register("gulag.commandRankPerms.line");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Check that the following conditions hold:
        //  - Actor is in town.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireActorInTown();
        if (condition.isFailure()) {
            return;
        }

        final Town town = condition.getActorTown();

        // Ensure permissions.
        if (town.hasPermission(player, TownPermission.RANK_MANAGE)) {
            Messenger.send(sender, "gulag.commandRankPerms.line",
                    String.join(", ", Arrays.stream(TownPermission.values())
                            .map(TownPermission::getNode)
                            .collect(Collectors.toList())));
        }
    }
}
