package de.fuchspfoten.gulag.command.chef;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.fuchslib.ui.SelectorWheel;
import de.fuchspfoten.fuchslib.ui.SelectorWheel.SelectorWheelOption;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Town;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town chef setmayor &lt;name&gt; command.
 */
public class TownChefSetmayorCommand extends LeafCommand {

    /**
     * Attempts a mayor change.
     *
     * @param player The player performing the action.
     * @param target The target for the change.
     * @param dryRun Iff {@code true}, this only shows a confirmation wheel.
     */
    private static void attemptMayorChange(final Player player, final OfflinePlayer target, final boolean dryRun) {
        // Check that the following conditions hold:
        //  - Actor is in town.
        //  - Target is in town.
        //  - Actor town is target town.
        //  - Actor is mayor.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(player).withActor(player).withTarget(target)
                .requireActorInTown()
                .requireTargetInTown()
                .requireActorTownIsTargetTown()
                .requireActorIsMayor();
        if (condition.isFailure()) {
            return;
        }
        final Town town = condition.getActorTown();

        if (dryRun) {
            final SelectorWheel wheel = new SelectorWheel();
            wheel.cancelOption(new SelectorWheelOption(Messenger.getFormat("gulag.commandMayor.abort"), l -> {
            }));
            wheel.options(new SelectorWheelOption(Messenger.getFormat("gulag.commandMayor.confirm"),
                    l -> attemptMayorChange(player, target, false)));
            wheel.show(player);
            return;
        }

        town.forEachOnline(p -> Messenger.send(p, "gulag.commandMayor.broadcast", player.getName(),
                target.getName()));
        town.setMayorId(target.getUniqueId());
    }

    /**
     * Constructor.
     */
    public TownChefSetmayorCommand() {
        super("gulag.townHelp.chef.setmayor", "gulag.use");
        Messenger.register("gulag.commandMayor.broadcast");
        Messenger.register("gulag.commandMayor.abort");
        Messenger.register("gulag.commandMayor.confirm");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "gulag.townHelp.chef.setmayor");
            return;
        }
        final String target = args[0];
        final OfflinePlayer targetPlayer = PlayerHelper.requireSeenOfflinePlayerByName(player, target);
        if (targetPlayer == null || targetPlayer == sender) {
            return;
        }

        attemptMayorChange(player, targetPlayer, true);
    }
}
