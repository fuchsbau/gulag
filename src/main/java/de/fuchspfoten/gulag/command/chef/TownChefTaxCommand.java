package de.fuchspfoten.gulag.command.chef;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownManager;
import de.fuchspfoten.gulag.model.TownPermission;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town chef tax &lt;amount&gt; command.
 */
public class TownChefTaxCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownChefTaxCommand() {
        super("gulag.townHelp.chef.tax", "gulag.use");
        Messenger.register("gulag.commandTax.success");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "gulag.townHelp.chef.tax");
            return;
        }
        final String amount = args[0];

        // Parse the amount.
        final int amountParsed;
        try {
            amountParsed = Math.min(Integer.parseInt(amount), TownManager.get().getMaximumTax());
            if (amountParsed < 0) {
                return;
            }
        } catch (final NumberFormatException ex) {
            return;
        }

        // Check that the following conditions hold:
        //  - Actor is in town.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player).requireActorInTown();
        if (condition.isFailure()) {
            return;
        }

        final Town town = condition.getActorTown();

        // Ensure permissions.
        if (town.hasPermission(player, TownPermission.SET_TAX)) {
            // Change the tax.
            town.setTax(amountParsed);
            Messenger.send(sender, "gulag.commandTax.success");
        }
    }
}
