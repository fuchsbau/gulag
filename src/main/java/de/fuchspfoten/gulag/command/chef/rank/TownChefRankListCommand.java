package de.fuchspfoten.gulag.command.chef.rank;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Rank;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownPermission;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town chef rank list command.
 */
public class TownChefRankListCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownChefRankListCommand() {
        super("gulag.townHelp.chef.rank.list", "gulag.use");
        Messenger.register("gulag.commandRankList.line");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Check that the following conditions hold:
        //  - Actor is in town.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player).requireActorInTown();
        if (condition.isFailure()) {
            return;
        }

        final Town town = condition.getActorTown();

        // Ensure permissions.
        if (town.hasPermission(player, TownPermission.RANK_MANAGE)) {
            for (final Rank rank : town.getRanks()) {
                Messenger.send(sender, "gulag.commandRankList.line", rank.getName(),
                        String.join(", ", rank.getPermissions()));
            }
        }
    }
}
