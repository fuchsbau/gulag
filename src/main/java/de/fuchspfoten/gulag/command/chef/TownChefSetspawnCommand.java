package de.fuchspfoten.gulag.command.chef;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownPermission;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town chef setspawn command.
 */
public class TownChefSetspawnCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownChefSetspawnCommand() {
        super("gulag.townHelp.chef.setspawn", "gulag.use");
        Messenger.register("gulag.commandSetSpawn.broadcast");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Check that the following conditions hold:
        //  - Actor is in town world.
        //  - Actor is in town.
        //  - Actor chunk is claimed by actor town.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireActorInTownWorld()
                .requireActorInTown()
                .requireActorChunkIsOwnedByActorTown();
        if (condition.isFailure()) {
            return;
        }

        final Town town = condition.getActorTown();

        // Ensure permissions.
        if (town.hasPermission(player, TownPermission.SET_SPAWN)) {
            town.setSpawn(player.getLocation());
            town.forEachOnline(p -> Messenger.send(p, "gulag.commandSetSpawn.broadcast", sender.getName()));
        }
    }
}
