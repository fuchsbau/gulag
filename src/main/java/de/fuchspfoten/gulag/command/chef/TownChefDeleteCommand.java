package de.fuchspfoten.gulag.command.chef;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownManager;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town chef delete command.
 */
public class TownChefDeleteCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownChefDeleteCommand() {
        super("gulag.townHelp.chef.delete", "gulag.use");
        Messenger.register("gulag.commandDelete.notEmpty");
        Messenger.register("gulag.commandDelete.success");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Check that the following conditions hold:
        //  - Actor is in a town.
        //  - Actor is mayor.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireActorInTown()
                .requireActorIsMayor();
        if (condition.isFailure()) {
            return;
        }

        final Town town = condition.getActorTown();

        // Check if the town is empty.
        if (town.getResidentIDs().size() != 1) {
            Messenger.send(sender, "gulag.commandDelete.notEmpty");
            return;
        }

        // Delete the town.
        TownManager.get().removeTown(town);
        Messenger.send(sender, "gulag.commandDelete.success");
    }
}
