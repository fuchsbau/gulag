package de.fuchspfoten.gulag.command.chef;

import de.fuchspfoten.fuchslib.command.TreeCommand;
import de.fuchspfoten.gulag.command.chef.rank.TownChefRankCommand;

/**
 * /town chef command.
 */
public class TownChefCommand extends TreeCommand {

    /**
     * Constructor.
     */
    public TownChefCommand() {
        super("gulag.townHelp.chef.self");
        addSubCommand("claim", new TownChefClaimCommand());
        addSubCommand("delete", new TownChefDeleteCommand());
        addSubCommand("invite", new TownChefInviteCommand());
        addSubCommand("kick", new TownChefKickCommand());
        addSubCommand("rank", new TownChefRankCommand());
        addSubCommand("setmayor", new TownChefSetmayorCommand());
        addSubCommand("setspawn", new TownChefSetspawnCommand());
        addSubCommand("tax", new TownChefTaxCommand());
        addSubCommand("unclaim", new TownChefUnclaimCommand());
    }
}
