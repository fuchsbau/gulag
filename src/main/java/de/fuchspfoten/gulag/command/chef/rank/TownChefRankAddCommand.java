package de.fuchspfoten.gulag.command.chef.rank;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownPermission;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.regex.Pattern;

/**
 * /town chef rank add &lt;name&gt; command.
 */
public class TownChefRankAddCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownChefRankAddCommand() {
        super("gulag.townHelp.chef.rank.add", "gulag.use");
        Messenger.register("gulag.commandRankAdd.invalidName");
        Messenger.register("gulag.commandRankAdd.success");
        Messenger.register("gulag.commandRankAdd.tooMany");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "gulag.townHelp.chef.rank.add");
            return;
        }
        final String name = args[0];

        // Check that the name matches [a-zA-Z][a-zA-Z0-9-]+ and is between 3 and 16 characters long.
        final Pattern pattern = Pattern.compile("^[a-zA-Z][a-zA-Z0-9-]+$");
        if (!pattern.matcher(name).matches() || name.length() > 16 || name.length() < 3) {
            Messenger.send(sender, "gulag.commandRankAdd.invalidName");
            return;
        }

        // Check that the following conditions hold:
        //  - Actor is in a town.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireActorInTown();
        if (condition.isFailure()) {
            return;
        }

        final Town town = condition.getActorTown();

        // Ensure that the town has not too many ranks.
        if (town.getRanks().size() > 15) {
            Messenger.send(sender, "gulag.commandRankAdd.tooMany");
            return;
        }

        // Ensure that no rank by that name exists.
        if (town.getRank(name) != null) {
            Messenger.send(sender, "gulag.rankExist");
            return;
        }

        // Ensure permissions.
        if (town.hasPermission(player, TownPermission.RANK_MANAGE)) {
            Messenger.send(sender, "gulag.commandRankAdd.success");
            town.createRank(name);
        }
    }
}
