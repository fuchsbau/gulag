package de.fuchspfoten.gulag.command.chef.rank;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Rank;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownPermission;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town chef rank remove &lt;name&gt; command.
 */
public class TownChefRankRemoveCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownChefRankRemoveCommand() {
        super("gulag.townHelp.chef.rank.remove", "gulag.use");
        Messenger.register("gulag.commandRankRemove.success");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "gulag.townHelp.chef.rank.remove");
            return;
        }
        final String name = args[0];

        // Check that the following conditions hold:
        //  - Actor is in a town.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireActorInTown();
        if (condition.isFailure()) {
            return;
        }

        final Town town = condition.getActorTown();

        // Ensure that a rank by that name exists.
        final Rank rank = town.getRank(name);
        if (rank == null) {
            Messenger.send(sender, "gulag.rankNotExist");
            return;
        }

        // Ensure permissions.
        if (town.hasPermission(player, TownPermission.RANK_MANAGE)) {
            // Check that the given rank is lower than the sender's rank, if the sender isn't able to manage all ranks.
            final Rank senderRank = town.getRankOf(player.getUniqueId());
            if (senderRank == null || !rank.isLowerThan(senderRank)) {
                if (!town.hasPermission(player, TownPermission.RANK_MANAGE_ALL)) {
                    Messenger.send(sender, "gulag.rankNotLower");
                    return;
                }
            }

            Messenger.send(sender, "gulag.commandRankRemove.success");
            town.removeRank(name);
        }
    }
}
