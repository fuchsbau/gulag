package de.fuchspfoten.gulag.command.chef;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.TownConnectivityChecker;
import de.fuchspfoten.gulag.model.ChunkCoordinate;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownManager;
import de.fuchspfoten.gulag.model.TownPermission;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town chef unclaim command.
 */
public class TownChefUnclaimCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownChefUnclaimCommand() {
        super("gulag.townHelp.chef.unclaim", "gulag.use");
        Messenger.register("gulag.commandUnclaim.broadcast");
        Messenger.register("gulag.commandUnclaim.lastChunk");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Check that the following conditions hold:
        //  - Actor is in town world.
        //  - Actor is in town.
        //  - Actor chunk is claimed by actor town.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireActorInTownWorld()
                .requireActorInTown()
                .requireActorChunkIsOwnedByActorTown();
        if (condition.isFailure()) {
            return;
        }

        final Town town = condition.getActorTown();
        final ChunkCoordinate coord = condition.getActorCoord();

        // Check that the current chunk is not the last chunk.
        if (town.getPlots().size() == 1) {
            Messenger.send(sender, "gulag.commandUnclaim.lastChunk");
            return;
        }

        // Ensure permissions.
        if (town.hasPermission(player, TownPermission.UNCLAIM)) {
            if (!TownConnectivityChecker.isConnectedWithoutChunk(town, coord)) {
                Messenger.send(sender, "gulag.cantDeleteConnector");
                return;
            }

            TownManager.get().unclaimChunk(coord);
            town.forEachOnline(p -> Messenger.send(p, "gulag.commandUnclaim.broadcast", sender.getName()));
        }
    }
}
