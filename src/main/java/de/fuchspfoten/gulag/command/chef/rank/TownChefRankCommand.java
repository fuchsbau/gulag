package de.fuchspfoten.gulag.command.chef.rank;

import de.fuchspfoten.fuchslib.command.TreeCommand;

/**
 * /town chef rank command.
 */
public class TownChefRankCommand extends TreeCommand {

    /**
     * Constructor.
     */
    public TownChefRankCommand() {
        super("gulag.townHelp.chef.rank.self");
        addSubCommand("add", new TownChefRankAddCommand());
        addSubCommand("demote", new TownChefRankDemoteCommand());
        addSubCommand("grant", new TownChefRankGrantCommand());
        addSubCommand("list", new TownChefRankListCommand());
        addSubCommand("perms", new TownChefRankPermsCommand());
        addSubCommand("promote", new TownChefRankPromoteCommand());
        addSubCommand("remove", new TownChefRankRemoveCommand());
        addSubCommand("revoke", new TownChefRankRevokeCommand());
    }
}
