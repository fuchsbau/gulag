package de.fuchspfoten.gulag.command.chef.rank;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Rank;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownPermission;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town chef rank revoke &lt;rank&gt; &lt;perm&gt; command.
 */
public class TownChefRankRevokeCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownChefRankRevokeCommand() {
        super("gulag.townHelp.chef.rank.revoke", "gulag.use");
        Messenger.register("gulag.commandRankRevoke.success");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 2) {
            Messenger.send(sender, "gulag.townHelp.chef.rank.revoke");
            return;
        }
        final String targetRankName = args[0];
        final String targetPermNode = args[1];

        // Check that the following conditions hold:
        //  - Actor is in town.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireActorInTown();
        if (condition.isFailure()) {
            return;
        }

        final Town town = condition.getActorTown();

        // Check that the target rank exists.
        final Rank targetRank = town.getRank(targetRankName);
        if (targetRank == null) {
            Messenger.send(sender, "gulag.rankNotExist");
            return;
        }

        // Check that the target permission exists.
        final TownPermission targetPerm = TownPermission.byNode(targetPermNode);
        if (targetPerm == null) {
            Messenger.send(sender, "gulag.permNotExist");
            return;
        }

        // Ensure permissions.
        if (town.hasPermission(player, TownPermission.RANK_MANAGE)) {
            // Check that the given rank is lower than the sender's rank, if the sender isn't able to manage all ranks.
            final Rank senderRank = town.getRankOf(player.getUniqueId());
            if (senderRank == null || !targetRank.isLowerThan(senderRank)) {
                if (!town.hasPermission(player, TownPermission.RANK_MANAGE_ALL)) {
                    Messenger.send(sender, "gulag.rankNotLower");
                    return;
                }
            }

            // Change the rank and announce it.
            Messenger.send(sender, "gulag.commandRankRevoke.success");
            targetRank.removePermission(targetPerm.getNode());
        }
    }
}
