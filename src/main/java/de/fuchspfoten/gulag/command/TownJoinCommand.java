package de.fuchspfoten.gulag.command;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownManager;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town join &lt;name&gt; command.
 */
public class TownJoinCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownJoinCommand() {
        super("gulag.townHelp.join", "gulag.use");
        Messenger.register("gulag.commandJoin.broadcast");
        Messenger.register("gulag.commandJoin.notInvited");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "gulag.townHelp.join");
            return;
        }
        final String name = args[0];

        // Check the following conditions:
        //  - Target town exists.
        //  - Actor is not in town.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireTargetTownByName(name)
                .requireActorNotInTown();
        if (condition.isFailure()) {
            return;
        }

        final Town town = condition.getTargetTown();

        // Check that the user is invited.
        if (!town.isInvited(player.getUniqueId())) {
            Messenger.send(sender, "gulag.commandJoin.notInvited");
            return;
        }

        // Join the town.
        TownManager.get().joinTown(town, player.getUniqueId());
        town.forEachOnline(p -> Messenger.send(p, "gulag.commandJoin.broadcast", sender.getName()));
    }
}
