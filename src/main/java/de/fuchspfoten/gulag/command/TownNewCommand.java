package de.fuchspfoten.gulag.command;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.GulagPlugin;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownManager;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.regex.Pattern;

/**
 * /town new &lt;name&gt; command.
 */
public class TownNewCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownNewCommand() {
        super("gulag.townHelp.new", "gulag.use");
        Messenger.register("gulag.commandNew.broadcast");
        Messenger.register("gulag.commandNew.invalidName");
        Messenger.register("gulag.commandNew.townExists");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "gulag.townHelp.new");
            return;
        }
        final String name = args[0];

        // Check that the name matches [a-zA-Z][a-zA-Z0-9-]+ and is between 3 and 16 characters long.
        final Pattern pattern = Pattern.compile("^[a-zA-Z][a-zA-Z0-9-]+$");
        if (!pattern.matcher(name).matches() || name.length() > 16 || name.length() < 3) {
            Messenger.send(sender, "gulag.commandNew.invalidName");
            return;
        }

        // Check that no town with that name exists.
        if (TownManager.get().getTown(name) != null) {
            Messenger.send(sender, "gulag.commandNew.townExists");
            return;
        }

        // Check the following conditions:
        //  - Actor is in town world.
        //  - Actor is not in a town.
        //  - Actor is in an unclaimed chunk.
        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player)
                .requireActorInTownWorld()
                .requireActorNotInTown()
                .requireActorChunkIsUnclaimed()
                .requireActorHasMoney(TownManager.get().getCost("create"));
        if (condition.isFailure()) {
            return;
        }

        // Withdraw the costs.
        GulagPlugin.getSelf().getEconomy().withdrawPlayer(player, TownManager.get().getCost("create"));

        // Create the town.
        final Town town = TownManager.get().createTown(name, condition.getActorCoord(), player.getUniqueId());
        Bukkit.getOnlinePlayers().forEach(p -> Messenger.send(p, "gulag.commandNew.broadcast", sender.getName(),
                town.getName()));
    }
}
