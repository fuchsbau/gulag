package de.fuchspfoten.gulag.command;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.gulag.TownConditionHelper;
import de.fuchspfoten.rohrpost.TeleportTask;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /town spawn [town] command.
 */
public class TownSpawnCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public TownSpawnCommand() {
        super("gulag.townHelp.spawn", "gulag.use");
        Messenger.register("gulag.commandSpawn.notExist");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        final TownConditionHelper condition = new TownConditionHelper();
        condition.withIssuer(sender).withActor(player);

        // Determine the target.
        if (args.length >= 1) {
            condition.requireActorTownByName(args[0]);
        } else {
            condition.requireActorInTown();
        }
        if (condition.isFailure()) {
            return;
        }

        final Location spawn = condition.getActorTown().getSpawn();
        if (spawn == null) {
            Messenger.send(sender, "gulag.commandSpawn.notExist");
            return;
        }

        // Teleport the player.
        final TeleportTask tp = new TeleportTask(player, spawn);
        tp.start();
    }
}
