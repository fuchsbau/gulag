package de.fuchspfoten.gulag.event;

import de.fuchspfoten.gulag.model.Town;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

/**
 * Fired when a player crosses town borders.
 */
public class PlayerTownBorderCrossEvent extends PlayerEvent {

    /**
     * The list of event handlers, used by the event API protocol.
     */
    private static final HandlerList handlers = new HandlerList();

    /**
     * Required by event API protocol.
     *
     * @return The list of handlers.
     * @see #getHandlers()
     */
    public static HandlerList getHandlerList() {
        return handlers;
    }

    /**
     * The town the player crosses from. {@code null} for wilderness.
     */
    private @Getter final Town from;

    /**
     * The town the player crosses to. {@code null} for wilderness.
     */
    private @Getter final Town to;

    /**
     * Constructor.
     *
     * @param who The player crossing borders.
     */
    public PlayerTownBorderCrossEvent(final Player who, final Town from, final Town to) {
        super(who);
        this.from = from;
        this.to = to;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}
