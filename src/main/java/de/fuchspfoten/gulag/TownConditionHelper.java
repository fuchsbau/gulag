package de.fuchspfoten.gulag;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.gulag.model.ChunkCoordinate;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownManager;
import lombok.Getter;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

/**
 * Helper for checking conditions about towns.
 */
@SuppressWarnings("UnusedReturnValue")  // Allow chaining.
public class TownConditionHelper {

    /**
     * Registers messages of the condition checks.
     */
    public static void registerMessages() {
        Messenger.register("gulag.alreadyInTown");
        Messenger.register("gulag.chunkAlreadyClaimed");
        Messenger.register("gulag.chunkOwned");
        Messenger.register("gulag.mayorImpossible");
        Messenger.register("gulag.notClaimed");
        Messenger.register("gulag.notEnoughMoney");
        Messenger.register("gulag.notInTown");
        Messenger.register("gulag.noTownWorld");
        Messenger.register("gulag.notSameTown");
        Messenger.register("gulag.targetAlreadyInTown");
        Messenger.register("gulag.targetNotInTown");
        Messenger.register("gulag.townDoesNotExist");
        Messenger.register("gulag.townPermissionMayor");
    }

    /**
     * The issuer of the action that invokes the condition helper.
     */
    private CommandSender issuer = null;

    /**
     * The actor of the action that invokes the condition helper.
     */
    private @Getter OfflinePlayer actor = null;

    /**
     * The town in which the actor is (or a standalone town that is the actor).
     */
    private @Getter Town actorTown = null;

    /**
     * The coordinate of the chunk the actor is in.
     */
    private @Getter ChunkCoordinate actorCoord = null;

    /**
     * The target of the action that invokes the condition helper.
     */
    private @Getter OfflinePlayer target = null;

    /**
     * The town in which the target is (or a standalone town that is the target).
     */
    private @Getter Town targetTown = null;

    /**
     * Whether the condition check has failed.
     */
    private @Getter boolean failure = false;

    /**
     * Requires that the actor stands in a chunk claimed by the actor town.
     *
     * @return this, for chaining.
     */
    public TownConditionHelper requireActorChunkIsOwnedByActorTown() {
        // Quick bail out.
        if (failure) {
            return this;
        }

        // This check requires an actor and an actor town.
        if (actor == null) {
            throw new IllegalStateException("no actor present!");
        }
        if (actorTown == null) {
            throw new IllegalStateException("no actor town present!");
        }

        // The actor has to be online.
        if (!actor.isOnline()) {
            failure = true;
            return this;
        }

        actorCoord = ChunkCoordinate.forChunk(actor.getPlayer().getLocation().getChunk());
        if (TownManager.get().getTownAt(actorCoord) != actorTown) {
            failure = true;
            tellIssuer("gulag.notClaimed");
        }
        return this;
    }

    /**
     * Requires that the actor chunk is not sold.
     *
     * @return this, for chaining.
     */
    public TownConditionHelper requireActorChunkNotSold() {
        // Quick bail out.
        if (failure) {
            return this;
        }

        // This check requires an actor.
        if (actor == null) {
            throw new IllegalStateException("no actor present!");
        }

        // The actor has to be online.
        if (!actor.isOnline()) {
            failure = true;
            return this;
        }

        actorCoord = ChunkCoordinate.forChunk(actor.getPlayer().getLocation().getChunk());
        final Town town = TownManager.get().getTownAt(actorCoord);
        if (town != null && town.getPlotAt(actorCoord).isOwned()) {
            failure = true;
            tellIssuer("gulag.chunkOwned");
        }
        return this;
    }

    /**
     * Requires that the actor stands in a chunk claimed by any town.
     *
     * @return this, for chaining.
     */
    public TownConditionHelper requireActorChunkIsOwnedByAnyTown() {
        // Quick bail out.
        if (failure) {
            return this;
        }

        // This check requires an actor and an actor town.
        if (actor == null) {
            throw new IllegalStateException("no actor present!");
        }

        // The actor has to be online.
        if (!actor.isOnline()) {
            failure = true;
            return this;
        }

        actorCoord = ChunkCoordinate.forChunk(actor.getPlayer().getLocation().getChunk());
        if (TownManager.get().getTownAt(actorCoord) == null) {
            failure = true;
            tellIssuer("gulag.notClaimed");
        }
        return this;
    }

    /**
     * Requires that the actor stands in an unclaimed chunk.
     *
     * @return this, for chaining.
     */
    public TownConditionHelper requireActorChunkIsUnclaimed() {
        // Quick bail out.
        if (failure) {
            return this;
        }

        // This check requires an actor.
        if (actor == null) {
            throw new IllegalStateException("no actor present!");
        }

        // The actor has to be online.
        if (!actor.isOnline()) {
            failure = true;
            return this;
        }

        actorCoord = ChunkCoordinate.forChunk(actor.getPlayer().getLocation().getChunk());
        if (TownManager.get().getTownAt(actorCoord) != null) {
            failure = true;
            tellIssuer("gulag.chunkAlreadyClaimed");
        }
        return this;
    }

    /**
     * Requires the actor to have the given amount of money.
     *
     * @param howMuch The amount of money.
     * @return this, for chaining.
     */
    public TownConditionHelper requireActorHasMoney(final double howMuch) {
        // Quick bail out.
        if (failure) {
            return this;
        }

        // This check requires an actor.
        if (actor == null) {
            throw new IllegalStateException("no actor present!");
        }

        if (!GulagPlugin.getSelf().getEconomy().has(actor, howMuch)) {
            failure = true;
            tellIssuer("gulag.notEnoughMoney");
        }
        return this;
    }

    /**
     * Requires the actor to be in a town.
     *
     * @return this, for chaining.
     */
    public TownConditionHelper requireActorInTown() {
        // Quick bail out.
        if (failure) {
            return this;
        }

        // This check requires an actor.
        if (actor == null) {
            throw new IllegalStateException("no actor present!");
        }

        actorTown = TownManager.get().getTownOf(actor);
        if (actorTown == null) {
            failure = true;
            tellIssuer("gulag.notInTown");
        }
        return this;
    }

    /**
     * Requires that the actor is in the town world.
     *
     * @return this, for chaining.
     */
    public TownConditionHelper requireActorInTownWorld() {
        // Quick bail out.
        if (failure) {
            return this;
        }

        // This check requires an actor.
        if (actor == null) {
            throw new IllegalStateException("no actor present!");
        }

        // The actor has to be online.
        if (!actor.isOnline()) {
            failure = true;
            return this;
        }

        if (!TownManager.get().isTownWorld(actor.getPlayer().getLocation().getWorld())) {
            failure = true;
            tellIssuer("gulag.noTownWorld");
        }
        return this;
    }

    /**
     * Requires that the actor is not mayor of the actor town.
     *
     * @return this, for chaining.
     */
    public TownConditionHelper requireActorIsMayor() {
        // Quick bail out.
        if (failure) {
            return this;
        }

        // This check requires an actor and an actor town.
        if (actor == null) {
            throw new IllegalStateException("no actor present!");
        }
        if (actorTown == null) {
            throw new IllegalStateException("no actor town present!");
        }

        if (!actorTown.getMayorId().equals(actor.getUniqueId())) {
            failure = true;
            tellIssuer("gulag.townPermissionMayor");
        }
        return this;
    }

    /**
     * Requires that the actor is not mayor of the actor town.
     *
     * @return this, for chaining.
     */
    public TownConditionHelper requireActorIsNotMayor() {
        // Quick bail out.
        if (failure) {
            return this;
        }

        // This check requires an actor and an actor town.
        if (actor == null) {
            throw new IllegalStateException("no actor present!");
        }
        if (actorTown == null) {
            throw new IllegalStateException("no actor town present!");
        }

        if (actorTown.getMayorId().equals(actor.getUniqueId())) {
            failure = true;
            tellIssuer("gulag.mayorImpossible");
        }
        return this;
    }

    /**
     * Requires the actor to be in no town.
     *
     * @return this, for chaining.
     */
    public TownConditionHelper requireActorNotInTown() {
        // Quick bail out.
        if (failure) {
            return this;
        }

        // This check requires an actor.
        if (actor == null) {
            throw new IllegalStateException("no actor present!");
        }

        actorTown = TownManager.get().getTownOf(actor);
        if (actorTown != null) {
            failure = true;
            tellIssuer("gulag.alreadyInTown");
        }
        return this;
    }

    /**
     * Requires the town with the given name to exist and sets it as the actor town.
     *
     * @param name The town.
     * @return this, for chaining.
     */
    public TownConditionHelper requireActorTownByName(final String name) {
        // Quick bail out.
        if (failure) {
            return this;
        }

        actorTown = TownManager.get().getTown(name);
        if (actorTown == null) {
            failure = true;
            tellIssuer("gulag.townDoesNotExist");
        }
        return this;
    }

    public TownConditionHelper requireActorTownIsTargetTown() {
        // Quick bail out.
        if (failure) {
            return this;
        }

        // This check requires a target town and an actor town.
        if (targetTown == null) {
            throw new IllegalStateException("no target town present!");
        }
        if (actorTown == null) {
            throw new IllegalStateException("no actor town present!");
        }

        if (actorTown != targetTown) {
            failure = true;
            tellIssuer("gulag.notSameTown");
        }
        return this;
    }

    /**
     * Requires the target to be in a town.
     *
     * @return this, for chaining.
     */
    public TownConditionHelper requireTargetInTown() {
        // Quick bail out.
        if (failure) {
            return this;
        }

        // This check requires an actor.
        if (target == null) {
            throw new IllegalStateException("no target present!");
        }

        targetTown = TownManager.get().getTownOf(target);
        if (targetTown == null) {
            failure = true;
            tellIssuer("gulag.targetNotInTown");
        }
        return this;
    }

    /**
     * Requires the target to be in no town.
     *
     * @return this, for chaining.
     */
    public TownConditionHelper requireTargetNotInTown() {
        // Quick bail out.
        if (failure) {
            return this;
        }

        // This check requires an target.
        if (target == null) {
            throw new IllegalStateException("no target present!");
        }

        targetTown = TownManager.get().getTownOf(target);
        if (targetTown != null) {
            failure = true;
            tellIssuer("gulag.targetAlreadyInTown");
        }
        return this;
    }

    /**
     * Requires the town with the given name to exist and sets it as the target town.
     *
     * @param name The town.
     * @return this, for chaining.
     */
    public TownConditionHelper requireTargetTownByName(final String name) {
        // Quick bail out.
        if (failure) {
            return this;
        }

        targetTown = TownManager.get().getTown(name);
        if (targetTown == null) {
            failure = true;
            tellIssuer("gulag.townDoesNotExist");
        }
        return this;
    }

    /**
     * Sets the actor for this condition helper.
     *
     * @param actor The actor.
     * @return this, for chaining.
     */
    public TownConditionHelper withActor(final OfflinePlayer actor) {
        // Quick bail out.
        if (failure) {
            return this;
        }

        this.actor = actor;
        return this;
    }

    /**
     * Sets the issuer for this condition helper.
     *
     * @param issuer The issuer.
     * @return this, for chaining.
     */
    public TownConditionHelper withIssuer(final CommandSender issuer) {
        // Quick bail out.
        if (failure) {
            return this;
        }

        this.issuer = issuer;
        return this;
    }

    /**
     * Sets the target for this condition helper.
     *
     * @param target The target.
     * @return this, for chaining.
     */
    public TownConditionHelper withTarget(final OfflinePlayer target) {
        // Quick bail out.
        if (failure) {
            return this;
        }

        this.target = target;
        return this;
    }

    /**
     * Sends the issuer a message, if present.
     *
     * @param key  The key of the message.
     * @param data The data for the message.
     */
    private void tellIssuer(final String key, final Object... data) {
        if (issuer != null) {
            Messenger.send(issuer, key, data);
        }
    }
}
