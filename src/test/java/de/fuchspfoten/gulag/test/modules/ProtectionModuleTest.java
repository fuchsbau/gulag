package de.fuchspfoten.gulag.test.modules;

import de.fuchspfoten.fuchslib.tests.TestHelper;
import de.fuchspfoten.gulag.model.ChunkCoordinate;
import de.fuchspfoten.gulag.model.TownManager;
import de.fuchspfoten.gulag.test.GulagTest;
import de.fuchspfoten.mokkit.CancelledByEventException;
import de.fuchspfoten.mokkit.Mokkit;
import de.fuchspfoten.mokkit.block.MokkitBlock;
import de.fuchspfoten.mokkit.entity.living.animal.MokkitPig;
import de.fuchspfoten.mokkit.entity.living.animal.MokkitWolf;
import de.fuchspfoten.mokkit.entity.living.human.MokkitPlayer;
import de.fuchspfoten.mokkit.entity.living.monster.MokkitCreeper;
import de.fuchspfoten.mokkit.entity.projectile.arrow.MokkitArrow;
import de.fuchspfoten.mokkit.entity.projectile.potion.MokkitSplashPotion;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.entity.AreaEffectCloud;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Bat;
import org.bukkit.entity.Blaze;
import org.bukkit.entity.Boat;
import org.bukkit.entity.CaveSpider;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Donkey;
import org.bukkit.entity.DragonFireball;
import org.bukkit.entity.ElderGuardian;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.EnderDragon;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Endermite;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Evoker;
import org.bukkit.entity.Ghast;
import org.bukkit.entity.Giant;
import org.bukkit.entity.Guardian;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Husk;
import org.bukkit.entity.Illusioner;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.LargeFireball;
import org.bukkit.entity.LeashHitch;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Llama;
import org.bukkit.entity.MagmaCube;
import org.bukkit.entity.Mule;
import org.bukkit.entity.MushroomCow;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Painting;
import org.bukkit.entity.Parrot;
import org.bukkit.entity.Pig;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.PolarBear;
import org.bukkit.entity.Rabbit;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.Shulker;
import org.bukkit.entity.Silverfish;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.SkeletonHorse;
import org.bukkit.entity.Slime;
import org.bukkit.entity.SmallFireball;
import org.bukkit.entity.Snowman;
import org.bukkit.entity.SpectralArrow;
import org.bukkit.entity.Spider;
import org.bukkit.entity.SplashPotion;
import org.bukkit.entity.Squid;
import org.bukkit.entity.Stray;
import org.bukkit.entity.TippedArrow;
import org.bukkit.entity.Vex;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Vindicator;
import org.bukkit.entity.Witch;
import org.bukkit.entity.Wither;
import org.bukkit.entity.WitherSkeleton;
import org.bukkit.entity.Wolf;
import org.bukkit.entity.Zombie;
import org.bukkit.entity.ZombieHorse;
import org.bukkit.entity.ZombieVillager;
import org.bukkit.entity.minecart.CommandMinecart;
import org.bukkit.entity.minecart.ExplosiveMinecart;
import org.bukkit.entity.minecart.HopperMinecart;
import org.bukkit.entity.minecart.PoweredMinecart;
import org.bukkit.entity.minecart.RideableMinecart;
import org.bukkit.entity.minecart.SpawnerMinecart;
import org.bukkit.entity.minecart.StorageMinecart;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.PistonBaseMaterial;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.Vector;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

/**
 * Tests for {@link de.fuchspfoten.gulag.modules.ProtectionModule}
 */
@SuppressWarnings({"OptionalGetWithoutIsPresent", "ReuseOfLocalVariable"})
public class ProtectionModuleTest extends GulagTest {

    /**
     * The town world.
     */
    private World townWorld;

    /**
     * The spawn world "world".
     */
    private World spawnWorld;

    /**
     * A mayor of a town.
     */
    private MokkitPlayer mayor;

    /**
     * An outsider.
     */
    private MokkitPlayer outsider;

    /**
     * A player in the wilderness.
     */
    private MokkitPlayer wildernessPlayer;

    /**
     * A player in another world.
     */
    private MokkitPlayer spawnPlayer;

    @Test
    public void ProtectionModuleTest_onBlockBreak() {
        // Place blocks.
        townWorld.getBlockAt(0, 4, 0).setType(Material.WOOD);
        townWorld.getBlockAt(-1, 4, 0).setType(Material.WOOD);
        townWorld.getBlockAt(15, 4, 15).setType(Material.WOOD);
        townWorld.getBlockAt(16, 4, 16).setType(Material.WOOD);
        townWorld.getBlockAt(100, 4, 100).setType(Material.WOOD);
        spawnWorld.getBlockAt(51, 4, 51).setType(Material.WOOD);

        // Mayor can break in town.
        Assert.assertEquals(Material.WOOD, townWorld.getBlockAt(0, 4, 0).getType());
        mayor.mokkit().breakBlock(townWorld.getBlockAt(0, 4, 0));
        Assert.assertEquals(Material.AIR, townWorld.getBlockAt(0, 4, 0).getType());

        // Mayor can not break in wilderness.
        Assert.assertEquals(Material.WOOD, townWorld.getBlockAt(-1, 4, 0).getType());
        TestHelper.assertThrows(() -> mayor.mokkit().breakBlock(townWorld.getBlockAt(-1, 4, 0)),
                CancelledByEventException.class);
        Assert.assertEquals(Material.WOOD, townWorld.getBlockAt(-1, 4, 0).getType());

        // Outsider can not break in town.
        Assert.assertEquals(Material.WOOD, townWorld.getBlockAt(15, 4, 15).getType());
        TestHelper.assertThrows(() -> outsider.mokkit().breakBlock(townWorld.getBlockAt(15, 4, 15)),
                CancelledByEventException.class);
        Assert.assertEquals(Material.WOOD, townWorld.getBlockAt(15, 4, 15).getType());

        // Outsider can not break in wilderness.
        Assert.assertEquals(Material.WOOD, townWorld.getBlockAt(16, 4, 16).getType());
        TestHelper.assertThrows(() -> outsider.mokkit().breakBlock(townWorld.getBlockAt(16, 4, 16)),
                CancelledByEventException.class);
        Assert.assertEquals(Material.WOOD, townWorld.getBlockAt(16, 4, 16).getType());

        // Wilderness player can not break in wilderness.
        Assert.assertEquals(Material.WOOD, townWorld.getBlockAt(100, 4, 100).getType());
        TestHelper.assertThrows(() -> wildernessPlayer.mokkit().breakBlock(townWorld.getBlockAt(100, 4, 100)),
                CancelledByEventException.class);
        Assert.assertEquals(Material.WOOD, townWorld.getBlockAt(100, 4, 100).getType());

        // Spawn player can break blocks.
        Assert.assertEquals(Material.WOOD, spawnWorld.getBlockAt(51, 4, 51).getType());
        spawnPlayer.mokkit().breakBlock(spawnWorld.getBlockAt(51, 4, 51));
        Assert.assertEquals(Material.AIR, spawnWorld.getBlockAt(51, 4, 51).getType());
    }

    @Test
    public void ProtectionModuleTest_onBlockPlace() {
        // Distribute blocks.
        mayor.setItemInHand(new ItemStack(Material.DIRT, 20));
        outsider.setItemInHand(new ItemStack(Material.DIRT, 20));
        wildernessPlayer.setItemInHand(new ItemStack(Material.DIRT, 20));
        spawnPlayer.setItemInHand(new ItemStack(Material.DIRT, 20));

        // Mayor can place in town.
        Assert.assertEquals(Material.AIR, townWorld.getBlockAt(0, 8, 0).getType());
        mayor.mokkit().rightClickBlock(townWorld.getBlockAt(0, 7, 0), BlockFace.UP);
        Assert.assertEquals(Material.DIRT, townWorld.getBlockAt(0, 8, 0).getType());

        // Mayor can not place in wilderness.
        Assert.assertEquals(Material.AIR, townWorld.getBlockAt(-1, 8, 0).getType());
        TestHelper.assertThrows(() -> mayor.mokkit().rightClickBlock(townWorld.getBlockAt(-1, 7, 0),
                BlockFace.UP), CancelledByEventException.class);
        Assert.assertEquals(Material.AIR, townWorld.getBlockAt(-1, 8, 0).getType());

        // Outsider can not place in town.
        Assert.assertEquals(Material.AIR, townWorld.getBlockAt(15, 8, 15).getType());
        TestHelper.assertThrows(() -> outsider.mokkit().rightClickBlock(townWorld.getBlockAt(15, 7, 15),
                BlockFace.UP), CancelledByEventException.class);
        Assert.assertEquals(Material.AIR, townWorld.getBlockAt(15, 8, 15).getType());

        // Outsider can not place in wilderness.
        Assert.assertEquals(Material.AIR, townWorld.getBlockAt(16, 8, 16).getType());
        TestHelper.assertThrows(() -> outsider.mokkit().rightClickBlock(townWorld.getBlockAt(16, 7, 16),
                BlockFace.UP), CancelledByEventException.class);
        Assert.assertEquals(Material.AIR, townWorld.getBlockAt(16, 8, 16).getType());

        // Wilderness player can not place in wilderness.
        Assert.assertEquals(Material.AIR, townWorld.getBlockAt(100, 8, 100).getType());
        TestHelper.assertThrows(() -> wildernessPlayer.mokkit().rightClickBlock(
                townWorld.getBlockAt(100, 7, 100), BlockFace.UP), CancelledByEventException.class);
        Assert.assertEquals(Material.AIR, townWorld.getBlockAt(100, 8, 100).getType());

        // Spawn player can place blocks.
        Assert.assertEquals(Material.AIR, spawnWorld.getBlockAt(51, 8, 51).getType());
        spawnPlayer.mokkit().rightClickBlock(spawnWorld.getBlockAt(51, 7, 51), BlockFace.UP);
        Assert.assertEquals(Material.DIRT, spawnWorld.getBlockAt(51, 8, 51).getType());
    }

    @Test
    public void ProtectionModuleTest_onEntityDamageByEntity_projectile() {
        final Pig target = townWorld.spawn(new Location(townWorld, 10, 10, 10), Pig.class);

        final ProjectileSource[] shooters = new ProjectileSource[]{
                null, mayor, outsider
        };
        for (final ProjectileSource shooter : shooters) {
            final Class<?>[] arrows = new Class[]{
                    Arrow.class, SpectralArrow.class, TippedArrow.class
            };
            final MokkitArrow[] spawnedArrows = new MokkitArrow[arrows.length];
            for (int i = 0; i < arrows.length; i++) {
                //noinspection unchecked
                spawnedArrows[i] = (MokkitArrow) townWorld.spawnArrow(new Location(townWorld, 10, 5 + i, 10),
                        new Vector(0.0f, 1.0f, 0.0f), 1.0f, 1.0f,
                        (Class<? extends Arrow>) arrows[i]);
            }

            for (int i = 0; i < spawnedArrows.length; i++) {
                spawnedArrows[i].setShooter(shooter);

                final double healthBefore = target.getHealth();
                if (shooter == outsider) {
                    // Outsider: Blocked.
                    final int j = i;
                    TestHelper.assertThrows(() -> spawnedArrows[j].mokkit().hitTarget(target),
                            CancelledByEventException.class);
                    Assert.assertEquals(healthBefore, target.getHealth(), 0.01);
                } else {
                    // No shooter / mayor: Works.
                    spawnedArrows[i].mokkit().hitTarget(target);
                    Assert.assertEquals(healthBefore - 1.0, target.getHealth(), 0.01);
                }
            }
        }
    }

    @Test
    public void ProtectionModuleTest_onEntityDamageByEntity_pvp() {
        final Location inTown = new Location(townWorld, 5, 5, 5);
        final Location outOfTown = new Location(townWorld, 20, 5, 20);

        // Mayor in town, outsider outside: Not possible.
        mayor.teleport(inTown);
        outsider.teleport(outOfTown);
        final double healthMayorBefore = mayor.getHealth();
        final double healthOutsiderBefore = outsider.getHealth();
        TestHelper.assertThrows(() -> mayor.mokkit().attackLiving(outsider, 1.0),
                CancelledByEventException.class);
        TestHelper.assertThrows(() -> outsider.mokkit().attackLiving(mayor, 1.0),
                CancelledByEventException.class);
        Assert.assertEquals(healthMayorBefore, mayor.getHealth(), 0.01);
        Assert.assertEquals(healthOutsiderBefore, outsider.getHealth(), 0.01);

        // Mayor outside, outsider in town: Not possible.
        mayor.teleport(outOfTown);
        outsider.teleport(inTown);
        TestHelper.assertThrows(() -> mayor.mokkit().attackLiving(outsider, 1.0),
                CancelledByEventException.class);
        TestHelper.assertThrows(() -> outsider.mokkit().attackLiving(mayor, 1.0),
                CancelledByEventException.class);
        Assert.assertEquals(healthMayorBefore, mayor.getHealth(), 0.01);
        Assert.assertEquals(healthOutsiderBefore, outsider.getHealth(), 0.01);

        // Mayor in town, outsider in town: Not possible.
        mayor.teleport(inTown);
        outsider.teleport(inTown);
        TestHelper.assertThrows(() -> mayor.mokkit().attackLiving(outsider, 1.0),
                CancelledByEventException.class);
        TestHelper.assertThrows(() -> outsider.mokkit().attackLiving(mayor, 1.0),
                CancelledByEventException.class);
        Assert.assertEquals(healthMayorBefore, mayor.getHealth(), 0.01);
        Assert.assertEquals(healthOutsiderBefore, outsider.getHealth(), 0.01);

        // Mayor outside, outsider outside: Possible.
        mayor.teleport(outOfTown);
        outsider.teleport(outOfTown);
        mayor.mokkit().attackLiving(outsider, 1.0);
        outsider.mokkit().attackLiving(mayor, 1.0);
        Assert.assertEquals(healthMayorBefore - 1.0, mayor.getHealth(), 0.01);
        Assert.assertEquals(healthOutsiderBefore - 1.0, outsider.getHealth(), 0.01);
    }

    @Test
    public void ProtectionModuleTest_onEntityDamageByEntity_tamedOwner() {
        final MokkitWolf wolf = (MokkitWolf) townWorld.spawn(new Location(townWorld, 10, 5, 10), Wolf.class);
        final Pig pig = townWorld.spawn(new Location(townWorld, 10, 6, 10), Pig.class);

        // Unassociated wolf can attack pig.
        final double beforePigHealth = pig.getHealth();
        wolf.mokkit().attackLiving(pig, 1.0);
        Assert.assertEquals(beforePigHealth - 1.0, pig.getHealth(), 0.01);

        // Mayor-associated wolf can attack pig.
        wolf.setOwner(mayor);
        wolf.mokkit().attackLiving(pig, 1.0);
        Assert.assertEquals(beforePigHealth - 2.0, pig.getHealth(), 0.01);

        // Outsider-associated wolf can not attack pig.
        wolf.setOwner(outsider);
        TestHelper.assertThrows(() -> wolf.mokkit().attackLiving(pig, 1.0),
                CancelledByEventException.class);
        Assert.assertEquals(beforePigHealth - 2.0, pig.getHealth(), 0.01);
    }

    @Test
    public void ProtectionModuleTest_onEntityDamageByEntity_whitelistLiving() {
        final Class<?>[] entities = new Class[]{
                Pig.class, Wolf.class, Cow.class, Sheep.class, Ocelot.class,
                PolarBear.class, Chicken.class, Rabbit.class, Llama.class, Bat.class,
                Evoker.class, Parrot.class, Spider.class, Witch.class, Enderman.class,
                Guardian.class, ElderGuardian.class, Skeleton.class, WitherSkeleton.class, Stray.class,
                Zombie.class, Husk.class, ZombieVillager.class, SkeletonHorse.class, ZombieHorse.class,
                Donkey.class, Mule.class, Vex.class, Vindicator.class, Illusioner.class,
                Creeper.class, Giant.class, Slime.class, Ghast.class, PigZombie.class,
                CaveSpider.class, Silverfish.class, Blaze.class, MagmaCube.class, EnderDragon.class,
                Wither.class, Endermite.class, Shulker.class, Squid.class, MushroomCow.class,
                Snowman.class, IronGolem.class, Horse.class, Villager.class
        };
        final LivingEntity[] targets = new LivingEntity[entities.length];
        for (int i = 0; i < targets.length; i++) {
            //noinspection unchecked
            targets[i] = townWorld.spawn(new Location(townWorld, 10, 5 + i, 10),
                    (Class<? extends LivingEntity>) entities[i]);
        }
        final boolean[] allowed = new boolean[]{
                false, false, false, false, false,
                true, false, false, false, false,
                true, false, true, true, true,
                true, true, true, true, true,
                true, true, true, false, false,
                false, false, true, true, true,
                true, true, true, true, true,
                true, true, true, true, true,
                true, true, true, false, false,
                false, false, false, false
        };

        for (int i = 0; i < targets.length; i++) {
            final double healthBefore = targets[i].getHealth();
            mayor.mokkit().attackLiving(targets[i], 0.5);
            Assert.assertEquals(healthBefore - 0.5, targets[i].getHealth(), 0.01);
            if (allowed[i]) {
                outsider.mokkit().attackLiving(targets[i], 0.5);
                Assert.assertEquals(healthBefore - 1.0, targets[i].getHealth(), 0.01);
            } else {
                final int j = i;
                TestHelper.assertThrows(() -> outsider.mokkit().attackLiving(targets[j], 0.5),
                        CancelledByEventException.class);
                Assert.assertEquals(healthBefore - 0.5, targets[i].getHealth(), 0.01);
            }
        }
    }

    @Test
    public void ProtectionModuleTest_onEntityDamageByEntity_whitelistNonLiving() {
        final Class<?>[] entities = new Class[]{
                Painting.class, LeashHitch.class, LargeFireball.class, SmallFireball.class, ItemFrame.class,
                DragonFireball.class, ArmorStand.class, EnderCrystal.class, Boat.class, RideableMinecart.class,
                CommandMinecart.class, StorageMinecart.class, PoweredMinecart.class, ExplosiveMinecart.class,
                HopperMinecart.class, SpawnerMinecart.class
        };
        final Entity[] targets = new Entity[entities.length];
        for (int i = 0; i < targets.length; i++) {
            //noinspection unchecked
            targets[i] = townWorld.spawn(new Location(townWorld, 10, 5 + i, 10),
                    (Class<? extends Entity>) entities[i]);
        }
        final boolean[] allowed = new boolean[]{
                false, false, true, false, false,
                true, false, false, false, false,
                false, false, false, false,
                false, false
        };

        for (int i = 0; i < targets.length; i++) {
            mayor.mokkit().damageEntity(targets[i]);
            // No clear thing to check. We need to rely on the events.
            if (allowed[i]) {
                outsider.mokkit().damageEntity(targets[i]);
            } else {
                final int j = i;
                TestHelper.assertThrows(() -> outsider.mokkit().damageEntity(targets[j]),
                        CancelledByEventException.class);
            }
        }
    }

    @Test
    public void ProtectionModuleTest_onEntityDamageByEntity_blockExplosionGriefing() {
        final MokkitCreeper creeper =
                (MokkitCreeper) townWorld.spawn(new Location(townWorld, 10, 5, 10), Creeper.class);
        final ArmorStand armorStand = townWorld.spawn(new Location(townWorld, 8, 6, 8), ArmorStand.class);

        // Creeper can explode-attack mayor.
        creeper.mokkit().attackLiving(mayor, 1.0, DamageCause.ENTITY_EXPLOSION);

        // Creeper can explode-attack outsider.
        creeper.mokkit().attackLiving(outsider, 1.0, DamageCause.ENTITY_EXPLOSION);

        // Creeper can not explode-attack armor stand.
        TestHelper.assertThrows(() -> creeper.mokkit().attackLiving(armorStand, 1.0,
                DamageCause.ENTITY_EXPLOSION), CancelledByEventException.class);
    }

    @Test
    public void ProtectionModuleTest_onEntityDamageByEntity_wolfDefense() {
        final Wolf wolf = townWorld.spawn(new Location(townWorld, 10, 5, 10), Wolf.class);

        // Mayor can attack wolf.
        final double mayorWolfHealth = wolf.getHealth();
        mayor.mokkit().attackLiving(wolf, 1.0);
        Assert.assertEquals(mayorWolfHealth - 1.0, wolf.getHealth(), 0.01);

        // Outsider can not attack neutral wolf.
        final double outsiderWolfHealth = wolf.getHealth();
        TestHelper.assertThrows(() -> outsider.mokkit().attackLiving(wolf, 1.0),
                CancelledByEventException.class);
        Assert.assertEquals(outsiderWolfHealth, wolf.getHealth(), 0.01);

        // Outsider can attack targetting wolf.
        wolf.setTarget(outsider);
        outsider.mokkit().attackLiving(wolf, 1.0);
        Assert.assertEquals(outsiderWolfHealth - 1.0, wolf.getHealth(), 0.01);
    }

    @Test
    public void ProtectionModuleTest_onHangingBreakByEntity() {
        // Create a painting.
        Painting painting = townWorld.spawn(new Location(townWorld, 10, 4, 10), Painting.class);

        // Pig can break.
        final MokkitPig pig = (MokkitPig) townWorld.spawn(new Location(townWorld, 8, 5, 8), Pig.class);
        pig.mokkit().damageEntity(painting);
        Assert.assertTrue(painting.isDead());

        // Respawn.
        painting = townWorld.spawn(new Location(townWorld, 10, 4, 10), Painting.class);

        // Mayor can break.
        mayor.mokkit().damageEntity(painting);
        Assert.assertTrue(painting.isDead());

        // Respawn.
        painting = townWorld.spawn(new Location(townWorld, 10, 4, 10), Painting.class);

        // Outsider can not break.
        final Painting outsiderPainting = painting;
        TestHelper.assertThrows(() -> outsider.mokkit().damageEntity(outsiderPainting),
                CancelledByEventException.class);
        Assert.assertFalse(painting.isDead());

        // Respawn in spawnWorld.
        painting = spawnWorld.spawn(new Location(spawnWorld, 50, 10, 50), Painting.class);

        // Spawn player can break.
        spawnPlayer.mokkit().damageEntity(painting);
        Assert.assertTrue(painting.isDead());
    }

    @Test
    public void ProtectionModuleTest_onHangingPlace() {
        // Distribute paintings.
        mayor.setItemInHand(new ItemStack(Material.PAINTING, 20));
        outsider.setItemInHand(new ItemStack(Material.PAINTING, 20));
        spawnPlayer.setItemInHand(new ItemStack(Material.PAINTING, 20));

        final Block townBlock = townWorld.getBlockAt(7, 10, 7);

        // Check that there are no paintings.
        final Collection<Painting> noPaintings = townWorld.getEntitiesByClass(Painting.class);
        Assert.assertEquals(0, noPaintings.size());

        // Mayor can place.
        mayor.mokkit().rightClickBlock(townBlock, BlockFace.NORTH);
        final Collection<Painting> mayorPaintings = townWorld.getEntitiesByClass(Painting.class);
        Assert.assertEquals(1, mayorPaintings.size());
        mayorPaintings.stream().findAny().get().remove();

        // Outsider can not place.
        TestHelper.assertThrows(() -> outsider.mokkit().rightClickBlock(townBlock, BlockFace.SOUTH),
                CancelledByEventException.class);
        final Collection<Painting> outsiderPaintings = townWorld.getEntitiesByClass(Painting.class);
        Assert.assertEquals(0, outsiderPaintings.size());

        // Spawn player can place.
        final Block spawnBlock = spawnWorld.getBlockAt(45, 7, 45);
        spawnPlayer.mokkit().rightClickBlock(spawnBlock, BlockFace.EAST);
        final Collection<Painting> spawnPaintings = spawnWorld.getEntitiesByClass(Painting.class);
        Assert.assertEquals(1, spawnPaintings.size());
        spawnPaintings.stream().findAny().get().remove();
    }

    @Test
    public void ProtectionModuleTest_onPlayerBucketEmpty() {
        // Distribute buckets.
        mayor.setItemInHand(new ItemStack(Material.WATER_BUCKET));
        outsider.setItemInHand(new ItemStack(Material.LAVA_BUCKET));
        spawnPlayer.setItemInHand(new ItemStack(Material.LAVA_BUCKET));

        // Mayor can empty.
        mayor.mokkit().rightClickBlock(townWorld.getBlockAt(3, 3, 3), BlockFace.UP);
        Assert.assertEquals(Material.WATER, townWorld.getBlockAt(3, 4, 3).getType());
        Assert.assertEquals(Material.BUCKET, mayor.getItemInHand().getType());

        // Outsider can not empty.
        TestHelper.assertThrows(() -> outsider.mokkit().rightClickBlock(townWorld.getBlockAt(15, 14, 15),
                BlockFace.UP), CancelledByEventException.class);
        Assert.assertEquals(Material.AIR, townWorld.getBlockAt(15, 15, 15).getType());
        Assert.assertEquals(Material.LAVA_BUCKET, outsider.getItemInHand().getType());

        TestHelper.assertThrows(() -> outsider.mokkit().rightClickBlock(townWorld.getBlockAt(17, 11, 17),
                BlockFace.UP), CancelledByEventException.class);
        Assert.assertEquals(Material.AIR, townWorld.getBlockAt(17, 12, 17).getType());
        Assert.assertEquals(Material.LAVA_BUCKET, outsider.getItemInHand().getType());

        // Spawn player can empty.
        spawnPlayer.mokkit().rightClickBlock(spawnWorld.getBlockAt(48, 6, 48), BlockFace.UP);
        Assert.assertEquals(Material.LAVA, spawnWorld.getBlockAt(48, 7, 48).getType());
        Assert.assertEquals(Material.BUCKET, spawnPlayer.getItemInHand().getType());
    }

    @Test
    public void ProtectionModuleTest_onPlayerBucketFill() {
        // Distribute buckets.
        mayor.setItemInHand(new ItemStack(Material.BUCKET));
        outsider.setItemInHand(new ItemStack(Material.BUCKET));
        spawnPlayer.setItemInHand(new ItemStack(Material.BUCKET));

        // Place water.
        townWorld.getBlockAt(3, 3, 3).setType(Material.WATER);
        townWorld.getBlockAt(15, 15, 15).setType(Material.WATER);
        townWorld.getBlockAt(17, 12, 17).setType(Material.WATER);
        spawnWorld.getBlockAt(49, 4, 49).setType(Material.WATER);

        // Mayor can fill.
        mayor.mokkit().rightClickBlock(townWorld.getBlockAt(3, 3, 3), BlockFace.SELF);
        Assert.assertEquals(Material.AIR, townWorld.getBlockAt(3, 3, 3).getType());
        Assert.assertEquals(Material.WATER_BUCKET, mayor.getItemInHand().getType());

        // Outsider can not fill.
        TestHelper.assertThrows(() -> outsider.mokkit().rightClickBlock(townWorld.getBlockAt(15, 15, 15),
                BlockFace.SELF), CancelledByEventException.class);
        Assert.assertEquals(Material.WATER, townWorld.getBlockAt(15, 15, 15).getType());
        Assert.assertEquals(Material.BUCKET, outsider.getItemInHand().getType());

        TestHelper.assertThrows(() -> outsider.mokkit().rightClickBlock(townWorld.getBlockAt(17, 12, 17),
                BlockFace.SELF), CancelledByEventException.class);
        Assert.assertEquals(Material.WATER, townWorld.getBlockAt(17, 12, 17).getType());
        Assert.assertEquals(Material.BUCKET, outsider.getItemInHand().getType());

        // Spawn player can fill.
        spawnPlayer.mokkit().rightClickBlock(spawnWorld.getBlockAt(49, 4, 49), BlockFace.SELF);
        Assert.assertEquals(Material.AIR, spawnWorld.getBlockAt(49, 4, 49).getType());
        Assert.assertEquals(Material.WATER_BUCKET, spawnPlayer.getItemInHand().getType());
    }

    @Test
    public void ProtectionModuleTest_onPlayerInteract() {
        // Place blocks in the spawn world.
        spawnWorld.getBlockAt(50, 5, 50).setType(Material.CHEST);
        spawnWorld.getBlockAt(50, 7, 50).setType(Material.WORKBENCH);

        // Spawn player can interact.
        spawnPlayer.mokkit().rightClickBlock(spawnWorld.getBlockAt(50, 5, 50), BlockFace.NORTH);
        spawnPlayer.mokkit().rightClickBlock(spawnWorld.getBlockAt(50, 7, 50), BlockFace.DOWN);

        // Place blocks in the town world.
        final Block[] blocks = new Block[]{
                townWorld.getBlockAt(7, 3, 7), townWorld.getBlockAt(7, 5, 7),
                townWorld.getBlockAt(7, 7, 7), townWorld.getBlockAt(7, 9, 7),
                townWorld.getBlockAt(7, 11, 7), townWorld.getBlockAt(7, 13, 7),
                townWorld.getBlockAt(7, 15, 7)
        };
        final Material[] newType = new Material[]{
                Material.DIRT, Material.ENCHANTMENT_TABLE, Material.ENDER_CHEST, Material.WOOD_BUTTON,
                Material.WORKBENCH, Material.CHEST, Material.BEACON
        };
        final boolean[] allowed = new boolean[]{true, true, true, true, true, false, false};

        // Mayor may interact with everything, outsider only with whitelisted.
        for (int i = 0; i < blocks.length; i++) {
            blocks[i].setType(newType[i]);
            if (allowed[i]) {
                outsider.mokkit().rightClickBlock(blocks[i], BlockFace.NORTH);
            } else {
                final int j = i;
                TestHelper.assertThrows(() -> outsider.mokkit().rightClickBlock(blocks[j], BlockFace.NORTH),
                        CancelledByEventException.class);
            }
            mayor.mokkit().rightClickBlock(blocks[i], BlockFace.SOUTH);
        }
    }

    @Test
    public void ProtectionModuleTest_onPlayerInteractEntity() {
        final Pig pig = townWorld.spawn(new Location(townWorld, 10, 8, 10), Pig.class);

        // Mayor can interact.
        mayor.mokkit().interactWith(pig);

        // Outsider can not interact.
        TestHelper.assertThrows(() -> outsider.mokkit().interactWith(pig), CancelledByEventException.class);

        // Spawn player can interact.
        final Pig spawnPig = spawnWorld.spawn(new Location(spawnWorld, 45, 6, 45), Pig.class);
        spawnPlayer.mokkit().interactWith(spawnPig);
    }

    @Test
    public void ProtectionModuleTest_onPlayerArmorStandManipulate() {
        final ArmorStand armorStand = townWorld.spawn(new Location(townWorld, 10, 8, 10), ArmorStand.class);

        // Mayor can interact.
        mayor.mokkit().manipulateArmorStand(armorStand, EquipmentSlot.HAND);

        // Outsider can not interact.
        TestHelper.assertThrows(() -> outsider.mokkit().manipulateArmorStand(armorStand, EquipmentSlot.HAND),
                CancelledByEventException.class);

        // Spawn player can interact.
        final ArmorStand spawnArmorStand = spawnWorld.spawn(new Location(spawnWorld, 45, 6, 45),
                ArmorStand.class);
        spawnPlayer.mokkit().manipulateArmorStand(spawnArmorStand, EquipmentSlot.HAND);
    }

    @Test
    public void ProtectionModuleTest_onAreaEffectCloudApply() {
        final Pig pigIn = townWorld.spawn(new Location(townWorld, 1, 5, 1), Pig.class);
        final Pig pigWild = townWorld.spawn(new Location(townWorld, -1, 5, -1), Pig.class);

        // Put both in wilderness.
        mayor.teleport(new Location(townWorld, -1, 5, 1));
        wildernessPlayer.teleport(new Location(townWorld, 1, 5, -1));

        Assert.assertFalse(pigIn.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertFalse(pigWild.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertFalse(mayor.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertFalse(wildernessPlayer.hasPotionEffect(PotionEffectType.POISON));

        // Thrown by the mayor. Should affect pig inside, mayor, and the wilderness player.
        final AreaEffectCloud aec1 = townWorld.spawn(new Location(townWorld, 0, 5, 0), AreaEffectCloud.class);
        aec1.setSource(mayor);
        aec1.setBasePotionData(new PotionData(PotionType.POISON));
        Mokkit.simulateTicks(20);
        aec1.remove();

        Assert.assertTrue(pigIn.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertFalse(pigWild.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertTrue(mayor.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertTrue(wildernessPlayer.hasPotionEffect(PotionEffectType.POISON));
        pigIn.removePotionEffect(PotionEffectType.POISON);
        mayor.removePotionEffect(PotionEffectType.POISON);
        wildernessPlayer.removePotionEffect(PotionEffectType.POISON);

        // Thrown by the wilderness player. Should affect mayor and wilderness player.
        final AreaEffectCloud aec2 = townWorld.spawn(new Location(townWorld, 0, 5, 0), AreaEffectCloud.class);
        aec2.setSource(wildernessPlayer);
        aec2.setBasePotionData(new PotionData(PotionType.POISON));
        Mokkit.simulateTicks(20);
        aec2.remove();

        Assert.assertFalse(pigIn.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertFalse(pigWild.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertTrue(mayor.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertTrue(wildernessPlayer.hasPotionEffect(PotionEffectType.POISON));
        mayor.removePotionEffect(PotionEffectType.POISON);
        wildernessPlayer.removePotionEffect(PotionEffectType.POISON);

        // Move one of the players into the town.
        wildernessPlayer.teleport(new Location(townWorld, 1, 5, 1));

        // Thrown by the mayor. Should affect pig inside and the mayor.
        final AreaEffectCloud aec3 = townWorld.spawn(new Location(townWorld, 0, 5, 0), AreaEffectCloud.class);
        aec3.setSource(mayor);
        aec3.setBasePotionData(new PotionData(PotionType.POISON));
        Mokkit.simulateTicks(20);
        aec3.remove();

        Assert.assertTrue(pigIn.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertFalse(pigWild.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertTrue(mayor.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertFalse(wildernessPlayer.hasPotionEffect(PotionEffectType.POISON));
        pigIn.removePotionEffect(PotionEffectType.POISON);
        mayor.removePotionEffect(PotionEffectType.POISON);

        // Thrown by the wilderness player. Should affect wilderness player.
        final AreaEffectCloud aec4 = townWorld.spawn(new Location(townWorld, 0, 5, 0), AreaEffectCloud.class);
        aec4.setSource(wildernessPlayer);
        aec4.setBasePotionData(new PotionData(PotionType.POISON));
        Mokkit.simulateTicks(20);
        aec4.remove();

        Assert.assertFalse(pigIn.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertFalse(pigWild.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertFalse(mayor.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertTrue(wildernessPlayer.hasPotionEffect(PotionEffectType.POISON));
    }

    @Test
    public void ProtectionModuleTest_onBlockPistonExtend_PInTInToOutThrows() {
        final Block piston = townWorld.getBlockAt(5, 5, 14);
        final Block target = townWorld.getBlockAt(5, 5, 15);

        // Place the piston.
        final BlockState bsState = piston.getState();
        bsState.setType(Material.PISTON_BASE);
        final PistonBaseMaterial pbm = new PistonBaseMaterial(Material.PISTON_BASE);
        pbm.setPowered(false);
        pbm.setFacingDirection(BlockFace.SOUTH);
        bsState.setData(pbm);
        bsState.update(true);

        // Place the block.
        target.setType(Material.STONE);

        TestHelper.assertThrows(() -> ((MokkitBlock) piston).mokkit().extendPiston(),
                CancelledByEventException.class);
    }

    @Test
    public void ProtectionModuleTest_onBlockPistonExtend_PInTOutToOutThrows() {
        final Block piston = townWorld.getBlockAt(5, 5, 15);
        final Block target = townWorld.getBlockAt(5, 5, 16);

        // Place the piston.
        final BlockState bsState = piston.getState();
        bsState.setType(Material.PISTON_BASE);
        final PistonBaseMaterial pbm = new PistonBaseMaterial(Material.PISTON_BASE);
        pbm.setPowered(false);
        pbm.setFacingDirection(BlockFace.SOUTH);
        bsState.setData(pbm);
        bsState.update(true);

        // Place the block.
        target.setType(Material.STONE);

        TestHelper.assertThrows(() -> ((MokkitBlock) piston).mokkit().extendPiston(),
                CancelledByEventException.class);
    }

    @Test
    public void ProtectionModuleTest_onBlockPistonExtend_POutTInToIn() {
        final Block piston = townWorld.getBlockAt(5, 5, 1);
        final Block target = townWorld.getBlockAt(6, 5, 1);

        // Place the piston.
        final BlockState bsState = piston.getState();
        bsState.setType(Material.PISTON_BASE);
        final PistonBaseMaterial pbm = new PistonBaseMaterial(Material.PISTON_BASE);
        pbm.setPowered(false);
        pbm.setFacingDirection(BlockFace.EAST);
        bsState.setData(pbm);
        bsState.update(true);

        // Place the block.
        target.setType(Material.STONE);

        ((MokkitBlock) piston).mokkit().extendPiston();
    }

    @Test
    public void ProtectionModuleTest_onBlockPistonExtend_POutTInToInThrows() {
        final Block piston = townWorld.getBlockAt(-1, 5, 1);
        final Block target = townWorld.getBlockAt(0, 5, 1);

        // Place the piston.
        final BlockState bsState = piston.getState();
        bsState.setType(Material.PISTON_BASE);
        final PistonBaseMaterial pbm = new PistonBaseMaterial(Material.PISTON_BASE);
        pbm.setPowered(false);
        pbm.setFacingDirection(BlockFace.EAST);
        bsState.setData(pbm);
        bsState.update(true);

        // Place the block.
        target.setType(Material.STONE);

        TestHelper.assertThrows(() -> ((MokkitBlock) piston).mokkit().extendPiston(),
                CancelledByEventException.class);
    }

    @Test
    public void ProtectionModuleTest_onBlockPistonExtend_POutTOutToInThrows() {
        final Block piston = townWorld.getBlockAt(17, 5, 14);
        final Block target = townWorld.getBlockAt(16, 5, 14);

        // Place the piston.
        final BlockState bsState = piston.getState();
        bsState.setType(Material.PISTON_BASE);
        final PistonBaseMaterial pbm = new PistonBaseMaterial(Material.PISTON_BASE);
        pbm.setPowered(false);
        pbm.setFacingDirection(BlockFace.WEST);
        bsState.setData(pbm);
        bsState.update(true);

        // Place the block.
        target.setType(Material.STONE);

        TestHelper.assertThrows(() -> ((MokkitBlock) piston).mokkit().extendPiston(),
                CancelledByEventException.class);
    }

    @Test
    public void ProtectionModuleTest_onBlockPistonExtend_POutTOutToOut() {
        final Block piston = townWorld.getBlockAt(5, 5, 101);
        final Block target = townWorld.getBlockAt(5, 5, 100);

        // Place the piston.
        final BlockState pistonState = piston.getState();
        pistonState.setType(Material.PISTON_BASE);
        final PistonBaseMaterial pbm = new PistonBaseMaterial(Material.PISTON_BASE);
        pbm.setPowered(false);
        pbm.setFacingDirection(BlockFace.NORTH);
        pistonState.setData(pbm);
        pistonState.update(true);

        // Place the block.
        target.setType(Material.STONE);

        ((MokkitBlock) piston).mokkit().extendPiston();
    }

    @Test
    public void ProtectionModuleTest_onBlockPistonExtend_PullViaPushFromTownThrows() {
        final Block piston = townWorld.getBlockAt(16, 5, 1);
        for (int i = 0; i < 3; i++) {
            final Block target = townWorld.getBlockAt(17, 5, 1 + i);
            target.setType(Material.SLIME_BLOCK);
        }
        final Block targetSlime1 = townWorld.getBlockAt(16, 5, 3);
        targetSlime1.setType(Material.SLIME_BLOCK);
        final Block target = townWorld.getBlockAt(15, 5, 3);
        target.setType(Material.STONE);

        // Place the piston.
        final BlockState bsState = piston.getState();
        bsState.setType(Material.PISTON_BASE);
        final PistonBaseMaterial pbm = new PistonBaseMaterial(Material.PISTON_BASE);
        pbm.setPowered(false);
        pbm.setFacingDirection(BlockFace.EAST);
        bsState.setData(pbm);
        bsState.update(true);

        TestHelper.assertThrows(() -> ((MokkitBlock) piston).mokkit().extendPiston(),
                CancelledByEventException.class);
    }

    @Test
    public void ProtectionModuleTest_onBlockPistonExtend_PushLineIntoTownThrows() {
        final Block piston = townWorld.getBlockAt(21, 5, 1);
        for (int i = 0; i < 5; i++) {
            final Block target = townWorld.getBlockAt(20 - i, 5, 1);
            target.setType(Material.STONE);
        }

        // Place the piston.
        final BlockState bsState = piston.getState();
        bsState.setType(Material.PISTON_BASE);
        final PistonBaseMaterial pbm = new PistonBaseMaterial(Material.PISTON_BASE);
        pbm.setPowered(false);
        pbm.setFacingDirection(BlockFace.WEST);
        bsState.setData(pbm);
        bsState.update(true);

        TestHelper.assertThrows(() -> ((MokkitBlock) piston).mokkit().extendPiston(),
                CancelledByEventException.class);
    }

    @Test
    public void ProtectionModuleTest_onBlockPistonExtend_PushViaSlimeIntoTownThrows() {
        final Block piston = townWorld.getBlockAt(18, 5, 1);
        for (int i = 0; i < 5; i++) {
            final Block target = townWorld.getBlockAt(17, 5, 1 + i);
            target.setType(Material.SLIME_BLOCK);
        }
        final Block target = townWorld.getBlockAt(16, 5, 5);
        target.setType(Material.STONE);

        // Place the piston.
        final BlockState bsState = piston.getState();
        bsState.setType(Material.PISTON_BASE);
        final PistonBaseMaterial pbm = new PistonBaseMaterial(Material.PISTON_BASE);
        pbm.setPowered(false);
        pbm.setFacingDirection(BlockFace.WEST);
        bsState.setData(pbm);
        bsState.update(true);

        TestHelper.assertThrows(() -> ((MokkitBlock) piston).mokkit().extendPiston(),
                CancelledByEventException.class);
    }

    @Test
    public void ProtectionModuleTest_onBlockPistonRetract_PInTInToIn() {
        final Block piston = townWorld.getBlockAt(5, 5, 15);
        final Block target = townWorld.getBlockAt(5, 5, 13);
        target.setType(Material.STONE);

        // Place the piston.
        final BlockState bsState = piston.getState();
        bsState.setType(Material.PISTON_STICKY_BASE);
        final PistonBaseMaterial pbm = new PistonBaseMaterial(Material.PISTON_STICKY_BASE);
        pbm.setPowered(false);
        pbm.setFacingDirection(BlockFace.NORTH);
        bsState.setData(pbm);
        bsState.update(true);
        ((MokkitBlock) piston).mokkit().extendPiston();

        ((MokkitBlock) piston).mokkit().retractPiston();
    }

    @Test
    public void ProtectionModuleTest_onBlockPistonRetract_PInTOutToInThrows() {
        final Block piston = townWorld.getBlockAt(5, 5, 14);
        final Block target = townWorld.getBlockAt(5, 5, 16);
        target.setType(Material.STONE);

        // Place the piston.
        final BlockState bsState = piston.getState();
        bsState.setType(Material.PISTON_STICKY_BASE);
        final PistonBaseMaterial pbm = new PistonBaseMaterial(Material.PISTON_STICKY_BASE);
        pbm.setPowered(false);
        pbm.setFacingDirection(BlockFace.SOUTH);
        bsState.setData(pbm);
        bsState.update(true);
        ((MokkitBlock) piston).mokkit().extendPiston();

        TestHelper.assertThrows(() -> ((MokkitBlock) piston).mokkit().retractPiston(), CancelledByEventException.class);
    }

    @Test
    public void ProtectionModuleTest_onBlockPistonRetract_PInTOutToOutThrows() {
        final Block piston = townWorld.getBlockAt(5, 5, 15);
        final Block target = townWorld.getBlockAt(5, 5, 17);
        target.setType(Material.STONE);

        // Place the piston.
        final BlockState bsState = piston.getState();
        bsState.setType(Material.PISTON_STICKY_BASE);
        final PistonBaseMaterial pbm = new PistonBaseMaterial(Material.PISTON_STICKY_BASE);
        pbm.setPowered(false);
        pbm.setFacingDirection(BlockFace.SOUTH);
        bsState.setData(pbm);
        bsState.update(true);
        ((MokkitBlock) piston).mokkit().extendPiston();

        TestHelper.assertThrows(() -> ((MokkitBlock) piston).mokkit().retractPiston(), CancelledByEventException.class);
    }

    @Test
    public void ProtectionModuleTest_onBlockPistonRetract_POutTInToInThrows() {
        final Block piston = townWorld.getBlockAt(5, 5, 16);
        final Block target = townWorld.getBlockAt(5, 5, 14);
        target.setType(Material.STONE);

        // Place the piston.
        final BlockState bsState = piston.getState();
        bsState.setType(Material.PISTON_STICKY_BASE);
        final PistonBaseMaterial pbm = new PistonBaseMaterial(Material.PISTON_STICKY_BASE);
        pbm.setPowered(false);
        pbm.setFacingDirection(BlockFace.NORTH);
        bsState.setData(pbm);
        bsState.update(true);
        ((MokkitBlock) piston).mokkit().extendPiston();

        TestHelper.assertThrows(() -> ((MokkitBlock) piston).mokkit().retractPiston(), CancelledByEventException.class);
    }

    @Test
    public void ProtectionModuleTest_onBlockPistonRetract_POutTInToOutThrows() {
        final Block piston = townWorld.getBlockAt(5, 5, 17);
        final Block target = townWorld.getBlockAt(5, 5, 15);
        target.setType(Material.STONE);

        // Place the piston.
        final BlockState bsState = piston.getState();
        bsState.setType(Material.PISTON_STICKY_BASE);
        final PistonBaseMaterial pbm = new PistonBaseMaterial(Material.PISTON_STICKY_BASE);
        pbm.setPowered(false);
        pbm.setFacingDirection(BlockFace.NORTH);
        bsState.setData(pbm);
        bsState.update(true);
        ((MokkitBlock) piston).mokkit().extendPiston();

        TestHelper.assertThrows(() -> ((MokkitBlock) piston).mokkit().retractPiston(), CancelledByEventException.class);
    }

    @Test
    public void ProtectionModuleTest_onBlockPistonRetract_POutTOutToOut() {
        final Block piston = townWorld.getBlockAt(5, 5, 20);
        final Block target = townWorld.getBlockAt(5, 5, 19);
        target.setType(Material.STONE);

        // Place the piston.
        final BlockState bsState = piston.getState();
        bsState.setType(Material.PISTON_STICKY_BASE);
        final PistonBaseMaterial pbm = new PistonBaseMaterial(Material.PISTON_STICKY_BASE);
        pbm.setPowered(false);
        pbm.setFacingDirection(BlockFace.NORTH);
        bsState.setData(pbm);
        bsState.update(true);
        ((MokkitBlock) piston).mokkit().extendPiston();

        ((MokkitBlock) piston).mokkit().retractPiston();
    }

    @Test
    public void ProtectionModuleTest_onBlockPistonRetract_PullInFromOutViaSlimeThrows() {
        final Block piston = townWorld.getBlockAt(5, 5, 16);
        townWorld.getBlockAt(3, 5, 16).setType(Material.STONE);
        townWorld.getBlockAt(3, 5, 17).setType(Material.SLIME_BLOCK);
        townWorld.getBlockAt(3, 5, 18).setType(Material.SLIME_BLOCK);
        townWorld.getBlockAt(4, 5, 18).setType(Material.SLIME_BLOCK);
        townWorld.getBlockAt(5, 5, 18).setType(Material.SLIME_BLOCK);

        // Place the piston.
        final BlockState bsState = piston.getState();
        bsState.setType(Material.PISTON_STICKY_BASE);
        final PistonBaseMaterial pbm = new PistonBaseMaterial(Material.PISTON_STICKY_BASE);
        pbm.setPowered(false);
        pbm.setFacingDirection(BlockFace.SOUTH);
        bsState.setData(pbm);
        bsState.update(true);
        ((MokkitBlock) piston).mokkit().extendPiston();

        TestHelper.assertThrows(() -> ((MokkitBlock) piston).mokkit().retractPiston(), CancelledByEventException.class);
    }

    @Test
    public void ProtectionModuleTest_onBlockPistonRetract_PullOutViaSlimeThrows() {
        final Block piston = townWorld.getBlockAt(5, 5, 21);
        final Block target = townWorld.getBlockAt(5, 5, 14);
        target.setType(Material.STONE);

        for (int i = 0; i < 5; i++) {
            townWorld.getBlockAt(5, 5, 15 + i).setType(Material.SLIME_BLOCK);
        }

        // Place the piston.
        final BlockState bsState = piston.getState();
        bsState.setType(Material.PISTON_STICKY_BASE);
        final PistonBaseMaterial pbm = new PistonBaseMaterial(Material.PISTON_STICKY_BASE);
        pbm.setPowered(false);
        pbm.setFacingDirection(BlockFace.NORTH);
        bsState.setData(pbm);
        bsState.update(true);
        ((MokkitBlock) piston).mokkit().extendPiston();

        TestHelper.assertThrows(() -> ((MokkitBlock) piston).mokkit().retractPiston(), CancelledByEventException.class);
    }

    @Test
    public void ProtectionModuleTest_onPotionSplash() {
        final Pig pigIn = townWorld.spawn(new Location(townWorld, 1, 5, 1), Pig.class);
        final Pig pigWild = townWorld.spawn(new Location(townWorld, -1, 5, -1), Pig.class);

        // Put both in wilderness.
        mayor.teleport(new Location(townWorld, -1, 5, 1));
        wildernessPlayer.teleport(new Location(townWorld, 1, 5, -1));

        // Thrown by the mayor. Should affect pig inside, mayor, and the wilderness player.
        final MokkitSplashPotion pot1 =
                (MokkitSplashPotion) townWorld.spawn(new Location(townWorld, 0, 5, 0), SplashPotion.class);
        pot1.mokkit().addEffect(PotionEffectType.POISON.createEffect(20, 0));
        pot1.setShooter(mayor);
        pot1.mokkit().splash();
        Assert.assertTrue(pigIn.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertTrue(mayor.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertTrue(wildernessPlayer.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertFalse(pigWild.hasPotionEffect(PotionEffectType.POISON));

        pigIn.removePotionEffect(PotionEffectType.POISON);
        mayor.removePotionEffect(PotionEffectType.POISON);
        wildernessPlayer.removePotionEffect(PotionEffectType.POISON);

        // Thrown by the wilderness player. Should affect mayor and the wilderness player.
        final MokkitSplashPotion pot2 =
                (MokkitSplashPotion) townWorld.spawn(new Location(townWorld, 0, 5, 0), SplashPotion.class);
        pot2.mokkit().addEffect(PotionEffectType.POISON.createEffect(20, 0));
        pot2.setShooter(wildernessPlayer);
        pot2.mokkit().splash();
        Assert.assertTrue(mayor.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertTrue(wildernessPlayer.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertFalse(pigIn.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertFalse(pigWild.hasPotionEffect(PotionEffectType.POISON));

        mayor.removePotionEffect(PotionEffectType.POISON);
        wildernessPlayer.removePotionEffect(PotionEffectType.POISON);

        // Move one of the players into the town.
        mayor.teleport(new Location(townWorld, 1, 5, 1));

        // Thrown by the mayor. Should affect the mayor and the pig inside.
        final MokkitSplashPotion pot3 =
                (MokkitSplashPotion) townWorld.spawn(new Location(townWorld, 0, 5, 0), SplashPotion.class);
        pot3.mokkit().addEffect(PotionEffectType.POISON.createEffect(20, 0));
        pot3.setShooter(mayor);
        pot3.mokkit().splash();
        Assert.assertTrue(mayor.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertTrue(pigIn.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertFalse(wildernessPlayer.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertFalse(pigWild.hasPotionEffect(PotionEffectType.POISON));

        mayor.removePotionEffect(PotionEffectType.POISON);
        pigIn.removePotionEffect(PotionEffectType.POISON);

        // Thrown by the wilderness player. Should affect the wilderness player.
        final MokkitSplashPotion pot4 =
                (MokkitSplashPotion) townWorld.spawn(new Location(townWorld, 0, 5, 0), SplashPotion.class);
        pot4.mokkit().addEffect(PotionEffectType.POISON.createEffect(20, 0));
        pot4.setShooter(wildernessPlayer);
        pot4.mokkit().splash();
        Assert.assertTrue(wildernessPlayer.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertFalse(pigIn.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertFalse(mayor.hasPotionEffect(PotionEffectType.POISON));
        Assert.assertFalse(pigWild.hasPotionEffect(PotionEffectType.POISON));
    }

    @Before
    public void createTownWorld() {
        spawnWorld = server.getWorld("world");
        townWorld = server.createWorld(new WorldCreator("townworld"));

        mayor = server.mokkit().joinPlayer("mayor");
        mayor.teleport(new Location(townWorld, 4, 5, 2));
        TownManager.get().createTown("test", ChunkCoordinate.forChunk(mayor.getLocation().getChunk()),
                mayor.getUniqueId());

        outsider = server.mokkit().joinPlayer("outsider");
        outsider.teleport(new Location(townWorld, 15, 5, 15));

        wildernessPlayer = server.mokkit().joinPlayer("wildernessPlayer");
        wildernessPlayer.teleport(new Location(townWorld, 100, 5, 100));

        spawnPlayer = server.mokkit().joinPlayer("spawnPlayer");
        spawnPlayer.teleport(new Location(spawnWorld, 50, 5, 50));
    }
}
