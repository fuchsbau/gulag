package de.fuchspfoten.gulag.test;

import de.fuchspfoten.fuchslib.FuchsLibPlugin;
import de.fuchspfoten.fuchslib.tests.BasePluginTest;
import de.fuchspfoten.geldspeicher.GeldspeicherPlugin;
import de.fuchspfoten.gulag.GulagPlugin;

/**
 * Adapter for plugin tests.
 */
@SuppressWarnings("AbstractClassExtendsConcreteClass")
public abstract class GulagTest extends BasePluginTest<GulagPlugin> {

    /**
     * Constructor.
     */
    protected GulagTest() {
        super(GulagPlugin.class, FuchsLibPlugin.class, GeldspeicherPlugin.class);
    }
}
