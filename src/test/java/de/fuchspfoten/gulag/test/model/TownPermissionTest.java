package de.fuchspfoten.gulag.test.model;

import de.fuchspfoten.gulag.model.TownPermission;
import de.fuchspfoten.gulag.test.GulagTest;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Tests for {@link de.fuchspfoten.gulag.model.TownPermission}.
 */
public class TownPermissionTest extends GulagTest {

    @Test
    public void TownPermissionTest_noDuplicateNodes() {
        final Set<String> nodes = Arrays.stream(TownPermission.values())
                .map(TownPermission::getNode)
                .collect(Collectors.toSet());
        Assert.assertEquals(TownPermission.values().length, nodes.size());
    }
}
