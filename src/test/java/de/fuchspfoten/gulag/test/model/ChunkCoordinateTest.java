package de.fuchspfoten.gulag.test.model;

import de.fuchspfoten.gulag.model.ChunkCoordinate;
import de.fuchspfoten.gulag.test.GulagTest;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.junit.Assert;
import org.junit.Test;

/**
 * Tests for {@link de.fuchspfoten.gulag.model.ChunkCoordinate}.
 */
public class ChunkCoordinateTest extends GulagTest {

    @Test
    public void ChunkCoordinateTest_forChunk() {
        final World world = server.getWorld("world");
        final Chunk someChunk = world.getChunkAt(201, 32);

        final ChunkCoordinate coord = ChunkCoordinate.forChunk(someChunk);
        Assert.assertEquals(someChunk.getX(), coord.getX());
        Assert.assertEquals(someChunk.getZ(), coord.getZ());
    }
}
