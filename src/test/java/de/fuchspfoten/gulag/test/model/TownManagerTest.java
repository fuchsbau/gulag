package de.fuchspfoten.gulag.test.model;

import de.fuchspfoten.fuchslib.tests.TestHelper;
import de.fuchspfoten.gulag.model.ChunkCoordinate;
import de.fuchspfoten.gulag.model.Plot;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownManager;
import de.fuchspfoten.gulag.model.TownPermission;
import de.fuchspfoten.gulag.test.GulagTest;
import de.fuchspfoten.mokkit.MokkitWorld;
import de.fuchspfoten.mokkit.entity.living.human.MokkitPlayer;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.WorldCreator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests for {@link TownManager}
 */
public class TownManagerTest extends GulagTest {

    /**
     * The town world.
     */
    private MokkitWorld townWorld;

    @Test
    public void TownManagerTest_claimChunk() {
        final TownManager tm = TownManager.get();

        final MokkitPlayer mayor = server.mokkit().joinPlayer("mayor");
        mayor.teleport(new Location(townWorld, 0, 10, 0));
        final ChunkCoordinate mayorChunk = ChunkCoordinate.forChunk(mayor.getLocation().getChunk());

        final ChunkCoordinate chunk1 = new ChunkCoordinate(mayorChunk.getX() + 1, mayorChunk.getZ());
        final ChunkCoordinate chunk2 = new ChunkCoordinate(mayorChunk.getX() + 2, mayorChunk.getZ());

        // New town.
        final Town town = tm.createTown("test", mayorChunk, mayor.getUniqueId());
        Assert.assertEquals(1, town.getPlots().size());
        Assert.assertEquals(mayorChunk, town.getPlotAt(mayorChunk).getCoordinate());
        Assert.assertNull(town.getPlotAt(chunk1));
        Assert.assertNull(town.getPlotAt(chunk2));

        // Can not reclaim chunk.
        TestHelper.assertThrows(() -> tm.claimChunk(town, mayorChunk), IllegalArgumentException.class);
        Assert.assertEquals(1, town.getPlots().size());

        // First claim.
        tm.claimChunk(town, chunk1);
        Assert.assertEquals(2, town.getPlots().size());
        Assert.assertEquals(mayorChunk, town.getPlotAt(mayorChunk).getCoordinate());
        Assert.assertEquals(chunk1, town.getPlotAt(chunk1).getCoordinate());
        Assert.assertNull(town.getPlotAt(chunk2));

        // Can not reclaim chunk.
        TestHelper.assertThrows(() -> tm.claimChunk(town, chunk1), IllegalArgumentException.class);
        Assert.assertEquals(2, town.getPlots().size());

        // Second claim.
        tm.claimChunk(town, chunk2);
        Assert.assertEquals(3, town.getPlots().size());
        Assert.assertEquals(mayorChunk, town.getPlotAt(mayorChunk).getCoordinate());
        Assert.assertEquals(chunk1, town.getPlotAt(chunk1).getCoordinate());
        Assert.assertEquals(chunk2, town.getPlotAt(chunk2).getCoordinate());

        // Can not reclaim chunk.
        TestHelper.assertThrows(() -> tm.claimChunk(town, chunk2), IllegalArgumentException.class);
        Assert.assertEquals(3, town.getPlots().size());
    }

    @Test
    public void TownManagerTest_createTown() {
        final TownManager tm = TownManager.get();

        final MokkitPlayer mayor = server.mokkit().joinPlayer("mayor");
        mayor.teleport(new Location(townWorld, 0, 10, 0));
        final ChunkCoordinate mayorChunk = ChunkCoordinate.forChunk(mayor.getLocation().getChunk());

        final MokkitPlayer other = server.mokkit().joinPlayer("other");
        other.teleport(new Location(townWorld, 100, 10, 100));
        final ChunkCoordinate otherChunk = ChunkCoordinate.forChunk(other.getLocation().getChunk());

        // Create a town called "test".
        tm.createTown("test", mayorChunk, mayor.getUniqueId());
        Assert.assertNotNull(tm.getTown("test"));
        Assert.assertNotNull(tm.getTownAt(mayorChunk));
        Assert.assertNotNull(tm.getTownOf(mayor));
        Assert.assertEquals(1, tm.getTowns().size());

        // Creating another town called "test" fails.
        TestHelper.assertThrows(() -> tm.createTown("test", otherChunk, other.getUniqueId()),
                IllegalArgumentException.class);
        Assert.assertEquals(1, tm.getTowns().size());

        // Creating another town in the same chunk fails.
        TestHelper.assertThrows(() -> tm.createTown("test2", mayorChunk, other.getUniqueId()),
                IllegalArgumentException.class);
        Assert.assertEquals(1, tm.getTowns().size());

        // Creating another town with the same mayor fails.
        TestHelper.assertThrows(() -> tm.createTown("test2", otherChunk, mayor.getUniqueId()),
                IllegalArgumentException.class);
        Assert.assertEquals(1, tm.getTowns().size());
    }

    @Test
    public void TownManagerTest_getCost() {
        final TownManager tm = TownManager.get();
        Assert.assertTrue(tm.getCost("create") > 0);
        Assert.assertTrue(tm.getCost("claim") > 0);
        Assert.assertTrue(tm.getCost("upkeep") > 0);
        Assert.assertTrue(tm.getCost("create") > tm.getCost("claim"));
        Assert.assertTrue(tm.getCost("claim") > tm.getCost("upkeep"));
    }

    @Test
    public void TownManagerTest_getTown() {
        final TownManager tm = TownManager.get();

        final MokkitPlayer mayor1 = server.mokkit().joinPlayer("mayor1");
        mayor1.teleport(new Location(townWorld, 0, 10, 0));
        final ChunkCoordinate mayor1Chunk = ChunkCoordinate.forChunk(mayor1.getLocation().getChunk());

        final MokkitPlayer mayor2 = server.mokkit().joinPlayer("mayor2");
        mayor2.teleport(new Location(townWorld, 100, 10, 100));
        final ChunkCoordinate mayor2Chunk = ChunkCoordinate.forChunk(mayor2.getLocation().getChunk());

        // No town.
        Assert.assertNull(tm.getTown("test"));
        Assert.assertNull(tm.getTown("test2"));
        Assert.assertNull(tm.getTown("xyz"));

        // One town.
        final Town town1 = tm.createTown("test", mayor1Chunk, mayor1.getUniqueId());
        Assert.assertEquals(town1, tm.getTown("test"));
        Assert.assertNull(tm.getTown("test2"));
        Assert.assertNull(tm.getTown("xyz"));

        // Two towns.
        final Town town2 = tm.createTown("test2", mayor2Chunk, mayor2.getUniqueId());
        Assert.assertEquals(town1, tm.getTown("test"));
        Assert.assertEquals(town2, tm.getTown("test2"));
        Assert.assertNull(tm.getTown("xyz"));

        // One town.
        tm.removeTown(town1);
        Assert.assertNull(tm.getTown("test"));
        Assert.assertEquals(town2, tm.getTown("test2"));
        Assert.assertNull(tm.getTown("xyz"));

        // No towns.
        tm.removeTown(town2);
        Assert.assertNull(tm.getTown("test"));
        Assert.assertNull(tm.getTown("test2"));
        Assert.assertNull(tm.getTown("xyz"));
    }

    @Test
    public void TownManagerTest_getTownAt() {
        final TownManager tm = TownManager.get();

        final MokkitPlayer mayor1 = server.mokkit().joinPlayer("mayor1");
        mayor1.teleport(new Location(townWorld, 0, 10, 0));
        final ChunkCoordinate mayor1Chunk = ChunkCoordinate.forChunk(mayor1.getLocation().getChunk());

        final MokkitPlayer mayor2 = server.mokkit().joinPlayer("mayor2");
        mayor2.teleport(new Location(townWorld, 100, 10, 100));
        final ChunkCoordinate mayor2Chunk = ChunkCoordinate.forChunk(mayor2.getLocation().getChunk());

        final ChunkCoordinate otherChunk = new ChunkCoordinate(-20, -20);

        // No town.
        Assert.assertNull(tm.getTownAt(mayor1Chunk));
        Assert.assertNull(tm.getTownAt(mayor2Chunk));
        Assert.assertNull(tm.getTownAt(otherChunk));

        // One town.
        final Town town1 = tm.createTown("Test", mayor1Chunk, mayor1.getUniqueId());
        Assert.assertEquals(town1, tm.getTownAt(mayor1Chunk));
        Assert.assertNull(tm.getTownAt(mayor2Chunk));
        Assert.assertNull(tm.getTownAt(otherChunk));

        // Two towns.
        final Town town2 = tm.createTown("Test2", mayor2Chunk, mayor2.getUniqueId());
        Assert.assertEquals(town1, tm.getTownAt(mayor1Chunk));
        Assert.assertEquals(town2, tm.getTownAt(mayor2Chunk));
        Assert.assertNull(tm.getTownAt(otherChunk));

        // One town.
        tm.removeTown(town1);
        Assert.assertNull(tm.getTownAt(mayor1Chunk));
        Assert.assertEquals(town2, tm.getTownAt(mayor2Chunk));
        Assert.assertNull(tm.getTownAt(otherChunk));

        // No towns.
        tm.removeTown(town2);
        Assert.assertNull(tm.getTownAt(mayor1Chunk));
        Assert.assertNull(tm.getTownAt(mayor2Chunk));
        Assert.assertNull(tm.getTownAt(otherChunk));
    }

    @Test
    public void TownManagerTest_getTownOf() {
        final TownManager tm = TownManager.get();

        final MokkitPlayer mayor1 = server.mokkit().joinPlayer("mayor1");
        mayor1.teleport(new Location(townWorld, 0, 10, 0));
        final ChunkCoordinate mayor1Chunk = ChunkCoordinate.forChunk(mayor1.getLocation().getChunk());

        final MokkitPlayer mayor2 = server.mokkit().joinPlayer("mayor2");
        mayor2.teleport(new Location(townWorld, 100, 10, 100));
        final ChunkCoordinate mayor2Chunk = ChunkCoordinate.forChunk(mayor2.getLocation().getChunk());

        final MokkitPlayer player = server.mokkit().joinPlayer("player");

        // No town.
        Assert.assertNull(tm.getTownOf(mayor1));
        Assert.assertNull(tm.getTownOf(mayor2));
        Assert.assertNull(tm.getTownOf(player));

        // One town.
        final Town town1 = tm.createTown("Test", mayor1Chunk, mayor1.getUniqueId());
        Assert.assertEquals(town1, tm.getTownOf(mayor1));
        Assert.assertNull(tm.getTownOf(mayor2));
        Assert.assertNull(tm.getTownOf(player));

        // Two towns.
        final Town town2 = tm.createTown("Test2", mayor2Chunk, mayor2.getUniqueId());
        Assert.assertEquals(town1, tm.getTownOf(mayor1));
        Assert.assertEquals(town2, tm.getTownOf(mayor2));
        Assert.assertNull(tm.getTownOf(player));

        // One town.
        tm.removeTown(town1);
        Assert.assertNull(tm.getTownOf(mayor1));
        Assert.assertEquals(town2, tm.getTownOf(mayor2));
        Assert.assertNull(tm.getTownOf(player));

        // No towns.
        tm.removeTown(town2);
        Assert.assertNull(tm.getTownOf(mayor1));
        Assert.assertNull(tm.getTownOf(mayor2));
        Assert.assertNull(tm.getTownOf(player));
    }

    @Test
    public void TownManagerTest_getTowns() {
        final TownManager tm = TownManager.get();

        final MokkitPlayer mayor1 = server.mokkit().joinPlayer("mayor1");
        mayor1.teleport(new Location(townWorld, 0, 10, 0));
        final ChunkCoordinate mayor1Chunk = ChunkCoordinate.forChunk(mayor1.getLocation().getChunk());

        final MokkitPlayer mayor2 = server.mokkit().joinPlayer("mayor2");
        mayor2.teleport(new Location(townWorld, 100, 10, 100));
        final ChunkCoordinate mayor2Chunk = ChunkCoordinate.forChunk(mayor2.getLocation().getChunk());

        // No towns.
        Assert.assertEquals(0, tm.getTowns().size());

        // One town.
        final Town town1 = tm.createTown("Test", mayor1Chunk, mayor1.getUniqueId());
        Assert.assertEquals(1, tm.getTowns().size());
        Assert.assertTrue(tm.getTowns().contains(town1));

        // Two towns.
        final Town town2 = tm.createTown("Test2", mayor2Chunk, mayor2.getUniqueId());
        Assert.assertEquals(2, tm.getTowns().size());
        Assert.assertTrue(tm.getTowns().contains(town1));
        Assert.assertTrue(tm.getTowns().contains(town2));

        // Delete town1.
        tm.removeTown(town1);
        Assert.assertEquals(1, tm.getTowns().size());
        Assert.assertFalse(tm.getTowns().contains(town1));
        Assert.assertTrue(tm.getTowns().contains(town2));

        // Delete town2.
        tm.removeTown(town2);
        Assert.assertEquals(0, tm.getTowns().size());
        Assert.assertFalse(tm.getTowns().contains(town1));
        Assert.assertFalse(tm.getTowns().contains(town2));
    }

    @Test
    public void TownManagerTest_isAllowedByProtection() {
        final TownManager tm = TownManager.get();

        // Locations.
        final Location wildernessLocation = new Location(townWorld, 20, 5, 20);
        final Location townLocation = new Location(townWorld, 10, 5, 10);

        // Mayor and town.
        final MokkitPlayer mayor = server.mokkit().joinPlayer("mayor");
        mayor.teleport(new Location(townWorld, 0, 10, 0));
        final ChunkCoordinate mayorChunk = ChunkCoordinate.forChunk(mayor.getLocation().getChunk());
        final Town town = tm.createTown("test", mayorChunk, mayor.getUniqueId());
        final Plot townPlot = town.getPlotAt(mayorChunk);

        // Town member player.
        final MokkitPlayer member = server.mokkit().joinPlayer("member");
        tm.joinTown(town, member.getUniqueId());

        // Outsider player.
        final MokkitPlayer outsider = server.mokkit().joinPlayer("outsider");

        // Operator player.
        final MokkitPlayer operator = server.mokkit().joinPlayer("operator");
        operator.setOp(true);

        // Operator can bypass.
        Assert.assertTrue(tm.isAllowedByProtection(operator, wildernessLocation));
        Assert.assertTrue(tm.isAllowedByProtection(operator, townLocation));

        // Creative can also bypass.
        operator.setOp(false);
        operator.setGameMode(GameMode.CREATIVE);
        Assert.assertTrue(tm.isAllowedByProtection(operator, wildernessLocation));
        Assert.assertTrue(tm.isAllowedByProtection(operator, townLocation));

        // Mayor and outsider tests.
        Assert.assertTrue(tm.isAllowedByProtection(mayor, townLocation));
        Assert.assertFalse(tm.isAllowedByProtection(mayor, wildernessLocation));
        Assert.assertFalse(tm.isAllowedByProtection(outsider, townLocation));
        Assert.assertFalse(tm.isAllowedByProtection(outsider, wildernessLocation));

        // Regular member can not bypass.
        Assert.assertFalse(tm.isAllowedByProtection(member, townLocation));
        Assert.assertFalse(tm.isAllowedByProtection(member, wildernessLocation));

        // Plot owner can bypass.
        townPlot.setOwnerId(member.getUniqueId());
        Assert.assertTrue(tm.isAllowedByProtection(member, townLocation));
        Assert.assertFalse(tm.isAllowedByProtection(member, wildernessLocation));

        // Member can not bypass on other-owned.
        townPlot.setOwnerId(mayor.getUniqueId());
        Assert.assertFalse(tm.isAllowedByProtection(member, townLocation));
        Assert.assertFalse(tm.isAllowedByProtection(member, wildernessLocation));

        // Member can bypass on plot with friend status.
        townPlot.addMember(member.getUniqueId());
        Assert.assertTrue(tm.isAllowedByProtection(member, townLocation));
        Assert.assertFalse(tm.isAllowedByProtection(member, wildernessLocation));

        // Disown the plot.
        townPlot.setOwnerId(null);
        townPlot.removeMember(member.getUniqueId());

        // Create a rank with PROTECTION_OWNED permission and a rank with PROTECTION_CITY permission.
        town.createRank("r1");
        town.getRank("r1").addPermission(TownPermission.PROTECTION_OWNED.getNode());
        town.createRank("r2");
        town.getRank("r2").addPermission(TownPermission.PROTECTION_CITY.getNode());

        // Member in r2 can modify unowned but can not modify owned.
        town.setRank(member.getUniqueId(), town.getRank("r2"));
        Assert.assertTrue(tm.isAllowedByProtection(member, townLocation));
        Assert.assertFalse(tm.isAllowedByProtection(member, wildernessLocation));
        townPlot.setOwnerId(mayor.getUniqueId());
        Assert.assertFalse(tm.isAllowedByProtection(member, townLocation));
        Assert.assertFalse(tm.isAllowedByProtection(member, wildernessLocation));

        // Member in r1 can modify owned but can not modify unowned.
        town.setRank(member.getUniqueId(), town.getRank("r1"));
        Assert.assertTrue(tm.isAllowedByProtection(member, townLocation));
        Assert.assertFalse(tm.isAllowedByProtection(member, wildernessLocation));
        townPlot.setOwnerId(null);
        Assert.assertFalse(tm.isAllowedByProtection(member, townLocation));
        Assert.assertFalse(tm.isAllowedByProtection(member, wildernessLocation));
    }

    @Test
    public void TownManagerTest_isTownWorld() {
        final TownManager tm = TownManager.get();
        Assert.assertTrue(tm.isTownWorld(townWorld));
        Assert.assertFalse(tm.isTownWorld(server.getWorld("world")));
    }

    @Test
    public void TownManagerTest_joinTown() {
        final TownManager tm = TownManager.get();

        final MokkitPlayer mayor = server.mokkit().joinPlayer("mayor");
        mayor.teleport(new Location(townWorld, 0, 10, 0));
        final ChunkCoordinate mayorChunk = ChunkCoordinate.forChunk(mayor.getLocation().getChunk());

        final MokkitPlayer player1 = server.mokkit().joinPlayer("player1");
        final MokkitPlayer player2 = server.mokkit().joinPlayer("player2");

        // New town.
        final Town town = tm.createTown("Test", mayorChunk, mayor.getUniqueId());
        Assert.assertEquals(1, town.getResidentIDs().size());
        Assert.assertTrue(town.getResidentIDs().contains(mayor.getUniqueId()));
        Assert.assertFalse(town.getResidentIDs().contains(player1.getUniqueId()));
        Assert.assertFalse(town.getResidentIDs().contains(player2.getUniqueId()));
        Assert.assertEquals(town, tm.getTownOf(mayor));
        Assert.assertNull(tm.getTownOf(player1));
        Assert.assertNull(tm.getTownOf(player2));

        // Mayor rejoin impossible.
        TestHelper.assertThrows(() -> tm.joinTown(town, mayor.getUniqueId()), IllegalArgumentException.class);
        Assert.assertEquals(1, town.getResidentIDs().size());

        // Player 1 joins.
        tm.joinTown(town, player1.getUniqueId());
        Assert.assertEquals(2, town.getResidentIDs().size());
        Assert.assertTrue(town.getResidentIDs().contains(mayor.getUniqueId()));
        Assert.assertTrue(town.getResidentIDs().contains(player1.getUniqueId()));
        Assert.assertFalse(town.getResidentIDs().contains(player2.getUniqueId()));
        Assert.assertEquals(town, tm.getTownOf(mayor));
        Assert.assertEquals(town, tm.getTownOf(player1));
        Assert.assertNull(tm.getTownOf(player2));

        // Player 1 rejoin impossible.
        TestHelper.assertThrows(() -> tm.joinTown(town, player1.getUniqueId()), IllegalArgumentException.class);
        Assert.assertEquals(2, town.getResidentIDs().size());

        // Player 2 joins.
        tm.joinTown(town, player2.getUniqueId());
        Assert.assertEquals(3, town.getResidentIDs().size());
        Assert.assertTrue(town.getResidentIDs().contains(mayor.getUniqueId()));
        Assert.assertTrue(town.getResidentIDs().contains(player1.getUniqueId()));
        Assert.assertTrue(town.getResidentIDs().contains(player2.getUniqueId()));
        Assert.assertEquals(town, tm.getTownOf(mayor));
        Assert.assertEquals(town, tm.getTownOf(player1));
        Assert.assertEquals(town, tm.getTownOf(player2));

        // Player 2 rejoin impossible.
        TestHelper.assertThrows(() -> tm.joinTown(town, player2.getUniqueId()), IllegalArgumentException.class);
        Assert.assertEquals(3, town.getResidentIDs().size());
    }

    @Test
    public void TownManagerTest_leaveTown() {
        final TownManager tm = TownManager.get();

        final MokkitPlayer mayor = server.mokkit().joinPlayer("mayor");
        mayor.teleport(new Location(townWorld, 0, 10, 0));
        final ChunkCoordinate mayorChunk = ChunkCoordinate.forChunk(mayor.getLocation().getChunk());

        final MokkitPlayer player1 = server.mokkit().joinPlayer("player1");
        final MokkitPlayer player2 = server.mokkit().joinPlayer("player2");

        // New town.
        final Town town = tm.createTown("Test", mayorChunk, mayor.getUniqueId());
        tm.joinTown(town, player1.getUniqueId());
        tm.joinTown(town, player2.getUniqueId());
        Assert.assertEquals(3, town.getResidentIDs().size());
        Assert.assertTrue(town.getResidentIDs().contains(mayor.getUniqueId()));
        Assert.assertTrue(town.getResidentIDs().contains(player1.getUniqueId()));
        Assert.assertTrue(town.getResidentIDs().contains(player2.getUniqueId()));
        Assert.assertEquals(town, tm.getTownOf(mayor));
        Assert.assertEquals(town, tm.getTownOf(player1));
        Assert.assertEquals(town, tm.getTownOf(player2));

        // Mayor can not leave.
        TestHelper.assertThrows(() -> tm.leaveTown(mayor.getUniqueId()), IllegalArgumentException.class);
        Assert.assertEquals(3, town.getResidentIDs().size());

        // Player 1 leaves.
        tm.leaveTown(player1.getUniqueId());
        Assert.assertEquals(2, town.getResidentIDs().size());
        Assert.assertTrue(town.getResidentIDs().contains(mayor.getUniqueId()));
        Assert.assertFalse(town.getResidentIDs().contains(player1.getUniqueId()));
        Assert.assertTrue(town.getResidentIDs().contains(player2.getUniqueId()));
        Assert.assertEquals(town, tm.getTownOf(mayor));
        Assert.assertNull(tm.getTownOf(player1));
        Assert.assertEquals(town, tm.getTownOf(player2));

        // Can not leave twice.
        TestHelper.assertThrows(() -> tm.leaveTown(player1.getUniqueId()), IllegalArgumentException.class);
        Assert.assertEquals(2, town.getResidentIDs().size());

        // Player 2 leaves.
        tm.leaveTown(player2.getUniqueId());
        Assert.assertEquals(1, town.getResidentIDs().size());
        Assert.assertTrue(town.getResidentIDs().contains(mayor.getUniqueId()));
        Assert.assertFalse(town.getResidentIDs().contains(player1.getUniqueId()));
        Assert.assertFalse(town.getResidentIDs().contains(player2.getUniqueId()));
        Assert.assertEquals(town, tm.getTownOf(mayor));
        Assert.assertNull(tm.getTownOf(player1));
        Assert.assertNull(tm.getTownOf(player2));

        // Can not leave twice.
        TestHelper.assertThrows(() -> tm.leaveTown(player2.getUniqueId()), IllegalArgumentException.class);
        Assert.assertEquals(1, town.getResidentIDs().size());
    }

    @Test
    public void TownManagerTest_load() {
        final MokkitPlayer player = server.mokkit().joinPlayer("player");
        final TownManager tm = TownManager.get();

        Assert.assertTrue(tm.getPlotsPerResident() > 0);
        Assert.assertTrue(tm.getMaximumTax() > 0);

        Assert.assertEquals(0, tm.getTowns().size());

        Assert.assertNull(tm.getTownOf(player));
        Assert.assertNull(tm.getTownAt(new ChunkCoordinate(20, 21)));
        Assert.assertNull(tm.getTown("test"));

        Assert.assertTrue(tm.getCost("claim") > 0);
        Assert.assertTrue(tm.getCost("create") > 0);
        Assert.assertTrue(tm.getCost("upkeep") > 0);
        Assert.assertTrue(tm.getCost("create") > tm.getCost("claim"));
    }

    @Test
    public void TownManagerTest_removeTown() {
        final TownManager tm = TownManager.get();

        final MokkitPlayer mayor1 = server.mokkit().joinPlayer("mayor1");
        mayor1.teleport(new Location(townWorld, 0, 10, 0));
        final ChunkCoordinate mayor1Chunk = ChunkCoordinate.forChunk(mayor1.getLocation().getChunk());

        final MokkitPlayer mayor2 = server.mokkit().joinPlayer("mayor2");
        mayor2.teleport(new Location(townWorld, 100, 10, 100));
        final ChunkCoordinate mayor2Chunk = ChunkCoordinate.forChunk(mayor2.getLocation().getChunk());

        // Create two towns.
        tm.createTown("Test1", mayor1Chunk, mayor1.getUniqueId());
        tm.createTown("Test2", mayor2Chunk, mayor2.getUniqueId());
        Assert.assertEquals(2, tm.getTowns().size());

        // Remove the second town.
        Assert.assertNotNull(tm.getTown("Test2"));
        tm.removeTown(tm.getTown("Test2"));
        Assert.assertNull(tm.getTown("Test2"));
        Assert.assertEquals(1, tm.getTowns().size());
    }

    @Test
    public void TownManagerTest_unclaimChunk() {
        final TownManager tm = TownManager.get();

        final MokkitPlayer mayor = server.mokkit().joinPlayer("mayor");
        mayor.teleport(new Location(townWorld, 0, 10, 0));
        final ChunkCoordinate mayorChunk = ChunkCoordinate.forChunk(mayor.getLocation().getChunk());

        final ChunkCoordinate chunk1 = new ChunkCoordinate(mayorChunk.getX() + 1, mayorChunk.getZ());
        final ChunkCoordinate chunk2 = new ChunkCoordinate(mayorChunk.getX() + 2, mayorChunk.getZ());
        final ChunkCoordinate chunk3 = new ChunkCoordinate(mayorChunk.getX() + 2, mayorChunk.getZ() + 1);

        // New town.
        final Town town = tm.createTown("test", mayorChunk, mayor.getUniqueId());
        tm.claimChunk(town, chunk1);
        tm.claimChunk(town, chunk2);
        Assert.assertEquals(3, town.getPlots().size());
        Assert.assertEquals(mayorChunk, town.getPlotAt(mayorChunk).getCoordinate());
        Assert.assertEquals(chunk1, town.getPlotAt(chunk1).getCoordinate());
        Assert.assertEquals(chunk2, town.getPlotAt(chunk2).getCoordinate());

        // Can not unclaim outside chunk.
        TestHelper.assertThrows(() -> tm.unclaimChunk(chunk3), IllegalArgumentException.class);
        Assert.assertEquals(3, town.getPlots().size());

        // Unclaim first chunk.
        tm.unclaimChunk(mayorChunk);
        Assert.assertEquals(2, town.getPlots().size());
        Assert.assertNull(town.getPlotAt(mayorChunk));
        Assert.assertEquals(chunk1, town.getPlotAt(chunk1).getCoordinate());
        Assert.assertEquals(chunk2, town.getPlotAt(chunk2).getCoordinate());

        // Can not unclaim twice.
        TestHelper.assertThrows(() -> tm.unclaimChunk(mayorChunk), IllegalArgumentException.class);
        Assert.assertEquals(2, town.getPlots().size());

        // Unclaim second chunk.
        tm.unclaimChunk(chunk1);
        Assert.assertEquals(1, town.getPlots().size());
        Assert.assertNull(town.getPlotAt(mayorChunk));
        Assert.assertNull(town.getPlotAt(chunk1));
        Assert.assertEquals(chunk2, town.getPlotAt(chunk2).getCoordinate());

        // Can not unclaim twice.
        TestHelper.assertThrows(() -> tm.unclaimChunk(chunk1), IllegalArgumentException.class);
        Assert.assertEquals(1, town.getPlots().size());

        // Can not unclaim final chunk.
        TestHelper.assertThrows(() -> tm.unclaimChunk(chunk2), IllegalArgumentException.class);
        Assert.assertEquals(1, town.getPlots().size());
    }

    @Before
    public void createTownWorld() {
        townWorld = server.createWorld(new WorldCreator("townworld"));
    }
}
