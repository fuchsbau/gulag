package de.fuchspfoten.gulag.test.model;

import de.fuchspfoten.gulag.model.ChunkCoordinate;
import de.fuchspfoten.gulag.model.Plot;
import de.fuchspfoten.gulag.model.Rank;
import de.fuchspfoten.gulag.model.Town;
import de.fuchspfoten.gulag.model.TownManager;
import de.fuchspfoten.gulag.model.TownPermission;
import de.fuchspfoten.gulag.test.GulagTest;
import de.fuchspfoten.mokkit.entity.living.human.MokkitPlayer;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Predicate;

/**
 * Tests for {@link de.fuchspfoten.gulag.model.Town}.
 */
public class TownTest extends GulagTest {

    /**
     * The town that is used in tests.
     */
    private Town town;

    /**
     * The mayor of the town.
     */
    private MokkitPlayer mayor;

    @Test
    public void TownTest_createRank() {
        Assert.assertEquals(0, town.getRanks().size());
        Assert.assertNull(town.getRank("test"));
        town.createRank("test");
        Assert.assertNotNull(town.getRank("test"));
        Assert.assertEquals(1, town.getRanks().size());
    }

    @Test
    public void TownTest_forEachOnline() {
        final TownManager tm = TownManager.get();

        // Add a fake player.
        tm.joinTown(town, UUID.randomUUID());

        // Add a real player.
        final MokkitPlayer member = server.mokkit().joinPlayer("member");
        tm.joinTown(town, member.getUniqueId());

        // Send a message to every online player.
        town.forEachOnline(p -> p.sendMessage("x"));

        Assert.assertEquals("x", mayor.mokkit().getLastReceivedMessage());
        Assert.assertEquals("x", member.mokkit().getLastReceivedMessage());
    }

    @Test
    public void TownTest_getPlotAt() {
        final ChunkCoordinate coord = new ChunkCoordinate(123, 123);
        Assert.assertNull(town.getPlotAt(coord));
        Assert.assertNotNull(town.getPlotAt(ChunkCoordinate.forChunk(mayor.getLocation().getChunk())));
    }

    @Test
    public void TownTest_getPlots() {
        Assert.assertEquals(1, town.getPlots().size());
        final ChunkCoordinate coord = ChunkCoordinate.forChunk(mayor.getLocation().getChunk());
        Assert.assertTrue(town.getPlots().contains(town.getPlotAt(coord)));

        final ChunkCoordinate neighbor = new ChunkCoordinate(coord.getX() + 1, coord.getZ());
        TownManager.get().claimChunk(town, neighbor);
        Assert.assertEquals(2, town.getPlots().size());
        Assert.assertTrue(town.getPlots().contains(town.getPlotAt(neighbor)));
    }

    @Test
    public void TownTest_getRank() {
        town.createRank("AbCdEfG");
        final Rank rank = town.getRank("AbCdEfG");
        Assert.assertNotNull(rank);
        Assert.assertEquals(rank, town.getRank("abcdefg"));
    }

    @Test
    public void TownTest_getRankOf() {
        town.createRank("chef");
        final Rank chefRank = town.getRank("chef");

        Assert.assertNull(town.getRankOf(mayor.getUniqueId()));
        town.setRank(mayor.getUniqueId(), chefRank);
        Assert.assertEquals(chefRank, town.getRankOf(mayor.getUniqueId()));
    }

    @Test
    public void TownTest_getRanks() {
        Assert.assertEquals(0, town.getRanks().size());
        Assert.assertFalse(town.getRanks().contains(town.getRank("a")));
        Assert.assertFalse(town.getRanks().contains(town.getRank("b")));

        town.createRank("a");
        Assert.assertEquals(1, town.getRanks().size());
        Assert.assertTrue(town.getRanks().contains(town.getRank("a")));
        Assert.assertFalse(town.getRanks().contains(town.getRank("b")));

        town.createRank("b");
        Assert.assertEquals(2, town.getRanks().size());
        Assert.assertTrue(town.getRanks().contains(town.getRank("a")));
        Assert.assertTrue(town.getRanks().contains(town.getRank("b")));
    }

    @Test
    public void TownTest_getResidentIDs() {
        final Player member = server.mokkit().joinPlayer("member");

        Assert.assertEquals(1, town.getResidentIDs().size());
        Assert.assertTrue(town.getResidentIDs().contains(mayor.getUniqueId()));
        Assert.assertFalse(town.getResidentIDs().contains(member.getUniqueId()));

        TownManager.get().joinTown(town, member.getUniqueId());
        Assert.assertEquals(2, town.getResidentIDs().size());
        Assert.assertTrue(town.getResidentIDs().contains(mayor.getUniqueId()));
        Assert.assertTrue(town.getResidentIDs().contains(member.getUniqueId()));
    }

    @Test
    public void TownTest_hasPermission() {
        final MokkitPlayer member = server.mokkit().joinPlayer("member");
        TownManager.get().joinTown(town, member.getUniqueId());

        // Mayor has every permission.
        for (final TownPermission tp : TownPermission.values()) {
            Assert.assertTrue(town.hasPermission(mayor, tp));
            Assert.assertTrue(town.hasPermission(mayor.getUniqueId(), tp));
        }

        // Member has no permission.
        for (final TownPermission tp : TownPermission.values()) {
            Assert.assertFalse(town.hasPermission(member, tp));
            Assert.assertFalse(town.hasPermission(member.getUniqueId(), tp));

            // Check the error message.
            Assert.assertTrue(member.mokkit().getLastReceivedMessage().contains("townPermission"));
            member.mokkit().clearChatLog();
        }

        final Predicate<TownPermission> permissionSelection = tp -> tp.getNode().length() % 2 == 0;
        town.createRank("somePermissions");
        final Rank rank = town.getRank("somePermissions");
        for (final TownPermission tp : TownPermission.values()) {
            if (permissionSelection.test(tp)) {
                rank.addPermission(tp.getNode());
            }
        }

        // Ensure that not all permissions are added to the rank...
        Assert.assertNotEquals(TownPermission.values().length,
                rank.getPermissions().size());

        // Add the member to the rank.
        town.setRank(member.getUniqueId(), rank);

        // Member has the permissions for which the predicate holds.
        for (final TownPermission tp : TownPermission.values()) {
            if (permissionSelection.test(tp)) {
                Assert.assertTrue(town.hasPermission(member, tp));
                Assert.assertTrue(town.hasPermission(member.getUniqueId(), tp));
            } else {
                Assert.assertFalse(town.hasPermission(member, tp));
                Assert.assertFalse(town.hasPermission(member.getUniqueId(), tp));

                // Check the error message.
                Assert.assertTrue(member.mokkit().getLastReceivedMessage().contains("townPermission"));
                member.mokkit().clearChatLog();
            }
        }
    }

    @Test
    public void TownTest_invite() {
        final UUID who = UUID.randomUUID();

        Assert.assertFalse(town.isInvited(who));
        town.invite(who);
        Assert.assertTrue(town.isInvited(who));
        town.invite(who);
        Assert.assertTrue(town.isInvited(who));
    }

    @Test
    public void TownTest_isInvited() {
        final UUID who = UUID.randomUUID();

        Assert.assertFalse(town.isInvited(who));
        Assert.assertFalse(town.isInvited(mayor.getUniqueId()));
        Assert.assertFalse(town.isInvited(UUID.randomUUID()));

        town.invite(who);
        Assert.assertTrue(town.isInvited(who));
        Assert.assertFalse(town.isInvited(mayor.getUniqueId()));
        Assert.assertFalse(town.isInvited(UUID.randomUUID()));
    }

    @Test
    public void TownTest_modifyBalance() {
        final int balance = town.getBalance();

        town.modifyBalance(200);
        Assert.assertEquals(balance + 200, town.getBalance());

        town.modifyBalance(-100);
        Assert.assertEquals(balance + 100, town.getBalance());
    }

    @Test
    public void TownTest_removeRank() {
        final List<Rank> ranks = new ArrayList<>();

        // Add 9 members.
        for (int i = 0; i < 3; i++) {
            final String rankName = "r" + i;
            town.createRank(rankName);
            final Rank rank = town.getRank(rankName);
            ranks.add(rank);

            // Add players.
            for (int j = 0; j < 3; j++) {
                final Player who = server.mokkit().joinPlayer("player" + i + '_' + j);
                TownManager.get().joinTown(town, who.getUniqueId());
                town.setRank(who.getUniqueId(), rank);
            }
        }

        // Check the setup.
        Assert.assertEquals(3, town.getRanks().size());
        for (final Rank rank : ranks) {
            Assert.assertEquals(3, rank.getMemberIds().size());
        }
        Assert.assertEquals(10, town.getResidentIDs().size());

        // Remove the ranks one by one.
        for (int i = 0; i < 3; i++) {
            town.removeRank(ranks.get(i).getName());
            Assert.assertEquals(2 - i, town.getRanks().size());
            final int withoutRank = (int) town.getResidentIDs().stream()
                    .map(town::getRankOf)
                    .filter(Objects::isNull)
                    .count();
            Assert.assertEquals(1 + 3 * (i + 1), withoutRank);
        }

        // Check the setup again.
        Assert.assertEquals(0, town.getRanks().size());
        Assert.assertEquals(10, town.getResidentIDs().size());
        for (final UUID who : town.getResidentIDs()) {
            Assert.assertNull(town.getRankOf(who));
        }
    }

    @Test
    public void TownTest_runTaxes_disband() {
        // Starting at 100%: 2 runs of upkeep.
        final int balanceBefore = town.getBalance();

        // First collection: 50% balance.
        town.runTaxes();
        Assert.assertEquals(balanceBefore / 2, town.getBalance());
        Assert.assertTrue(mayor.mokkit().getLastReceivedMessage().contains("taxCollected"));
        Assert.assertTrue(!mayor.mokkit().hasMessageLike("lostPlots"));

        // Second collection: 0% balance.
        town.runTaxes();
        Assert.assertEquals(0, town.getBalance());
        Assert.assertTrue(mayor.mokkit().getLastReceivedMessage().contains("taxCollected"));
        Assert.assertTrue(!mayor.mokkit().hasMessageLike("lostPlots"));

        // Third collection: Town disbanded.
        town.runTaxes();
        Assert.assertEquals(0, TownManager.get().getTowns().size());
        Assert.assertTrue(mayor.mokkit().getLastReceivedMessage().contains("lostTown"));
        Assert.assertTrue(!mayor.mokkit().hasMessageLike("lostPlots"));
    }

    @Test
    public void TownTest_runTaxes_losePlots() {
        final ChunkCoordinate coord = ChunkCoordinate.forChunk(mayor.getLocation().getChunk());
        final Plot plot = town.getPlotAt(coord);
        plot.setOwnerId(mayor.getUniqueId());

        // Collection: Plots seized.
        town.runTaxes();
        Assert.assertTrue(mayor.mokkit().hasMessageLike("lostPlots"));
        Assert.assertTrue(mayor.mokkit().hasMessageLike("taxCollected"));
        Assert.assertTrue(!mayor.mokkit().hasMessageLike("lostTown"));
    }

    @Test
    public void TownTest_runTaxes_collectMoney() {
        final TownManager tm = TownManager.get();

        // Add members to the town.
        final Player member1 = server.mokkit().joinPlayer("member1");
        tm.joinTown(town, member1.getUniqueId());
        final Player member2 = server.mokkit().joinPlayer("member2");
        tm.joinTown(town, member2.getUniqueId());

        // Claim more chunks.
        final ChunkCoordinate coord = ChunkCoordinate.forChunk(mayor.getLocation().getChunk());
        final ChunkCoordinate x1 = new ChunkCoordinate(coord.getX() + 1, coord.getZ());
        final ChunkCoordinate xm1 = new ChunkCoordinate(coord.getX() - 1, coord.getZ());
        final ChunkCoordinate z1 = new ChunkCoordinate(coord.getX(), coord.getZ() + 1);
        final ChunkCoordinate zm1 = new ChunkCoordinate(coord.getX(), coord.getZ() - 1);
        tm.claimChunk(town, x1);
        tm.claimChunk(town, xm1);
        tm.claimChunk(town, z1);
        tm.claimChunk(town, zm1);

        // Assign 3 plots to member1, 0 to member2, and 2 to the mayor.
        final Plot plot = town.getPlotAt(coord);
        final Plot plotX1 = town.getPlotAt(x1);
        final Plot plotXm1 = town.getPlotAt(xm1);
        final Plot plotZ1 = town.getPlotAt(z1);
        final Plot plotZm1 = town.getPlotAt(zm1);
        plot.setOwnerId(mayor.getUniqueId());
        plotX1.setOwnerId(mayor.getUniqueId());
        plotXm1.setOwnerId(member1.getUniqueId());
        plotZ1.setOwnerId(member1.getUniqueId());
        plotZm1.setOwnerId(member1.getUniqueId());

        // Assign 10 units of money to every member.
        final Economy eco = server.getServicesManager().load(Economy.class);
        eco.withdrawPlayer(mayor, eco.getBalance(mayor));
        eco.depositPlayer(mayor, 10);
        eco.withdrawPlayer(member1, eco.getBalance(member1));
        eco.depositPlayer(member1, 10);
        eco.withdrawPlayer(member2, eco.getBalance(member2));
        eco.depositPlayer(member2, 10);

        // This should collect 3 from member1, 0 from member2, and 2 from the mayor.
        Assert.assertEquals(10, eco.getBalance(mayor), 0.01);
        Assert.assertEquals(10, eco.getBalance(member1), 0.01);
        Assert.assertEquals(10, eco.getBalance(member2), 0.01);
        town.setTax(1);
        town.modifyBalance(-town.getBalance());
        town.modifyBalance(10000);
        town.runTaxes();
        Assert.assertEquals(8, eco.getBalance(mayor), 0.01);
        Assert.assertEquals(7, eco.getBalance(member1), 0.01);
        Assert.assertEquals(10, eco.getBalance(member2), 0.01);

        final int deducted = town.getPlots().size() * TownManager.get().getCost("upkeep");
        final int paid = 5;
        Assert.assertEquals(10000 - deducted + paid, town.getBalance());
    }

    @Test
    public void TownTest_setRank() {
        town.createRank("test1");
        town.createRank("test2");
        final Rank rank1 = town.getRank("test1");
        final Rank rank2 = town.getRank("test2");

        Assert.assertEquals(0, rank1.getMemberIds().size());
        Assert.assertEquals(0, rank2.getMemberIds().size());
        Assert.assertNull(town.getRankOf(mayor.getUniqueId()));

        town.setRank(mayor.getUniqueId(), rank1);
        Assert.assertEquals(1, rank1.getMemberIds().size());
        Assert.assertEquals(0, rank2.getMemberIds().size());
        Assert.assertEquals(rank1, town.getRankOf(mayor.getUniqueId()));

        town.setRank(mayor.getUniqueId(), rank2);
        Assert.assertEquals(0, rank1.getMemberIds().size());
        Assert.assertEquals(1, rank2.getMemberIds().size());
        Assert.assertEquals(rank2, town.getRankOf(mayor.getUniqueId()));

        town.setRank(mayor.getUniqueId(), null);
        Assert.assertEquals(0, rank1.getMemberIds().size());
        Assert.assertEquals(0, rank2.getMemberIds().size());
        Assert.assertNull(town.getRankOf(mayor.getUniqueId()));
    }

    @Before
    public void createTownWorld() {
        final World townWorld = server.createWorld(new WorldCreator("townworld"));

        mayor = server.mokkit().joinPlayer("mayor");
        mayor.teleport(new Location(townWorld, 10, 10, 10));
        final ChunkCoordinate coord = ChunkCoordinate.forChunk(mayor.getLocation().getChunk());
        town = TownManager.get().createTown("test", coord, mayor.getUniqueId());
    }
}
